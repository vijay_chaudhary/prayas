package com.org.prayas;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import dagger.hilt.android.HiltAndroidApp;

/**
 * Created by Vijay on 01/02/2022.
 */
@HiltAndroidApp
public class MyApplication extends Application {
    public LocalBroadcastManager mBroadcastManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    public LocalBroadcastManager getLocalBroadcastManager() {
        return mBroadcastManager;
    }

    /**
     * Check for  permission is not granted
     *
     * @param permission list of  permission
     * @return non granted  permission list
     */
    public List<String> checkPermissionList(String[] permission, Context context) {
        List<String> needPermission = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permission != null &&
                permission.length > 0) {
            needPermission = new ArrayList<>();
            for (String aPermission : permission) {
                if (ActivityCompat.checkSelfPermission(context, aPermission) !=
                        PackageManager.PERMISSION_GRANTED) {
                    needPermission.add(aPermission);
                }
            }
        }
        return needPermission;
    }

    public RequestOptions glideBaseOptions(int drawable) {
        return new RequestOptions().placeholder(drawable)
                .error(drawable).dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
    }

    public RequestOptions glideCenterCropOptions(int drawable) {
        return glideBaseOptions(drawable).centerCrop();
    }

    public RequestOptions glideOptionRoundCorner(int radios, int placeHolder) {
        return glideBaseOptions(placeHolder)
                .transform(new MultiTransformation<>(new CenterCrop(), new RoundedCorners(radios)))
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }

    public RequestOptions glideOptionRoundCornerSimple(int radios, int placeHolder) {
        return glideBaseOptions(placeHolder)
                .transform(new MultiTransformation<>(new RoundedCorners(radios)))
                .diskCacheStrategy(DiskCacheStrategy.ALL).error(placeHolder);
    }
}
