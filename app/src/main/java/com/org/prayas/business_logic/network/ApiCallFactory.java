package com.org.prayas.business_logic.network;

import static com.org.prayas.business_logic.preferences.PrefKeys.prefIsUserLogIn;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefUserToken;

import android.text.TextUtils;


import com.org.prayas.BuildConfig;
import com.org.prayas.business_logic.preferences.UtilsSharedPreferences;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Vijay 05, Aug.
 */

public class ApiCallFactory {
    private static final int NETWORK_CALL_TIMEOUT = 60; //600, 300

    public static ApiHelper create(String url, UtilsSharedPreferences sharedPreferences) {
        return getRetrofit(url, sharedPreferences).create(ApiHelper.class);
    }

    private static Retrofit getRetrofit(String url, UtilsSharedPreferences sharedPreferences) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(NETWORK_CALL_TIMEOUT, TimeUnit.SECONDS);
        builder.writeTimeout(NETWORK_CALL_TIMEOUT, TimeUnit.SECONDS);
        builder.followRedirects(false);
        builder.addInterceptor(chain -> {
            String accessToken = sharedPreferences.getString(prefUserToken);
            Request.Builder requestBuilder = chain.request().newBuilder();
            if (!TextUtils.isEmpty(accessToken) && sharedPreferences.getBoolean(prefIsUserLogIn)) {
                requestBuilder.addHeader("Authorization", "Basic "+accessToken);
            }
            return chain.proceed(requestBuilder.build());
        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }

        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(builder.build())
                .build();
    }
}
