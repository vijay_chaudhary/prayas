package com.org.prayas.business_logic.preferences;

public class PrefKeys {
    public static final String prefBjp = "com.bjp.org.business_logic.preferences.bjp";
    public static final String prefIsUserLogIn = "prefIsUserLogIn";
    public static final String prefUserToken = "prefUserToken";
    public static final String prefLang = "prefLang";
    public static final String prefUserId = "prefUserId";
    public static final String prefIsRememberMe = "prefIsRememberMe";
    public static final String prefLoginUserId = "prefLoginUserId";
    public static final String prefLoginPassword = "prefLoginPassword";

    public static final String prefUserEmail = "prefUserEmail";
    public static final String prefUserMobile = "prefUserMobile";
    public static final String prefUserName = "prefUserName";
    public static final String prefUserType = "prefUserType";
    public static final String prefUserImage = "prefUserImage";
    public static final String prefUserFirstName = "prefUserFirstName";
    public static final String prefUserMiddleName = "prefUserMiddleName";
    public static final String prefUserLastName = "prefUserLastName";
    public static final String prefIsMLOldCertificateHolder = "prefIsMLOldCertificateHolder";
    public static final String prefIsUserFirstTime = "IsUserFirstTime";

}
