package com.org.prayas.business_logic.interfaces;

/**
 * Created by Naria Sachin on Mar, 24 2021 19:05.
 */

public interface OnScrollListener {
    void onScrollStateChanged(Boolean isEnabled);

    void onScrolled(int totalItemCount, int pastVisibleItems);
}
