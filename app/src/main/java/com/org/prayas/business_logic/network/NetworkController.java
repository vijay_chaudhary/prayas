package com.org.prayas.business_logic.network;

public class NetworkController {

    //  Prefix
    private static final String LOGIN = "login/";
    private static final String USER = "user/";
    private static final String COMMON = "common/";
    private static final String MONEY_LENDER = "MoneyLender/";
    private static final String CO_OPERATIVE_SOCIETY = "CoOperativeSociety/";

    //  APIs
    public static final String API_LOGIN = LOGIN + "login";
    public static final String API_FORGOT_PASSWORD = LOGIN + "Forgot_Password";

    public static final String API_REGISTER = USER + "Register";
    public static final String API_GET_USER_TYPE = USER + "GetUserType";
    public static final String API_VERIFY_OTP = USER + "VerifyOTP";
    public static final String API_USER_PROFILE = USER + "GetUserProfile";
    public static final String API_RESET_PASSWORD = USER + "ResetPassword";

    public static final String API_MONEY_LENDER_LIST = MONEY_LENDER + "MoneyLenderList";
    public static final String API_MONTHLY_PATRAK_LIST = MONEY_LENDER + "MonthlyPatrakList";
    public static final String API_ML_DASHBOARD = MONEY_LENDER + "MoneyLenderDashboard";
    public static final String API_GET_ML_DETAILS = MONEY_LENDER + "MoneyLenderDetails";
    public static final String API_GET_ML_OLD_CER_DETAILS = MONEY_LENDER + "MoneyLenderOldCertificateDetails";
    public static final String API_ML_UPLOAD_RESIDENTIAL = MONEY_LENDER + "MoneyLenderUploadResidentialProof";
    public static final String API_ML_UPLOAD_BUSINESS = MONEY_LENDER + "MoneyLenderUploadBusinessProof";
    public static final String API_ML_UPLOAD_RENT = MONEY_LENDER + "MoneyLenderUploadRentProof";
    public static final String API_ML_UPLOAD_OTHER = MONEY_LENDER + "MoneyLenderUploadOtherProof";
    public static final String API_ML_REGISTER = MONEY_LENDER + "MoneyLenderRegisterNew";
    public static final String API_ML_OLD_CER_REGISTER = MONEY_LENDER + "MoneyLenderOldCertificateRegister";
    public static final String API_ML_DELETE_PROOF = MONEY_LENDER + "MoneyLenderDeleteProof";
    public static final String API_SUBMIT_MONTHLY_PATRAK = MONEY_LENDER + "SubmitMonthlyPatrak";
    public static final String API_MP_DEFAULTS = MONEY_LENDER + "GetDefaultsForAddMonthlyPartrak";
    public static final String API_DELETE_MP = MONEY_LENDER + "DeleteMonthlyPatrak";

    public static final String CO_OPERATIVE_SOCIETY_LIST = CO_OPERATIVE_SOCIETY + "CoOperativeSocietyList";
    public static final String API_GET_SOCIETY_DETAILS = CO_OPERATIVE_SOCIETY + "CoOperativeSocietyDetails";
    public static final String API_GET_SOCIETY_REGISTER = CO_OPERATIVE_SOCIETY + "CoOperativeSocietyRegister";


    public static final String API_GET_SOCIETY_DASHBOARD= CO_OPERATIVE_SOCIETY + "CoOperativeSocietyDashboard";

    public static final String API_GET_STATE = COMMON + "GetState";
    public static final String API_GET_DISTRICT = COMMON + "GetDistrictByState";
    public static final String API_GET_TALUKA = COMMON + "GetTalukaByDistrict";
    public static final String API_GET_VILLAGE_CITY = COMMON + "GetVillageByDistrict";
    public static final String API_GET_PERSON_TYPE = COMMON + "GetPersonType";
    public static final String API_GET_BUSINESS_TYPE = COMMON + "GetBusinessType";
    public static final String API_GET_FINANCIAL_YEAR = COMMON + "GetFinancialYear";
    public static final String API_GET_FINANCIAL_MONTHS = COMMON + "GetFinancialMonths";
    public static final String API_GET_RESIDENTIAL_DOC_TYPE = COMMON + "GetResidentialDocType";
    public static final String API_GET_BUSINESS_DOC_TYPE = COMMON + "GetBusinessDocType";
    public static final String API_GET_RENT_DOC_TYPE = COMMON + "GetRentDocType";
    public static final String API_GET_OTHER_DOC_TYPE = COMMON + "GetOtherDocType";
    public static final String API_GET_SOCIETY_TYPE = COMMON + "GetSocietyType";
    public static final String API_GET_AUDIT_CLASS = COMMON + "GetAuditClass";
    public static final String API_GET_MEMBER_CATEGORY = COMMON + "GetMemberCategory";
    public static final String API_GET_SOCIETY_STATUS = COMMON + "GetSocFilterStatus";
    public static final String API_GET_SOCIETY_HOME_DTL_TYPE = COMMON + "GetSocietyHomeDetailType";
    public static final String API_GET_SOCIETY_GODOWN_TYPE = COMMON + "GetSocietyGodownType";
    public static final String API_GET_SOCIETY_TYPE_UNDER_12 = COMMON + "GetSocietyTypeUnderRules12";

    public static final String API_GET_UPDATE_PROFILE = LOGIN + "UpdateProfile";

    //  Default PARAMs
    public static final String PARAMS_UNIQUE_ID = "UniqueId";
    public static final String PARAMS_TOKEN = "token";
    public static final String PARAMS_LANG = "language";
    public static final String PARAMS_USERNAME = "UserName";
    public static final String PARAMS_START = "Start";
    public static final String PARAMS_LENGTH = "Length";
    public static final String PARAMS_SEARCH_TEXT = "Searchtxt";
    public static final String PARAMS_STATUS = "Status";
    public static final String PARAMS_STATE_ID = "StateID";
    public static final String PARAMS_DISTRICT_ID = "DistrictID";
    public static final String PARAMS_TALUKA_ID = "TalukaID";
    public static final String PARAMS_PASSWORD = "Password";
    public static final String PARAMS_CURRENT_PASSWORD = "CurrentPassword";
    public static final String PARAMS_NEW_PASSWORD = "NewPassword";
    public static final String PARAMS_USER_TYPE_ID = "UserTypeId";
    public static final String PARAMS_NAME = "Name";
    public static final String PARAMS_EMAIL_ID = "EmailId";
    public static final String PARAMS_MOBILE_NO = "MobileNo";
    public static final String PARAMS_IS_ML_OLD_CERT_HOLDER = "IsMLOldCertificateHolder";
    public static final String PARAMS_SOCIETY_ID = "SocietyId";
    public static final String PARAMS_SOCIETY_TYPE = "SocietyType";
    public static final String PARAMS_ML_ID = "MoneyLenderId";
    public static final String PARAMS_MONEY_LENDER_ID = "money_lender_id";
    public static final String PARAMS_DOC_CATEGORY_ID = "DocumentCategoryId";
    public static final String PARAMS_DOCUMENT_ID = "DocumentId";
    public static final String PARAMS_DOCUMENT_TYPE_ID = "DocumentTypeId";
    public static final String PARAMS_DOC_FILE = "File";
    public static final String PARAMS_DATA = "data";
    public static final String PARAMS_SIGN = "sign";
    public static final String PARAMS_AFFIDAVIT = "affidavit";
    public static final String PARAMS_POLICE_CERTIFICATE = "police_certificate";
    public static final String PARAMS_OLD_REG_CERTIFICATE = "old_registration_certificate";
    public static final String PARAMS_PHOTO = "photo";
    public static final String PARAMS_YEAR = "year";
    public static final String PARAMS_MONTH = "month";
    public static final String PARAMS_VEPARI_LEN_AMOUNT = "VepariLendingAmount";
    public static final String PARAMS_BIN_VEPARI_LEN_AMOUNT = "BinVepariLendingAmount";
    public static final String PARAMS_PAST_LEN_AMOUNT = "PastLendingAmount";
    public static final String PARAMS_TOTAL_LEN_AMOUNT = "TotalLendingAmount";
    public static final String PARAMS_DATE_OF_CREDIT = "DateOfCredit";
    public static final String PARAMS_IS_SAVE_DRAFT = "isSaveDraft";

    public static final String PARAMS_ID = "ID";
    public static final String PARAMS_ID_lower = "id";
    public static final String PARAMS_EMAIL_OTP = "EmailOTP";
}
