package com.org.prayas.business_logic.pojo.base;

import java.util.List;

/**
 * Created by Vijay Aug, 05 2021.
 */

public abstract class PojoResponseListBase extends PojoResponseBase {
    public abstract boolean isValidList();

    public abstract int getListCount();

    public boolean isValidList(List<?> list) {
        return (list != null && !list.isEmpty());
    }
}
