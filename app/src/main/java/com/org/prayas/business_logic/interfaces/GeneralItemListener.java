package com.org.prayas.business_logic.interfaces;

import android.view.View;

/**
 * Created by Naria Sachin on Jan, 16 2020 17:39.
 */

public interface GeneralItemListener {
    void onItemClick(View view, int position, Object item);
}
