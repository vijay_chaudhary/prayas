package com.org.prayas.business_logic.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class NetworkHelper {

    private final Context mContext;

    @Inject
    public NetworkHelper(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * Checks Internet Connectivity.
     *
     * @return returns true if connected else false.
     */
    public boolean isInternetConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return true;
        }
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork == null || !activeNetwork.isConnected();
    }
}
