package com.org.prayas.business_logic.interfaces;

/**
 * Created by Naria Sachin on Jan, 16 2020 17:39.
 */

public interface GeneralItemCheckListener {
    void onItemCheck(boolean isCheck, int position, Object item);
}
