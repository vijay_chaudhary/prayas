package com.org.prayas.business_logic.interfaces;

import java.util.List;

public interface GeneralResponseListener<T> {
    void onResponse(List<T> list);
}
