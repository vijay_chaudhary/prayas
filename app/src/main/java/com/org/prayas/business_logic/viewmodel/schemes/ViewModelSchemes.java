package com.org.prayas.business_logic.viewmodel.schemes;

import static com.org.prayas.utils.ConstantCodes.MINISTRY_ID;
import static com.org.prayas.utils.ConstantCodes.PAGE_MIN_DEPT;
import static com.org.prayas.utils.ConstantCodes.PAGE_SECTORS;
import static com.org.prayas.utils.Utils.getEncryptedString;

import android.text.TextUtils;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableInt;
import androidx.databinding.ObservableList;

import com.org.prayas.R;
import com.org.prayas.business_logic.interactors.ObservableString;
import com.org.prayas.business_logic.interfaces.OnScrollListener;
import com.org.prayas.business_logic.pojo.PojoMinDeptData;
import com.org.prayas.business_logic.pojo.PojoPlace;
import com.org.prayas.business_logic.pojo.PojoSchemes;
import com.org.prayas.business_logic.pojo.PojoSchemesFilterData;
import com.org.prayas.business_logic.pojo.PojoSectorsData;
import com.org.prayas.business_logic.viewmodel.ViewModelBase;
import com.org.prayas.utils.Logger;
import com.org.prayas.utils.ResourceProvider;
import com.org.prayas.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class ViewModelSchemes extends ViewModelBase {
    public ObservableString searchText = new ObservableString("");
    public ObservableList<String> sectorsIDs = new ObservableArrayList<>();
    public ObservableList<String> minDeptIDs = new ObservableArrayList<>();

    public ObservableList<PojoSchemes> schemesList = new ObservableArrayList<>();
    public ObservableList<PojoSchemes> schemesListFiltered = new ObservableArrayList<>();
    public ObservableArrayList<PojoSchemesFilterData> observableListFilter = new ObservableArrayList<>();

    public ObservableInt observerNoRecord = new ObservableInt(R.string.message_no_record_found);
    //public ObservableString textDepartment = new ObservableString("");

    public ObservableArrayList<PojoPlace> observableState = new ObservableArrayList<>();
    public ObservableArrayList<PojoPlace> observableDistrict = new ObservableArrayList<>();

    public ObservableInt statePosition = new ObservableInt(0);
    public ObservableInt districtPosition = new ObservableInt(0);
    public ObservableString stateId = new ObservableString("");
    public ObservableString districtId = new ObservableString("");
    public List<String> sectorIdList = new ArrayList<>();
    public List<String> miniIdList = new ArrayList<>();

    @Inject
    public ViewModelSchemes(ResourceProvider resourceProvider) {
        this.mResourceProvider = resourceProvider;
        /*PojoSchemes pojoSchemes;

        pojoSchemes = new PojoSchemes();
        pojoSchemes.setTitle("PM Shram Yogi Maandhan Yojana");
        pojoSchemes.setTitleLogo(R.drawable.bg_pm_sym_logo_);
        pojoSchemes.setItemColor("#EFC4DF");

        pojoSchemes.setTextTitle("Subscribers (% of Women)");
        pojoSchemes.setTitleValue("30.50");
        pojoSchemes.setTitleRupee("Lakh (65%)");

        pojoSchemes.setTextDesc("Disbursed");
        pojoSchemes.setDescValue("7.56");
        pojoSchemes.setDescRupee("L Cr");

        pojoSchemes.setMcValue("3.40");
        //pojoSchemes.setUpDownValue("+28%");

        pojoSchemes.setSchemeId("1");
        pojoSchemes.setMinistryId("27");
        pojoSchemes.setSectorId("2");
        pojoSchemes.setState("1");
        pojoSchemes.setDistrict("12");
        pojoSchemes.setTotalSubscribers("4109458");
        pojoSchemes.setAmountDisbursed("11256982.45");

        pojoSchemes.getSubscribeTopState().add(new PojoPlace("1. Uttar Pradesh"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("2. Himachal Pradesh"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("3. Tamil Nadu"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("4. Rajasthan"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("5. Maharashtra"));

        pojoSchemes.getDisbursedTopState().add(new PojoPlace("1. Gujarat"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("2. Rajasthan"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("3. Panjab"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("4. Kerala"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("5. Maharashtra"));

        pojoSchemes.getSubscribeData().add(new PojoGraph("Oct-21", 10));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Nov-21", 5));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Dec-21", 2));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Jan-22", 7));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Feb-22", 4));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Mar-22", 11));

        pojoSchemes.getDisbursedData().add(new PojoGraph("Oct-23", 5));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Nov-23", 6));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Dec-23", 2));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Jan-24", 9));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Feb-24", 4));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Mar-24", 3));

        schemesList.add(pojoSchemes);

        pojoSchemes = new PojoSchemes();
        pojoSchemes.setTitle("Atmanirbhar Bharat Rojgar Yojana");
        pojoSchemes.setTitleLogo(R.drawable.bg_abry_logo_);
        pojoSchemes.setItemColor("#E5CCF2");

        pojoSchemes.setTextTitle("Beneficiaries");
        pojoSchemes.setTitleValue("85");
        pojoSchemes.setTitleRupee("Lakh");

        pojoSchemes.setTextDesc("Establishments Benefited");
        pojoSchemes.setDescValue("2.19");
        pojoSchemes.setDescRupee("Lakh");

        pojoSchemes.setMcValue("3.40");
        //pojoSchemes.setUpDownValue("+28%");

        pojoSchemes.setSchemeId("2");
        pojoSchemes.setMinistryId("1");
        pojoSchemes.setSectorId("1");
        pojoSchemes.setState("1");
        pojoSchemes.setDistrict("11,12");
        pojoSchemes.setTotalSubscribers("8965418");
        pojoSchemes.setAmountDisbursed("98563154.80");

        pojoSchemes.getSubscribeTopState().add(new PojoPlace("1. Himachal Pradesh"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("2. Madhya Pradesh"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("3. Nagaland"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("4. Punjab"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("5. Orissa"));

        pojoSchemes.getDisbursedTopState().add(new PojoPlace("1. Kerala"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("2. Mizoram"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("3. Sikkim"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("4. Jharkhand"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("5. Meghalaya"));

        pojoSchemes.getSubscribeData().add(new PojoGraph("Oct-26", 2));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Nov-26", 7));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Dec-26", 4));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Jan-27", 12));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Feb-27", 8));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Mar-27", 3));

        pojoSchemes.getDisbursedData().add(new PojoGraph("Oct-28", 2));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Nov-28", 12));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Dec-28", 5));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Jan-29", 10));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Feb-29", 6));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Mar-29", 9));

        schemesList.add(pojoSchemes);

        pojoSchemes = new PojoSchemes();
        pojoSchemes.setTitle("eShram Yojana");
        pojoSchemes.setTitleLogo(R.drawable.bg_e_shram_logo_);
        pojoSchemes.setItemColor("#CECFF5");

        pojoSchemes.setTextTitle("Beneficiaries");
        pojoSchemes.setTitleValue("30.40");
        pojoSchemes.setTitleRupee("Lakh");

        pojoSchemes.setTextDesc("Establishments Benefited");
        pojoSchemes.setDescValue("14.05");
        pojoSchemes.setDescRupee("Cr (20.70%)");

        pojoSchemes.setMcValue("3.90");
        //pojoSchemes.setUpDownValue("+28%");

        pojoSchemes.setSchemeId("1");
        pojoSchemes.setMinistryId("70");
        pojoSchemes.setSectorId("101");
        pojoSchemes.setState("10");
        pojoSchemes.setDistrict("20");
        pojoSchemes.setTotalSubscribers("6235458");
        pojoSchemes.setAmountDisbursed("65896548.30");

        pojoSchemes.getSubscribeTopState().add(new PojoPlace("1. Uttarakhand"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("2. West Bengal"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("3. Tripura"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("4. Maharashtra"));
        pojoSchemes.getSubscribeTopState().add(new PojoPlace("5. Haryana"));

        pojoSchemes.getDisbursedTopState().add(new PojoPlace("1. Nagaland"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("2. Goa"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("3. Gujarat"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("4. Delhi"));
        pojoSchemes.getDisbursedTopState().add(new PojoPlace("5. Andhra Pradesh"));

        pojoSchemes.getSubscribeData().add(new PojoGraph("Oct-26", 3));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Nov-26", 5));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Dec-26", 10));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Jan-27", 6));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Feb-27", 11));
        pojoSchemes.getSubscribeData().add(new PojoGraph("Mar-27", 3));

        pojoSchemes.getDisbursedData().add(new PojoGraph("Oct-23", 11));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Nov-23", 5));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Dec-23", 8));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Jan-24", 3));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Feb-24", 5));
        pojoSchemes.getDisbursedData().add(new PojoGraph("Mar-24", 2));
        schemesList.add(pojoSchemes);*/

        //textDepartment.set("Ministry of Labour & Employment");

        observableState.add(new PojoPlace(mResourceProvider.getString(R.string.text_select_state)));
        observableDistrict.add(new PojoPlace(mResourceProvider.getString(R.string.text_select_district)));

        //schemesListFiltered.addAll(schemesList);

        /*PojoSchemes pojoSchemes = null;
        for (int i = 1; i <= 7; i++) {

            pojoSchemes = new PojoSchemes();
            pojoSchemes.setTitle("PM Shram Yogi Maandhan Yojana");
            pojoSchemes.setTitleLogoUrl("http://14.143.90.241/prayash/Scheme_Logo/70107.png");
            pojoSchemes.setSectorId(""+i);

            pojoSchemes.setTextTitle("Subscribers (% of Women)");
            pojoSchemes.setTitleValue("30.50");
            pojoSchemes.setTitleRupee("Lakh (65%)");

            pojoSchemes.setTextDesc("Disbursed");
            pojoSchemes.setDescValue("7.56");
            pojoSchemes.setDescRupee("L Cr");

            pojoSchemes.setMcValue("3.40");
            //pojoSchemes.setUpDownValue("+28%");

            pojoSchemes.setSchemeId("1");
            pojoSchemes.setMinistryId("27");
            //pojoSchemes.setSectorId("2");
            pojoSchemes.setState("1");
            pojoSchemes.setDistrict("12");
            pojoSchemes.setUpColorData();

            schemesList.add(pojoSchemes);
        }

        //schemesList.add(pojoSchemes);
        schemesListFiltered.addAll(schemesList);*/

    }

    public void onRefreshList(boolean isFromSearch) {
        observableSwipeRefreshLayout.set(true);

        searchText.set("");
        statePosition.set(0);
        districtPosition.set(0);
        stateId.set("");
        districtId.set("");
        clearAll();
        callDashboardWS();

        callStateAPI();
        callFilterAPI();
    }

    public void apiFilter() {
       /* if (isInternetConnected(R.string.message_noconnection)) {
            observableListFilter.clear();

            List<PojoSchemesFilterData> pojoDashboardData = new ArrayList<>();

            List<PojoSectorsData> pojoSectorsDataList = new ArrayList<>();
            PojoSectorsData pojoSectorsData = new PojoSectorsData();
            pojoSectorsData.setId("1");
            pojoSectorsData.setTitle("Agriculture & Rural");
            pojoSectorsData.setBg_color(getColor(myColorMap.get(pojoSectorsData.getId())));
            pojoSectorsDataList.add(pojoSectorsData);

            pojoSectorsData = new PojoSectorsData();
            pojoSectorsData.setId("2");
            pojoSectorsData.setTitle("Social & Welfare");
            pojoSectorsData.setBg_color(getColor(myColorMap.get(pojoSectorsData.getId())));
            pojoSectorsDataList.add(pojoSectorsData);

            pojoSectorsData = new PojoSectorsData();
            pojoSectorsData.setId("3");
            pojoSectorsData.setTitle("Finance & Economy");
            pojoSectorsData.setBg_color(getColor(myColorMap.get(pojoSectorsData.getId())));
            pojoSectorsDataList.add(pojoSectorsData);

            pojoSectorsData = new PojoSectorsData();
            pojoSectorsData.setId("4");
            pojoSectorsData.setTitle("Infrastructure & Resources");
            pojoSectorsData.setBg_color(getColor(myColorMap.get(pojoSectorsData.getId())));
            pojoSectorsDataList.add(pojoSectorsData);

            pojoSectorsData = new PojoSectorsData();
            pojoSectorsData.setId("5");
            pojoSectorsData.setTitle("Technology & Governance");
            pojoSectorsData.setBg_color(getColor(myColorMap.get(pojoSectorsData.getId())));
            pojoSectorsDataList.add(pojoSectorsData);

            pojoSectorsData = new PojoSectorsData();
            pojoSectorsData.setId("6");
            pojoSectorsData.setTitle("Foreign & Security");
            pojoSectorsData.setBg_color(getColor(myColorMap.get(pojoSectorsData.getId())));
            pojoSectorsDataList.add(pojoSectorsData);

            pojoSectorsData = new PojoSectorsData();
            pojoSectorsData.setId("7");
            pojoSectorsData.setTitle("Human Resources");
            pojoSectorsData.setBg_color(getColor(myColorMap.get(pojoSectorsData.getId())));
            pojoSectorsDataList.add(pojoSectorsData);

            List<PojoMinDeptData> pojoMinDeptDataList = new ArrayList<>();
            String[] ministry = {"Ministry of Agriculture and Farmers Welfare", "Ministry of AYUSH", "Ministry of Chemicals and Fertilizers", "Ministry of Civil Aviation", "Ministry of Coal", "Ministry of Commerce and Industry", "Ministry of Communications", "Ministry of Consumer Affairs, Food and Public Distribution", "Ministry of Corporate Affairs", "Ministry of Culture", "Ministry of Defence", "Ministry of Development of North Eastern Region", "Ministry of Earth Sciences", "Ministry of Electronics and Information Technology", "Ministry of Environment, Forest and Climate Change", "Ministry of External Affairs", "Ministry of Finance", "Ministry of Fisheries, Animal Husbandry and Dairying", "Ministry of Food Processing Industries", "Ministry of Health and Family Welfare", "Ministry of Heavy Industries", "Ministry of Home Affairs", "Ministry of Housing and Urban Affairs", "Ministry of Education", "Ministry of Information and Broadcasting", "Ministry of Jal Shakti", "Ministry of Labour and Employment", "Ministry of Law and Justice", "Ministry of Micro, Small and Medium Enterprises", "Ministry of Mines", "Ministry of Minority Affairs", "Ministry of New and Renewable Energy", "Ministry of Panchayati Raj", "Ministry of Parliamentary Affairs", "Ministry of Personnel, Public Grievances and Pensions", "Ministry of Petroleum and Natural Gas", "Ministry of Power", "Ministry of Railways", "Ministry of Road Transport and Highways", "Ministry of Rural Development", "Ministry of Science and Technology", "Ministry of Ports, Shipping and Waterways", "Ministry of Skill Development and Entrepreneurship", "Ministry of Social Justice and Empowerment", "Ministry of Statistics and Programme Implementation", "Ministry of Steel", "Ministry of Textiles", "Ministry of Tourism", "Ministry of Tribal Affairs", "Ministry of Women and Child Development", "Ministry of Youth Affairs and Sports", "Cabinet Secretariat", "Department of Space ", "Department of Atomic Energy", "Ministry of Planning", "National Security Council Secretariat", "NITI Aayog", "Prime Minister Office"};

            for (int i = 0; i < ministry.length; i++) {
                PojoMinDeptData pojoMinDeptData = new PojoMinDeptData();
                pojoMinDeptData.setId(String.valueOf(i + 1));
                pojoMinDeptData.setTitle(ministry[i]);
                pojoMinDeptDataList.add(pojoMinDeptData);
            }

            PojoSchemesFilterData pojo = new PojoSchemesFilterData();
            pojo.setSectorsVisible(true);
            pojo.setPage_key(PAGE_SECTORS);
            pojo.setPojoSectorsDataList(pojoSectorsDataList);
            pojoDashboardData.add(pojo);

            pojo = new PojoSchemesFilterData();
            pojo.setMinDeptVisible(false);
            pojo.setPage_key(PAGE_MIN_DEPT);
            pojo.setPojoMinDeptDataSearchList(pojoMinDeptDataList);
            pojo.setPojoMinDeptDataList(pojoMinDeptDataList);
            pojoDashboardData.add(pojo);

            observableListFilter.addAll(pojoDashboardData);
        }*/
    }

    public void setUpNoData() {
        if (schemesListFiltered.isEmpty()) {
            observerIsNoRecords.set(true);
        } else {
            observerIsNoRecords.set(false);
        }
    }

    public void clearAll() {
        sectorsIDs.clear();
        minDeptIDs.clear();
        if (observableListFilter.size() > 0) {
            for (int i = 0; i < observableListFilter.size(); i++) {
                List<PojoSectorsData> pojoSectorsDataList = observableListFilter.get(i).getPojoSectorsDataList();
                if (pojoSectorsDataList != null) {
                    for (int j = 0; j < pojoSectorsDataList.size(); j++) {
                        if (pojoSectorsDataList.get(j).isIs_selected()) {
                            pojoSectorsDataList.get(j).setIs_selected(false);
                        }
                    }
                }

                List<PojoMinDeptData> deptDataList = observableListFilter.get(i).getPojoMinDeptDataList();
                if (deptDataList != null) {
                    for (int j = 0; j < deptDataList.size(); j++) {
                        if (deptDataList.get(j).isIs_selected()) {
                            deptDataList.get(j).setIs_selected(false);
                        }
                    }
                }
            }
        }
    }

    public void resetBottomDialog() {
        if (observableListFilter.size() > 0) {
            for (int i = 0; i < observableListFilter.size(); i++) {
                List<PojoSectorsData> pojoSectorsDataList = observableListFilter.get(i).getPojoSectorsDataList();
                if (pojoSectorsDataList != null) {
                    for (int j = 0; j < pojoSectorsDataList.size(); j++) {
                        pojoSectorsDataList.get(j).setIs_selected(false);
                        for (int k = 0; k < sectorsIDs.size(); k++) {
                            if (sectorsIDs.get(k).equalsIgnoreCase(pojoSectorsDataList.get(j).getId())) {
                                pojoSectorsDataList.get(j).setIs_selected(true);
                            }
                        }
                    }
                }

                List<PojoMinDeptData> deptDataList = observableListFilter.get(i).getPojoMinDeptDataList();
                if (deptDataList != null) {
                    for (int j = 0; j < deptDataList.size(); j++) {
                        deptDataList.get(j).setIs_selected(false);
                        for (int k = 0; k < minDeptIDs.size(); k++) {
                            if (minDeptIDs.get(k).equalsIgnoreCase(deptDataList.get(j).getId())) {
                                deptDataList.get(j).setIs_selected(true);
                            }
                        }
                    }
                }
            }
        }
    }

    public void setSelectFilter() {
        sectorsIDs.clear();
        minDeptIDs.clear();
        if (observableListFilter.size() > 0) {
            for (int i = 0; i < observableListFilter.size(); i++) {
                List<PojoSectorsData> pojoSectorsDataList = observableListFilter.get(i).getPojoSectorsDataList();
                if (pojoSectorsDataList != null) {
                    for (int j = 0; j < pojoSectorsDataList.size(); j++) {
                        if (pojoSectorsDataList.get(j).isIs_selected()) {
                            sectorsIDs.add(pojoSectorsDataList.get(j).getId());
                        }
                    }
                }

                List<PojoMinDeptData> deptDataList = observableListFilter.get(i).getPojoMinDeptDataList();
                if (deptDataList != null) {
                    for (int j = 0; j < deptDataList.size(); j++) {
                        if (deptDataList.get(j).isIs_selected()) {
                            minDeptIDs.add(deptDataList.get(j).getId());
                        }
                    }
                }
            }
        }
    }


    public void callStateAPI() {
        if (isInternetConnected()) {
            String uniqueId = Utils.getUniqueIDWhitRandomString();
            //observerProgressBar.set(true);
            compositeDisposable.add(mApiHelper.getState("", uniqueId, getToken(uniqueId))
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(pojo -> {
                        if (isValidApiResponse(pojo)) {
                            if (pojo.getPojoPlaces() != null) {
                                observableState.clear();
                                observableState.add(new PojoPlace(mResourceProvider.getString(R.string.text_select_state)));
                                if (pojo.getPojoPlaces().size() > 0) {
                                    for (PojoPlace data : pojo.getPojoPlaces()) {
                                        if (data != null) {
                                            data.decryptData(uniqueId);
                                        }
                                    }
                                    observableState.addAll(pojo.getPojoPlaces());
                                } /*else  {
                                    handleApiErrorMessage(pojo);
                                }*/
                            } else {
                                handleApiErrorMessage(pojo);
                            }
                        }
                        //observerProgressBar.set(false);
                    }, throwable -> {
                        //observerProgressBar.set(false);
                        handleApiErrorMessage(throwable.getMessage());
                        Logger.e("throwable", throwable.getMessage());
                    }));
        }
    }

    public void callDistrictAPI() {
        if (isInternetConnected(R.string.message_noconnection)) {
            String uniqueId = Utils.getUniqueIDWhitRandomString();
            observerProgressBar.set(true);
            compositeDisposable.add(mApiHelper.getDistrict("", uniqueId,
                    getToken(uniqueId),
                    getEncryptedString(stateId.getTrimmed(), uniqueId))
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(pojo -> {
                        if (isValidApiResponse(pojo)) {
                            if (pojo.getPojoPlaces() != null) {
                                observableDistrict.clear();
                                observableDistrict.add(new PojoPlace(mResourceProvider.getString(R.string.text_select_district)));
                                if (pojo.getPojoPlaces().size() > 0) {
                                    for (PojoPlace data : pojo.getPojoPlaces()) {
                                        if (data != null) {
                                            data.decryptData(uniqueId);
                                        }
                                    }

                                    observableDistrict.addAll(pojo.getPojoPlaces());
                                } /*else  {
                                    handleApiErrorMessage(pojo);
                                }*/
                            } else {
                                handleApiErrorMessage(pojo);
                            }
                        }
                        observerProgressBar.set(false);
                    }, throwable -> {
                        observerProgressBar.set(false);
                        handleApiErrorMessage(throwable.getMessage());
                        Logger.e("throwable", throwable.getMessage());
                    }));
        }
    }

    public void callDashboardAPI() {
        observerProgressBar.set(true);
        callDashboardWS();
    }

    public void callDashboardWS() {
        if (isInternetConnected(R.string.message_noconnection)) {
            String randomDeviceId = Utils.getUniqueIDWhitRandomString();
            String uniqueId = "";
            String state_Id = (!TextUtils.isEmpty(stateId.getTrimmed()) ? stateId.getTrimmed() : "");
            String district_Id = (!TextUtils.isEmpty(districtId.getTrimmed()) ? districtId.getTrimmed() : "");
            String search = (!TextUtils.isEmpty(searchText.getTrimmed()) ? searchText.getTrimmed() : "");
            String ministryDepartmentID = TextUtils.join(",", minDeptIDs);
            String sectorID = TextUtils.join(",", sectorsIDs);

            compositeDisposable.add(mApiHelper.getDashboard("", randomDeviceId,
                    getToken(uniqueId), uniqueId,
                    getEncryptedString(MINISTRY_ID, randomDeviceId),
                    getEncryptedString(state_Id, randomDeviceId),
                    getEncryptedString(district_Id, randomDeviceId),
                    getEncryptedString(search, randomDeviceId),
                    getEncryptedString(ministryDepartmentID, randomDeviceId),
                    getEncryptedString(sectorID, randomDeviceId))
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(pojo -> {
                        if (isValidApiResponse(pojo)) {
                            if (pojo.getSchemesList() != null) {
                                schemesList.clear();
                                schemesListFiltered.clear();
                                if (pojo.getSchemesList().size() > 0) {

                                    for (PojoSchemes pojoSchemes : pojo.getSchemesList()) {
                                        if (pojoSchemes != null) {
                                            pojoSchemes.decryptData(randomDeviceId);
                                            pojoSchemes.setUpColorData();
                                        }
                                    }

                                    schemesList.addAll(pojo.getSchemesList());
                                    schemesListFiltered.addAll(pojo.getSchemesList());
                                } /*else  {
                                    handleApiErrorMessage(pojo);
                                }*/
                            } else {
                                handleApiErrorMessage(pojo);
                            }
                        }
                        setUpNoData();
                        observerProgressBar.set(false);
                        observableSwipeRefreshLayout.set(false);
                    }, throwable -> {
                        setUpNoData();
                        observerProgressBar.set(false);
                        observableSwipeRefreshLayout.set(false);
                        handleApiErrorMessage(throwable.getMessage());
                        Logger.e("throwable", throwable.getMessage());
                    }));
        } else {
            observerProgressBar.set(false);
            observableSwipeRefreshLayout.set(false);
            setUpNoData();
        }
    }


    public void callFilterAPI() {
        if (isInternetConnected()) {
            String uniqueId = Utils.getUniqueIDWhitRandomString();
            compositeDisposable.add(mApiHelper.getFilter("", uniqueId,
                    getToken(uniqueId),
                    getEncryptedString(MINISTRY_ID, uniqueId))
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(pojo -> {
                        if (isValidApiResponse(pojo)) {

                            observableListFilter.clear();
                            List<PojoSchemesFilterData> pojoDashboardData = new ArrayList<>();

                            if (pojo.getSector() == null && pojo.getMinistry() == null) {
                                handleApiErrorMessage(pojo);
                            }

                            if (pojo.getSector() != null && !pojo.getSector().isEmpty()) {
                                for (PojoSectorsData sectorsData : pojo.getSector()) {
                                    if (sectorsData != null) {
                                        sectorsData.decryptData(uniqueId);
                                        sectorsData.setUpColorData();
                                    }
                                }

                                PojoSchemesFilterData filterData = new PojoSchemesFilterData();
                                filterData.setSectorsVisible(true);
                                filterData.setPage_key(PAGE_SECTORS);
                                filterData.setPojoSectorsDataList(pojo.getSector());
                                pojoDashboardData.add(filterData);
                            }

                            if (pojo.getMinistry() != null && !pojo.getMinistry().isEmpty()) {
                                for (PojoMinDeptData pojoMinDeptData : pojo.getMinistry()) {
                                    if (pojoMinDeptData != null) {
                                        pojoMinDeptData.decryptData(uniqueId);
                                    }
                                }

                                PojoSchemesFilterData filterData = new PojoSchemesFilterData();
                                filterData.setMinDeptVisible(false);
                                filterData.setPage_key(PAGE_MIN_DEPT);
                                filterData.setPojoMinDeptDataSearchList(pojo.getMinistry());
                                filterData.setPojoMinDeptDataList(pojo.getMinistry());
                                pojoDashboardData.add(filterData);
                            }
                            observableListFilter.addAll(pojoDashboardData);
                        }
                        //observerProgressBar.set(false);
                    }, throwable -> {
                        //observerProgressBar.set(false);
                        handleApiErrorMessage(throwable.getMessage());
                        Logger.e("throwable", throwable.getMessage());
                    }));
        } /*else {
            observerProgressBar.set(false);
        }*/
    }

    public void onClickDistrictSpinner() {
        if (TextUtils.isEmpty(stateId.get())) {
            observerSnackBarInt.set(R.string.text_select_state_first);
        }
    }

    public OnScrollListener onScrollListener = new OnScrollListener() {
        @Override
        public void onScrollStateChanged(Boolean isEnabled) {
            observableIsSwipeEnable.set(isEnabled);
        }

        @Override
        public void onScrolled(int totalItemCount, int pastVisibleItems) {

        }
    };

}
