package com.org.prayas.business_logic.interfaces;

import io.reactivex.Single;

/**
 * Created by Naria Sachin on Mar, 24 2021 18:42.
 */

public interface ListenerListSupport<D> {
    Single<D> getApiRequest(int pageIndex);

    void onApiResult(D successApiResult);
}
