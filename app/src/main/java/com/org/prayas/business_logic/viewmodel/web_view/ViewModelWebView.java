package com.org.prayas.business_logic.viewmodel.web_view;

import com.org.prayas.business_logic.viewmodel.ViewModelBase;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class ViewModelWebView extends ViewModelBase {

    @Inject
    public ViewModelWebView() {
    }
}
