package com.org.prayas.business_logic.pojo;

import com.google.gson.annotations.SerializedName;
import com.org.prayas.business_logic.pojo.base.PojoResponseBase;

import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoStateData extends PojoResponseBase {
    @SerializedName("list")
    private List<PojoPlace> pojoPlaces;
}
