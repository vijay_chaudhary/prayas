package com.org.prayas.business_logic.pojo.base;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vijay Aug, 05 2021.
 */

public class PojoResponseBase {
    @SerializedName("message")
    private String message;
    @SerializedName("error_log")
    private String error_log;
    @SerializedName(value = "resultflag", alternate = {"resultFlag"})
    private int resultflag;

    @SerializedName("image_url")
    private String image_url;


    public String getError_log() {
        return error_log;
    }

    public void setError_log(String error_log) {
        this.error_log = error_log;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getResultflag() {
        return resultflag;
    }

    public void setResultflag(int resultflag) {
        this.resultflag = resultflag;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
