package com.org.prayas.business_logic.preferences;

import static com.org.prayas.business_logic.preferences.PrefKeys.prefBjp;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class UtilsSharedPreferences {

    private final SharedPreferences mPreferences;

    @Inject
    public UtilsSharedPreferences(Context context) {
        mPreferences = context.getSharedPreferences(prefBjp, Context.MODE_PRIVATE);
    }

    public boolean getBoolean(String prefName) {
        return mPreferences.getBoolean(prefName, false);
    }

    public boolean getBooleanDefault(String prefName, boolean defaultValue) {
        return mPreferences.getBoolean(prefName, defaultValue);
    }

    public void setBoolean(String prefName, boolean value) {
        mPreferences.edit().putBoolean(prefName, value).apply();
    }

    public String getString(String prefName) {
        return mPreferences.getString(prefName, "");
    }

    public String getStringDefault(String prefName, String defaultValue) {
        return mPreferences.getString(prefName, defaultValue);
    }

    public void setString(String prefName, String value) {
        mPreferences.edit().putString(prefName, value).apply();
    }

    public int getInteger(String prefName) {
        return mPreferences.getInt(prefName, 0);
    }

    public int getIntegerDefault(String prefName, int defaultValue) {
        return mPreferences.getInt(prefName, defaultValue);
    }

    public void setInteger(String prefName, int value) {
        mPreferences.edit().putInt(prefName, value).apply();
    }

    public long getLong(String prefName) {
        return mPreferences.getLong(prefName, 0L);
    }

    public long getLongDefault(String prefName, long defaultValue) {
        return mPreferences.getLong(prefName, defaultValue);
    }

    public void setLong(String prefName, long value) {
        mPreferences.edit().putLong(prefName, value).apply();
    }

    public void clearAllPreferences() {
        mPreferences.edit().clear().apply();
    }
}
