package com.org.prayas.business_logic.pojo;

import static com.org.prayas.utils.Utils.getDecryptedString;

import com.google.gson.annotations.SerializedName;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoKpIValue {
    @SerializedName("indicator")
    private String indicator;

    @SerializedName("top1_State")
    private String top1State;

    @SerializedName("top2_State")
    private String top2State;

    @SerializedName("top3_State")
    private String top3State;

    @SerializedName("top4_State")
    private String top4State;

    @SerializedName("top5_State")
    private String top5State;

    @SerializedName("unique_Id")
    private String uniqueId;

    @SerializedName("scheme_Name")
    private String schemeName;

    public void decryptData(String uniqueID) {
        indicator = getDecryptedString(indicator, uniqueID);
        top1State = getDecryptedString(top1State, uniqueID);
        top2State = getDecryptedString(top2State, uniqueID);
        top3State = getDecryptedString(top3State, uniqueID);
        top4State = getDecryptedString(top4State, uniqueID);
        top5State = getDecryptedString(top5State, uniqueID);
        uniqueId = getDecryptedString(uniqueId, uniqueID);
        schemeName = getDecryptedString(schemeName, uniqueID);
    }
}
