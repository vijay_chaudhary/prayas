package com.org.prayas.business_logic.viewmodel.main;

import static com.org.prayas.business_logic.preferences.PrefKeys.prefLoginUserId;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefUserEmail;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefUserImage;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefUserMobile;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefUserName;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;

import com.org.prayas.R;
import com.org.prayas.business_logic.interactors.ObservableString;
import com.org.prayas.business_logic.pojo.base.PojoMenu;
import com.org.prayas.business_logic.viewmodel.ViewModelBase;
import com.org.prayas.utils.ResourceProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class ViewModelMain extends ViewModelBase {

    public ObservableInt observableLogout = new ObservableInt();
    public ObservableString observableUserId = new ObservableString();
    public ObservableString observableName = new ObservableString();
    public ObservableString observableEmail = new ObservableString();
    public ObservableString observableMobile = new ObservableString();
    public ObservableString observableUserType = new ObservableString("");
    public ObservableString observableImageURL = new ObservableString();
    public ObservableBoolean isShowToolbar = new ObservableBoolean(true);

    public List<PojoMenu> headerList = new ArrayList<>();
    public HashMap<PojoMenu, List<PojoMenu>> childList = new HashMap<>();

    @Inject
    public ViewModelMain(ResourceProvider resourceProvider) {
        this.mResourceProvider = resourceProvider;
    }

    public void setNavBanner() {
        observableUserId.set(mPreferences.getString(prefLoginUserId));
        observableName.set(mPreferences.getString(prefUserName));
        observableEmail.set(mPreferences.getString(prefUserEmail));
        observableMobile.set(mPreferences.getString(prefUserMobile));
        observableImageURL.set(mPreferences.getString(prefUserImage));
        /*if (!TextUtils.isEmpty(mPreferences.getString(prefUserType))) {
            observableUserType.set(
                    mPreferences.getString(prefUserType).equalsIgnoreCase(mResourceProvider.getString(R.string.user_type_m)) ?
                            mResourceProvider.getString(R.string.text_money_lender) :
                            mResourceProvider.getString(R.string.text_society)
            );
        }*/
    }

    private void addMenuItem(int menuName, int resId) {
        PojoMenu menuModel = getMenuItem(menuName, resId, false);
        headerList.add(menuModel);
    }

    private void addChildMenuItem(int menuName, int resId, List<PojoMenu> subList) {
        PojoMenu menuModel = getMenuItem(menuName, resId, true);
        headerList.add(menuModel);

        childList.put(menuModel, subList);
    }

    private PojoMenu getMenuItem(int menuName, int resId, boolean hasChildren) {
        return new PojoMenu(menuName, resId, hasChildren);
    }

    public void prepareMenuData() {
        headerList.clear();
        childList.clear();

        // dashboard
        addMenuItem(R.string.drawer_dashboard, R.drawable.ic_vector_menu_plus);
        addMenuItem(R.string.text_schemes, R.drawable.ic_vector_menu_plus);
    }
}
