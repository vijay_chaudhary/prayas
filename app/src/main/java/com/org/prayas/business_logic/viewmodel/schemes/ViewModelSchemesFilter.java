package com.org.prayas.business_logic.viewmodel.schemes;

import androidx.databinding.ObservableArrayList;
import androidx.lifecycle.MutableLiveData;

import com.org.prayas.business_logic.pojo.PojoSchemesFilterData;
import com.org.prayas.business_logic.viewmodel.ViewModelBase;
import com.org.prayas.utils.ResourceProvider;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class ViewModelSchemesFilter extends ViewModelBase {

    public ObservableArrayList<PojoSchemesFilterData> observableListFilter = new ObservableArrayList<>();

    public MutableLiveData<String> callBusAreaBottomDialog = new MutableLiveData<>();

    public MutableLiveData<String> getCallBusAreaBottomDialog() {
        return callBusAreaBottomDialog;
    }

    @Inject
    public ViewModelSchemesFilter(ResourceProvider resourceProvider) {
        this.mResourceProvider = resourceProvider;
    }

}
