package com.org.prayas.business_logic.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

public @interface AppQualifier {

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    @interface BaseURL {}

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    @interface NetworkTimeout {}

}
