package com.org.prayas.business_logic.interfaces;

/**
 * Created by Naria Sachin on Mar, 22 2021 19:51.
 */

public interface ViewPageChangeListener {
    void onPageChange(int position);
}
