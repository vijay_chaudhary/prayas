package com.org.prayas.business_logic.pojo;

import static com.org.prayas.utils.Utils.getDecryptedString;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class PojoPlace {
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title = "";

    public PojoPlace(String title) {
        this.title = title;
    }

    public PojoPlace(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NonNull
    @Override
    public String toString() {
        return title;
    }

    public void decryptData(String uniqueId) {
        id = getDecryptedString(id, uniqueId);
        title = getDecryptedString(title, uniqueId);
    }
}
