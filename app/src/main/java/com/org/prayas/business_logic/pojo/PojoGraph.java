package com.org.prayas.business_logic.pojo;

import static com.org.prayas.utils.Utils.getDecryptedString;
import static com.org.prayas.utils.Utils.getFirstThreeChar;
import static com.org.prayas.utils.Utils.getLastTwoChar;
import static com.org.prayas.utils.Utils.stringToFloat;
import static com.org.prayas.utils.Utils.stringToInt;

import com.google.gson.annotations.SerializedName;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoGraph {
    @SerializedName("value")
    private String value;

    @SerializedName("month_Name")
    private String monthName;

    @SerializedName("year")
    private String year;

    @SerializedName("month_sort")
    private String monthSortData;

    private String xAxisData;
    private float yAxisData;
    private Integer monthSort;

    public void setUpXValue() {
        xAxisData = getFirstThreeChar(monthName) + " - " + getLastTwoChar(year);
    }

    public void decryptData(String uniqueID) {
        yAxisData = stringToFloat(getDecryptedString(value, uniqueID));
        monthName = getDecryptedString(monthName, uniqueID);
        year = getDecryptedString(year, uniqueID);
        monthSort = stringToInt(getDecryptedString(monthSortData, uniqueID));
    }
}
