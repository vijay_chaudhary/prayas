package com.org.prayas.business_logic.interfaces;

import android.view.View;

/**
 * Created by Naria Sachin on Jan, 17 2020 19:41.
 */

public interface GeneralListener {
    void onClick(View view);
}
