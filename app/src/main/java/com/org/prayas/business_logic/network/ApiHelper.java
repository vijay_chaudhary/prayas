package com.org.prayas.business_logic.network;

import com.org.prayas.business_logic.pojo.PojoDashboardData;
import com.org.prayas.business_logic.pojo.PojoFilterData;
import com.org.prayas.business_logic.pojo.PojoHomeData;
import com.org.prayas.business_logic.pojo.PojoMonthGraphData;
import com.org.prayas.business_logic.pojo.PojoStateData;
import com.org.prayas.business_logic.pojo.PojoStateGraphData;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Vijay on Aug, 05 Aug.
 */

public interface ApiHelper {

    @FormUrlEncoded
    @POST("home")
    Single<PojoHomeData> getHome(@Field("dummy") String dummy,
                                 @Query("deviceID") String deviceID);

    @FormUrlEncoded
    @POST("getstate")
    Single<PojoStateData> getState(@Field("dummy") String dummy,
                                   @Query("deviceID") String deviceID,
                                   @Query("token") String token);

    @FormUrlEncoded
    @POST("getdistrict")
    Single<PojoStateData> getDistrict(@Field("dummy") String dummy,
                                      @Query("deviceID") String deviceID,
                                      @Query("token") String token,
                                      @Query("stateid") String stateid);

    @FormUrlEncoded
    @POST("dashboard")
    Single<PojoDashboardData> getDashboard(@Field("dummy") String dummy,
                                           @Query("deviceID") String deviceID,
                                           @Query("token") String token,
                                           @Query("uniqueId") String uniqueId,
                                           @Query("ministryId") String ministryId,
                                           @Query("stateId") String stateId,
                                           @Query("districtId") String districtId,
                                           @Query("search") String search,
                                           @Query("MinistryDepartmentID") String MinistryDepartmentID,
                                           @Query("SectorID") String SectorID);

    @FormUrlEncoded
    @POST("getfilter")
    Single<PojoFilterData> getFilter(@Field("dummy") String dummy,
                                     @Query("deviceID") String deviceID,
                                     @Query("token") String token,
                                     @Query("ministry_id") String ministry_id);

    @FormUrlEncoded
    @POST("getstategraph")
    Single<PojoStateGraphData> getStateGraph(@Field("dummy") String dummy,
                                             @Query("deviceID") String deviceID,
                                             @Query("ministryId") String ministryId,
                                             @Query("schemeId") String schemeId);

    @FormUrlEncoded
    @POST("getmonthgraph")
    Single<PojoMonthGraphData> getMonthGraph(@Field("dummy") String dummy,
                                             @Query("deviceID") String deviceID,
                                             @Query("ministryId") String ministryId,
                                             @Query("scheme_id") String scheme_id,
                                             @Query("stateId") String stateId,
                                             @Query("districtId") String districtId);

}
