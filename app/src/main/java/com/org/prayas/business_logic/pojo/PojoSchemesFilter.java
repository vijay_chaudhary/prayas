package com.org.prayas.business_logic.pojo;

import com.google.gson.annotations.SerializedName;
import com.org.prayas.business_logic.pojo.base.PojoResponseBase;

import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Vijay Aug, 05 2021.
 */
@Getter
@Setter(AccessLevel.PRIVATE)
public class PojoSchemesFilter extends PojoResponseBase {

    @SerializedName("filterslist")
    private List<PojoSchemesFilterData> pojoSchemesFilterData;

}
