package com.org.prayas.business_logic.pojo;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.google.gson.annotations.SerializedName;
import com.org.prayas.BR;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Prashant Rajput on 16-12-2021.
 */
@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoSchemesFilterData extends BaseObservable {
    @SerializedName("page_key")
    private String page_key = null;

    @SerializedName("sectors")
    List<PojoSectorsData> pojoSectorsDataList = null;
    boolean isSectorsVisible;

    @SerializedName("min_dept")
    List<PojoMinDeptData> pojoMinDeptDataList = null;

    List<PojoMinDeptData> pojoMinDeptDataSearchList = null;
    boolean isMinDeptVisible;

   /* @Bindable
    public List<PojoMinDeptData> getPojoMinDeptDataSearchList() {
        return pojoMinDeptDataSearchList;
    }

    public void setPojoMinDeptDataSearchList(List<PojoMinDeptData> pojoMinDeptDataSearchList) {
        this.pojoMinDeptDataSearchList = pojoMinDeptDataSearchList;
        notifyPropertyChanged(BR.pojoMinDeptDataSearchList);
    }*/

    @Bindable
    public boolean isSectorsVisible() {
        return isSectorsVisible;
    }

    public void setSectorsVisible(boolean sectorsVisible) {
        isSectorsVisible = sectorsVisible;
        notifyPropertyChanged(BR.sectorsVisible);
    }

    @Bindable
    public boolean isMinDeptVisible() {
        return isMinDeptVisible;
    }

    public void setMinDeptVisible(boolean minDeptVisible) {
        isMinDeptVisible = minDeptVisible;
        notifyPropertyChanged(BR.minDeptVisible);
    }
}
