package com.org.prayas.business_logic.interfaces;

/**
 * Created by Prashant Rajput on 02-03-2021.
 */
public interface ViewPageListener {
    void onItemMove(int position);
    void onItemNavigateTo(int position);
}
