package com.org.prayas.business_logic.interactors;

import static com.org.prayas.utils.ConstantCodes.INVALIDATION_POSITION;

/**
 * Created by Vijay Aug, 05 2021.
 */

public class NotifySpinnerPosition {
    private int viewFlag;
    private NotifyPosition notifyPosition;
    private int spinnerPosition = INVALIDATION_POSITION;

    public void setSpinnerPosition(int spinnerPosition) {
        this.spinnerPosition = spinnerPosition;
    }

    public void setSpinnerPositionNotify(int spinnerPosition) {
        if (notifyPosition != null) {
            notifyPosition.onChangePosition(spinnerPosition, viewFlag);
        }
    }

    public void setSpinnerPositionNotifyAll(int spinnerPosition) {
        if (this.spinnerPosition == spinnerPosition) return;

        this.spinnerPosition = spinnerPosition;
        if (notifyPosition != null) {
            notifyPosition.onChangePosition(spinnerPosition, viewFlag);
        }
    }

    public int getSpinnerPosition() {
        return spinnerPosition;
    }

    public void setNotifyPositionListener(NotifyPosition notifyPosition, int viewFlag) {
        this.notifyPosition = notifyPosition;
        this.viewFlag = viewFlag;
    }

    public interface NotifyPosition {
        void onChangePosition(int position, int viewFlag);
    }
}
