package com.org.prayas.business_logic.pojo;

import com.google.gson.annotations.SerializedName;
import com.org.prayas.business_logic.pojo.base.PojoResponseBase;

import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoFilterData extends PojoResponseBase {
    @SerializedName("sector")
    private List<PojoSectorsData> sector = null;

    @SerializedName("ministry")
    private List<PojoMinDeptData> ministry = null;
}
