package com.org.prayas.business_logic.interfaces;

import androidx.cardview.widget.CardView;

/**
 * Created by Naria Sachin on Mar, 01 2021 19:26.
 */

public interface CardAdapter {
    int MAX_ELEVATION_FACTOR = 8;

    float getBaseElevation();

    CardView getCardViewAt(int position);

    int getCount();
}
