package com.org.prayas.business_logic.pojo;

import static com.org.prayas.utils.Utils.getDecryptedString;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.google.gson.annotations.SerializedName;
import com.org.prayas.BR;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Prashant Rajput on 16-12-2021.
 */
@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoMinDeptData extends BaseObservable {
    @SerializedName("id")
    private String id = null;

    @SerializedName("title")
    private String title = null;

    //@SerializedName("is_selected")
    private boolean is_selected;

    @Bindable
    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
        notifyPropertyChanged(BR.is_selected);
    }

    public void decryptData(String uniqueId) {
        id = getDecryptedString(id, uniqueId);
        title = getDecryptedString(title, uniqueId);
    }
}
