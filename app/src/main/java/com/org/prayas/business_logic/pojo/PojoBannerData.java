package com.org.prayas.business_logic.pojo;

import static com.org.prayas.utils.Utils.getDecryptedString;

import androidx.cardview.widget.CardView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Prashant Rajput on 16-12-2021.
 */
@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoBannerData {

    @SerializedName("scheme_logo_name")
    private String schemeLogoName;

    @SerializedName("scheme_title")
    private String schemeTitle;

    @Setter(AccessLevel.PUBLIC)
    transient private CardView cardView;

    public void decryptData(String uniqueId) {
        schemeLogoName = getDecryptedString(schemeLogoName, uniqueId);
        schemeTitle = getDecryptedString(schemeTitle, uniqueId);
    }
}
