package com.org.prayas.business_logic.pojo;

import static com.org.prayas.utils.Utils.getDecryptedString;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoHomeInfo {
    @SerializedName("sector")
    private String sector;

    @SerializedName("minDeptt")
    private String minDeptt;

    @SerializedName("schemes")
    private String schemes;

    @SerializedName("indicator")
    private String indicator;

    @SerializedName("logo")
    private List<PojoBannerData> logo = null;

    public void decryptData(String uniqueId) {
        sector = getDecryptedString(sector, uniqueId);
        minDeptt = getDecryptedString(minDeptt, uniqueId);
        schemes = getDecryptedString(schemes, uniqueId);
        indicator = getDecryptedString(indicator, uniqueId);

        if (logo != null && !logo.isEmpty()) {
            for (PojoBannerData data : logo) {
                if (data != null) {
                    data.decryptData(uniqueId);
                }
            }
        }
    }
}
