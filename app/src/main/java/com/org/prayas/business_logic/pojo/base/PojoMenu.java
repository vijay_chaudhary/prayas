package com.org.prayas.business_logic.pojo.base;

public class PojoMenu {
    private int resId;
    private int name;
    private boolean hasChildren = false;
    private boolean isSelected;

    public PojoMenu(int menuName, int resId, boolean hasChildren) {
        this.name = menuName;
        this.resId = resId;
        this.hasChildren = hasChildren;
    }


    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public boolean isHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
