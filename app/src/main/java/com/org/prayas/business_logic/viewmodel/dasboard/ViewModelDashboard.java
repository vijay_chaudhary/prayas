package com.org.prayas.business_logic.viewmodel.dasboard;

import android.text.TextUtils;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;

import com.org.prayas.R;
import com.org.prayas.business_logic.interactors.ObservableString;
import com.org.prayas.business_logic.pojo.PojoBannerData;
import com.org.prayas.business_logic.pojo.PojoDashboardServiceData;
import com.org.prayas.business_logic.pojo.PojoHomeInfo;
import com.org.prayas.business_logic.viewmodel.ViewModelBase;
import com.org.prayas.utils.Logger;
import com.org.prayas.utils.ResourceProvider;
import com.org.prayas.utils.Utils;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class ViewModelDashboard extends ViewModelBase {

    public ObservableBoolean observableIsLogin = new ObservableBoolean();
    public ObservableString observableUrl = new ObservableString();
    public ObservableArrayList<PojoBannerData> observableListBanner = new ObservableArrayList<>();
    public ObservableArrayList<PojoDashboardServiceData> observableListService = new ObservableArrayList<>();

    @Inject
    public ViewModelDashboard(ResourceProvider resourceProvider) {
        this.mResourceProvider = resourceProvider;

        setBannerData();
    }

    public void setBannerData() {
        /*PojoBannerData pojo=new PojoBannerData();
        pojo.setImage_url("https://i.ibb.co/pwF24Vp/logo-center-xxxhdpi.png");
        observableListBanner.add(pojo);

        pojo=new PojoBannerData();
        pojo.setImage_url("https://i.ibb.co/JRhB502/logo-center2-xxxhdpi.png");
        observableListBanner.add(pojo);*/
    }

    public void setDashboardData(PojoHomeInfo homeInfo) {
        boolean isClear = homeInfo == null;
        observableListService.clear();

        PojoDashboardServiceData pojo = new PojoDashboardServiceData();
        pojo.setBackBG(R.drawable.bg_sectors);
        pojo.setIcon(R.drawable.ic_ico_sector);
        pojo.setCountColor(R.color.pink_200);
        pojo.setTitle(R.string.text_sectors);
        pojo.setBottomBG(R.drawable.sector_bg);

        if (!isClear && !TextUtils.isEmpty(homeInfo.getSector())) {
            pojo.setCount(homeInfo.getSector());
        } else {
            pojo.setCount("");
        }
        observableListService.add(pojo);

        pojo = new PojoDashboardServiceData();
        pojo.setBackBG(R.drawable.bg_mini_dept);
        pojo.setIcon(R.drawable.ic_ico_mini_dept);
        pojo.setCountColor(R.color.purple_200);
        pojo.setTitle(R.string.text_mini_dept);
        pojo.setBottomBG(R.drawable.mini_dept_bg);
        if (!isClear && !TextUtils.isEmpty(homeInfo.getMinDeptt())) {
            pojo.setCount(homeInfo.getMinDeptt());
        } else {
            pojo.setCount("");
        }
        observableListService.add(pojo);

        pojo = new PojoDashboardServiceData();
        pojo.setBackBG(R.drawable.bg_schemes);
        pojo.setIcon(R.drawable.ic_ico_schemes);
        pojo.setCountColor(R.color.lime_300);
        pojo.setTitle(R.string.text_schemes);
        pojo.setBottomBG(R.drawable.schemes_bg);
        if (!isClear && !TextUtils.isEmpty(homeInfo.getSchemes())) {
            pojo.setCount(homeInfo.getSchemes());
        } else {
            pojo.setCount("");
        }
        observableListService.add(pojo);

        pojo = new PojoDashboardServiceData();
        pojo.setBackBG(R.drawable.bg_indicators);
        pojo.setIcon(R.drawable.ic_ico_indicator);
        pojo.setCountColor(R.color.blue_300);
        pojo.setTitle(R.string.text_indicators);
        pojo.setBottomBG(R.drawable.indicator_bg);
        if (!isClear && !TextUtils.isEmpty(homeInfo.getIndicator())) {
            pojo.setCount(homeInfo.getIndicator());
        } else {
            pojo.setCount("");
        }
        observableListService.add(pojo);
    }


    public void onRefreshList(boolean isFromSearch) {
        observableSwipeRefreshLayout.set(true);
        callHomeWS();
    }

    public void callHomeAPI() {
        observerProgressBar.set(true);
        callHomeWS();
    }

    public void callHomeWS() {
        if (isInternetConnected(R.string.message_noconnection)) {
            String randomDeviceId = Utils.getUniqueIDWhitRandomString();
            compositeDisposable.add(mApiHelper.getHome("", randomDeviceId)
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(pojo -> {
                        boolean isClear = true;
                        if (isValidApiResponse(pojo)) {
                            if (pojo.getCountData() != null && pojo.getCountData().getHomeInfo() != null) {
                                isClear = false;
                                PojoHomeInfo homeInfo = pojo.getCountData().getHomeInfo();
                                homeInfo.decryptData(randomDeviceId);
                                setDashboardData(homeInfo);
                                observableListBanner.clear();
                                if (homeInfo.getLogo() != null && homeInfo.getLogo().size() > 0) {
                                    observableListBanner.addAll(homeInfo.getLogo());
                                } /*else  {
                                    handleApiErrorMessage(pojo);
                                }*/
                            } else {
                                handleApiErrorMessage(pojo);
                            }
                        }
                        if (isClear) {
                            setDashboardData(null);
                        }
                        setUpNoData();
                        observerProgressBar.set(false);
                        observableSwipeRefreshLayout.set(false);
                    }, throwable -> {
                        observableSwipeRefreshLayout.set(false);
                        observerProgressBar.set(false);
                        setDashboardData(null);
                        setUpNoData();
                        handleApiErrorMessage(throwable.getMessage());
                        Logger.e("throwable", throwable.getMessage());
                    }));
        } else {
            observableSwipeRefreshLayout.set(false);
            observerProgressBar.set(false);
            setDashboardData(null);
            setUpNoData();
        }
    }

    public void setUpNoData() {
        if (observableListBanner.isEmpty()) {
            observerIsNoRecords.set(true);
        } else {
            observerIsNoRecords.set(false);
        }
    }

}
