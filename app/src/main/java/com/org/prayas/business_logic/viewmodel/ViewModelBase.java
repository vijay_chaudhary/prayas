package com.org.prayas.business_logic.viewmodel;

import static com.org.prayas.business_logic.network.NetworkController.PARAMS_LANG;
import static com.org.prayas.business_logic.network.NetworkController.PARAMS_TOKEN;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefLang;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefUserToken;
import static com.org.prayas.utils.ConstantCodes.TOKEN;
import static com.org.prayas.utils.Utils.getEncryptedString;

import android.text.TextUtils;

import androidx.annotation.StringRes;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.org.prayas.R;
import com.org.prayas.business_logic.interactors.ObservableString;
import com.org.prayas.business_logic.network.ApiHelper;
import com.org.prayas.business_logic.network.NetworkHelper;
import com.org.prayas.business_logic.pojo.base.PojoResponseBase;
import com.org.prayas.business_logic.preferences.UtilsSharedPreferences;
import com.org.prayas.business_logic.rx.SchedulerProvider;
import com.org.prayas.utils.ResourceProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Vijay Aug, 05 2021.
 */

public class ViewModelBase extends ViewModel {

    @Inject
    protected ResourceProvider mResourceProvider;

    @Inject
    protected ApiHelper mApiHelper;

    @Inject
    protected NetworkHelper mNetworkHelper;

    @Inject
    protected UtilsSharedPreferences mPreferences;

    @Inject
    protected SchedulerProvider mSchedulerProvider;

    @Inject
    protected CompositeDisposable mCompositeDisposable;

    public ObservableBoolean observerProgressBar = new ObservableBoolean(false);
    public ObservableBoolean observerIsNoRecords = new ObservableBoolean(false);
    public ObservableInt observerSnackBarInt = new ObservableInt();
    public final ObservableString observerSnackBarString = new ObservableString("");

    public ObservableBoolean observableBooleanIsSectorShow = new ObservableBoolean(true);

    public final ObservableBoolean observableSwipeRefreshLayout = new ObservableBoolean(false);
    public final ObservableBoolean observableIsSwipeEnable = new ObservableBoolean(true);

   /* public MutableLiveData<String> callNextButton = new MutableLiveData<>();
    public MutableLiveData<String> callSaveAsDraftButton = new MutableLiveData<>();
    public MutableLiveData<String> callSubmitButton = new MutableLiveData<>();
    public MutableLiveData<Boolean> onSaveAsDraft = new MutableLiveData<>();

    public ObservableBoolean observerIsSubmitted = new ObservableBoolean(false);*/

    public CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final MutableLiveData<String> liveDataTokenExpire = new MutableLiveData<>();

    public MutableLiveData<String> getLiveDataTokenExpire() {
        return liveDataTokenExpire;
    }

    public ViewModelBase() {
    }

    public ViewModelBase(ResourceProvider mResourceProvider) {
        this.mResourceProvider = mResourceProvider;
    }

    public void hideLoadingBars() {
        observerProgressBar.set(false);
    }

    public void hideNoRecordFound() {
        observerIsNoRecords.set(false);
    }

    public boolean isValidApiResponse(PojoResponseBase pojoResponse) {
        if (pojoResponse != null) {
            if (pojoResponse.getResultflag() == 1) {
                return true;
            } else if (pojoResponse.getResultflag() == 2) {
                liveDataTokenExpire.setValue(pojoResponse.getMessage());
                return false;
            } else {
                if (!TextUtils.isEmpty(pojoResponse.getMessage())) {
                    handleApiErrorMessage(pojoResponse.getMessage());
                } else {
                    handleApiErrorMessage(pojoResponse.getError_log());
                }
                return false;
            }
        } else {
            onApiFailure();
            return false;
        }
    }

    public boolean isValidApiResponseWithOutMSG(PojoResponseBase pojoResponse) {
        if (pojoResponse != null) {
            if (pojoResponse.getResultflag() == 1) {
                return true;
            } else if (pojoResponse.getResultflag() == 2) {
                liveDataTokenExpire.setValue(pojoResponse.getMessage());
                return false;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void onApiFailure() {
        observerSnackBarInt.set(R.string.message_something_wrong);
    }

    public void onApiFailure(String message) {
        observerSnackBarString.set(message);
    }

    public void handleApiErrorMessage(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            observerSnackBarString.set(msg);
        } else {
            onApiFailure();
        }
    }

    public void handleApiErrorMessage(PojoResponseBase pojoResponseBase) {
        if (!TextUtils.isEmpty(pojoResponseBase.getMessage())) {
            observerSnackBarString.set(pojoResponseBase.getMessage());
        } else if (!TextUtils.isEmpty(pojoResponseBase.getError_log())) {
            observerSnackBarString.set(pojoResponseBase.getError_log());
        } else {
            onApiFailure();
        }
    }

    /**
     * Param with Language and Id
     */
    protected JSONObject getParamsWithDefaults() {
        JSONObject mJsonObject = new JSONObject();
        try {
            mJsonObject.put(PARAMS_TOKEN, mPreferences.getString(prefUserToken));
            mJsonObject.put(PARAMS_LANG, mPreferences.getString(prefLang));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mJsonObject;
    }

    /**
     * Checking internet connection with message
     */
    public boolean isInternetConnected(@StringRes int stringResMessage) {
        boolean isNetwork = mNetworkHelper.isInternetConnected();
        observerIsNoRecords.set(isNetwork);
        if (isNetwork) {
            hideLoadingBars();
            hideNoRecordFound();
            observerSnackBarInt.set(stringResMessage);
            return false;
        }
        return true;
    }

    protected boolean isInternetConnected(String stringMessage) {
        boolean isNetwork = mNetworkHelper.isInternetConnected();
        observerIsNoRecords.set(!isNetwork);
        if (isNetwork) {
            hideLoadingBars();
            hideNoRecordFound();
            observerSnackBarString.set(stringMessage);
            return false;
        }
        return true;
    }

    protected boolean isInternetConnected() {
        return !mNetworkHelper.isInternetConnected();
    }

    /*
     * Convert to String request
     * */
    protected RequestBody getRequestBody(JSONObject jsonObject) {
        return RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
    }

    protected RequestBody getRequestBody(String object) {
        return RequestBody.create(MediaType.parse("application/json"), object);
    }

    protected MultipartBody.Part getMultiPartBody(String keyName, File file) {
        if (file != null && file.exists()) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), file);
            return MultipartBody.Part.createFormData(keyName, file.getName(), requestFile);
        }

        return null;
    }

   /* protected void storeUserDetailsPref(PojoUserDetails pojoUserDetails, String uniqueId) {
        mPreferences.setString(prefUserEmail, getDecryptedString(pojoUserDetails.getEmail_address(), uniqueId));
        mPreferences.setString(prefUserMobile, getDecryptedString(pojoUserDetails.getMobile(), uniqueId));
        mPreferences.setString(prefUserName, getDecryptedString(pojoUserDetails.getName(), uniqueId));
        mPreferences.setString(prefUserType, pojoUserDetails.getUser_type());
        mPreferences.setString(prefUserImage, pojoUserDetails.getImage_url());
        mPreferences.setString(prefUserFirstName, getDecryptedString(pojoUserDetails.getFirst_name(), uniqueId));
        mPreferences.setString(prefUserMiddleName, getDecryptedString(pojoUserDetails.getMiddle_name(), uniqueId));
        mPreferences.setString(prefUserLastName, getDecryptedString(pojoUserDetails.getLast_name(), uniqueId));
        mPreferences.setBoolean(prefIsMLOldCertificateHolder, pojoUserDetails.is_ml_old_certificate_holder());
        if (pojoUserDetails.getUser_id() != 0) {
            mPreferences.setString(prefUserId, String.valueOf(pojoUserDetails.getUser_id()));
        }
    }*/

    protected void snackBarMandatoryFields() {
        observerSnackBarInt.set(R.string.error_mandatory_fields);
    }

    public String getToken(String uniqueId) {
        return getEncryptedString(TOKEN, uniqueId);
    }
}
