package com.org.prayas.business_logic.pojo;


import static com.org.prayas.utils.Utils.getDecryptedString;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.org.prayas.business_logic.pojo.base.PojoResponseBase;

import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoMonthGraphData extends PojoResponseBase {

    @SerializedName("scheme_Id")
    private String schemeId;

    @SerializedName("scheme_logo_name")
    private String titleLogoUrl;

    @SerializedName("scheme_Name")
    private String title;


    @SerializedName("kpI_Value_1_Level")
    private String kpIValue1Level;
    @SerializedName("kpI_Name_1_Level")
    private String kpIName1Level;
    @SerializedName("kpI_Value_2_Level")
    private String kpIValue2Level;
    @SerializedName("kpI_Name_2_Level")
    private String kpIName2Level;


    @SerializedName("kpI_Value_Unit1")
    private String kpIValueUnit1;

    @SerializedName("kpI_Value_Unit2")
    private String kpIValueUnit2;

    @SerializedName("kpI_Value_1")
    private List<PojoGraph> kpIValue1 = null;

    @SerializedName("kpI_Value_2")
    private List<PojoGraph> kpIValue2 = null;

    public void decryptData(String uniqueId) {
        schemeId = getDecryptedString(schemeId, uniqueId);
        titleLogoUrl = getDecryptedString(titleLogoUrl, uniqueId);
        title = getDecryptedString(title, uniqueId);
        kpIValue1Level = getDecryptedString(kpIValue1Level, uniqueId);
        kpIName1Level = getDecryptedString(kpIName1Level, uniqueId);
        kpIValue2Level = getDecryptedString(kpIValue2Level, uniqueId);
        kpIName2Level = getDecryptedString(kpIName2Level, uniqueId);
        kpIValueUnit1 = getDecryptedString(kpIValueUnit1, uniqueId);
        kpIValueUnit2 = getDecryptedString(kpIValueUnit2, uniqueId);
    }
}
