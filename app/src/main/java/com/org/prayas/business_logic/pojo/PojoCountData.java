package com.org.prayas.business_logic.pojo;

import com.google.gson.annotations.SerializedName;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoCountData {
    @SerializedName("count")
    private PojoHomeInfo homeInfo;
}
