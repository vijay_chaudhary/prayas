package com.org.prayas.business_logic.di;

import com.org.prayas.BuildConfig;
import com.org.prayas.business_logic.network.ApiCallFactory;
import com.org.prayas.business_logic.network.ApiHelper;
import com.org.prayas.business_logic.preferences.UtilsSharedPreferences;
import com.org.prayas.business_logic.rx.AppSchedulerProvider;
import com.org.prayas.business_logic.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import io.reactivex.disposables.CompositeDisposable;

@Module
@InstallIn(SingletonComponent.class)
public class NetworkModule {

    @Provides
    @Singleton
    @AppQualifier.BaseURL
    String provideBaseUrl() {
        return BuildConfig.BASE_URL;
    }

    @Provides
    @Singleton
    ApiHelper provideApiService(@AppQualifier.BaseURL String url,
                                UtilsSharedPreferences sharedPreferences) {
        return ApiCallFactory.create(url, sharedPreferences);
    }

    @Provides
    @Singleton
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

}
