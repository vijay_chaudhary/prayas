package com.org.prayas.business_logic.pojo;


import static com.org.prayas.utils.ConstantCodes.myColorMap;
import static com.org.prayas.utils.Utils.getColor;
import static com.org.prayas.utils.Utils.getDecryptedString;

import com.google.gson.annotations.SerializedName;
import com.org.prayas.R;

import java.util.ArrayList;
import java.util.List;

public class PojoSchemes {
    @SerializedName("scheme_Name")
    private String title;

    @SerializedName("scheme_logo_name")
    private String titleLogoUrl;

    //private int titleLogo = R.drawable.bg_pm_sym_logo_;
    private int itemColor = 0;

    @SerializedName("kpI_NAME_1")
    private String textTitle;

    @SerializedName("kpI_VALUE_1")
    private String titleValue = "";
    private String titleRupee = "";

    @SerializedName("kpI_NAME_2")
    private String textDesc;

    @SerializedName("kpI_VALUE_2")
    private String descValue = "";
    private String descRupee = "";

    @SerializedName("data_Freq")
    private String dataFreq;

    @SerializedName("cumulative_Flag")
    private String cumulativeFlag;

    @SerializedName("scheme_DGQI")
    private String mcValue;



    private String upDownValue;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /*public int getTitleLogo() {
        return titleLogo;
    }

    public void setTitleLogo(int titleLogo) {
        this.titleLogo = titleLogo;
    }*/

    public int getItemColor() {
        return itemColor;
    }

    public void setItemColor(int itemColor) {
        this.itemColor = itemColor;
    }

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle;
    }

    public String getTitleValue() {
        return titleValue;
    }

    public void setTitleValue(String titleValue) {
        this.titleValue = titleValue;
    }

    public String getTitleRupee() {
        return titleRupee;
    }

    public void setTitleRupee(String titleRupee) {
        this.titleRupee = titleRupee;
    }

    public String getTextDesc() {
        return textDesc;
    }

    public void setTextDesc(String textDesc) {
        this.textDesc = textDesc;
    }

    public String getDescValue() {
        return descValue;
    }

    public void setDescValue(String descValue) {
        this.descValue = descValue;
    }

    public String getDescRupee() {
        return descRupee;
    }

    public void setDescRupee(String descRupee) {
        this.descRupee = descRupee;
    }

    public String getMcValue() {
        return mcValue;
    }

    public void setMcValue(String mcValue) {
        this.mcValue = mcValue;
    }

    public String getUpDownValue() {
        return upDownValue;
    }

    public void setUpDownValue(String upDownValue) {
        this.upDownValue = upDownValue;
    }

    public String getDataFreq() {
        return dataFreq;
    }

    public void setDataFreq(String dataFreq) {

        this.dataFreq = dataFreq;
    }

    public String getCumulativeFlag() {
        return cumulativeFlag;
    }

    public void setCumulativeFlag(String cumulativeFlag) {
        this.cumulativeFlag = cumulativeFlag;
    }

    public String getTitleLogoUrl() {
        return titleLogoUrl;
    }

    public void setTitleLogoUrl(String titleLogoUrl) {
        this.titleLogoUrl = titleLogoUrl;
    }

    @SerializedName("scheme_Id")
    private String schemeId;

    @SerializedName("ministryID")
    private String ministryId;

    @SerializedName("sec_Code")
    private String sectorId;
    private String state;
    private String district;

    /*private boolean isShowC;
    private boolean isShowM;
    private String totalSubscribers;
    private String amountDisbursed;
    private final List<PojoGraph> subscribeData = new ArrayList<>();
    private final List<PojoPlace> subscribeTopState = new ArrayList<>();
    private final List<PojoGraph> disbursedData = new ArrayList<>();
    private final List<PojoPlace> disbursedTopState = new ArrayList<>();*/

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getMinistryId() {
        return ministryId;
    }

    public void setMinistryId(String ministryId) {
        this.ministryId = ministryId;
    }

    public String getSectorId() {
        return sectorId;
    }

    public void setSectorId(String sectorId) {
        this.sectorId = sectorId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    /*public boolean isShowC() {
        return isShowC;
    }

    public void setShowC(boolean showC) {
        isShowC = showC;
    }

    public boolean isShowM() {
        return isShowM;
    }

    public void setShowM(boolean showM) {
        isShowM = showM;
    }

    public String getTotalSubscribers() {
        return totalSubscribers;
    }

    public void setTotalSubscribers(String totalSubscribers) {
        this.totalSubscribers = totalSubscribers;
    }

    public String getAmountDisbursed() {
        return amountDisbursed;
    }

    public void setAmountDisbursed(String amountDisbursed) {
        this.amountDisbursed = amountDisbursed;
    }*/

   /* public List<PojoGraph> getSubscribeData() {
        return subscribeData;
    }

    public List<PojoPlace> getSubscribeTopState() {
        return subscribeTopState;
    }

    public List<PojoGraph> getDisbursedData() {
        return disbursedData;
    }

    public List<PojoPlace> getDisbursedTopState() {
        return disbursedTopState;
    }*/

    /*@SerializedName("scheme_logo")
    private int schemeLogo;
    @SerializedName("state_code")
    private String stateCode;
    @SerializedName("dist_code")
    private String distCode;
    @SerializedName("scheme_code")
    private String schemeCode;
    @SerializedName("scheme_name")
    private String schemeName;
    @SerializedName("Sec_code")
    private String secCode;
    @SerializedName("MinistryID")
    private String ministryID;
    @SerializedName("KPI_NAME_1")
    private String kpiName1;
    @SerializedName("KPI_NAME_2")
    private String kpiName2;
    @SerializedName("KPI_VALUE_1")
    private String kpiValue1;
    @SerializedName("KPI_VALUE_2")
    private String kpiValue2;
    @SerializedName("Scheme_DGQI")
    private String schemeDGQI;
    @SerializedName("data_Freq")
    private String dataFreq;
    @SerializedName("State_Name")
    private String stateName;
    @SerializedName("district_name")
    private String districtName;
    @SerializedName("Yoy_value")
    private String yoyValue;
    @SerializedName("cumulative_flag")
    private boolean cumulativeFlag;
    @SerializedName("total_subscribes")
    private String totalSubscribes;
    @SerializedName("amount_Disbursed")
    private String amountDisbursed;

    public PojoSchemes() {
    }

    public PojoSchemes(int schemeLogo, String stateCode, String distCode, String schemeCode, String schemeName, String secCode, String ministryID, String kpiName1, String kpiName2, String kpiValue1, String kpiValue2, String schemeDGQI, String dataFreq, String stateName, String districtName, String yoyValue, boolean cumulativeFlag, String totalSubscribes, String amountDisbursed) {
        this.schemeLogo = schemeLogo;
        this.stateCode = stateCode;
        this.distCode = distCode;
        this.schemeCode = schemeCode;
        this.schemeName = schemeName;
        this.secCode = secCode;
        this.ministryID = ministryID;
        this.kpiName1 = kpiName1;
        this.kpiName2 = kpiName2;
        this.kpiValue1 = kpiValue1;
        this.kpiValue2 = kpiValue2;
        this.schemeDGQI = schemeDGQI;
        this.dataFreq = dataFreq;
        this.stateName = stateName;
        this.districtName = districtName;
        this.yoyValue = yoyValue;
        this.cumulativeFlag = cumulativeFlag;
        this.totalSubscribes = totalSubscribes;
        this.amountDisbursed = amountDisbursed;
    }

    public int getSchemeLogo() {
        return schemeLogo;
    }

    public void setSchemeLogo(int schemeLogo) {
        this.schemeLogo = schemeLogo;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getDistCode() {
        return distCode;
    }

    public void setDistCode(String distCode) {
        this.distCode = distCode;
    }

    public String getSchemeCode() {
        return schemeCode;
    }

    public void setSchemeCode(String schemeCode) {
        this.schemeCode = schemeCode;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getSecCode() {
        return secCode;
    }

    public void setSecCode(String secCode) {
        this.secCode = secCode;
    }

    public String getMinistryID() {
        return ministryID;
    }

    public void setMinistryID(String ministryID) {
        this.ministryID = ministryID;
    }

    public String getKpiName1() {
        return kpiName1;
    }

    public void setKpiName1(String kpiName1) {
        this.kpiName1 = kpiName1;
    }

    public String getKpiName2() {
        return kpiName2;
    }

    public void setKpiName2(String kpiName2) {
        this.kpiName2 = kpiName2;
    }

    public String getKpiValue1() {
        return kpiValue1;
    }

    public void setKpiValue1(String kpiValue1) {
        this.kpiValue1 = kpiValue1;
    }

    public String getKpiValue2() {
        return kpiValue2;
    }

    public void setKpiValue2(String kpiValue2) {
        this.kpiValue2 = kpiValue2;
    }

    public String getSchemeDGQI() {
        return schemeDGQI;
    }

    public void setSchemeDGQI(String schemeDGQI) {
        this.schemeDGQI = schemeDGQI;
    }

    public String getDataFreq() {
        return dataFreq;
    }

    public void setDataFreq(String dataFreq) {
        this.dataFreq = dataFreq;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getYoyValue() {
        return yoyValue;
    }

    public void setYoyValue(String yoyValue) {
        this.yoyValue = yoyValue;
    }

    public boolean getCumulativeFlag() {
        return cumulativeFlag;
    }

    public void setCumulativeFlag(boolean cumulativeFlag) {
        this.cumulativeFlag = cumulativeFlag;
    }

    public String getTotalSubscribes() {
        return totalSubscribes;
    }

    public void setTotalSubscribes(String totalSubscribes) {
        this.totalSubscribes = totalSubscribes;
    }

    public String getAmountDisbursed() {
        return amountDisbursed;
    }

    public void setAmountDisbursed(String amountDisbursed) {
        this.amountDisbursed = amountDisbursed;
    }*/

    public void setUpColorData() {
        itemColor = getColor(myColorMap.get(getSectorId()));
    }

    public void decryptData(String uniqueId) {
        title = getDecryptedString(title, uniqueId);
        titleLogoUrl = getDecryptedString(titleLogoUrl, uniqueId);
        textTitle = getDecryptedString(textTitle, uniqueId);
        titleValue = getDecryptedString(titleValue, uniqueId);
        textDesc = getDecryptedString(textDesc, uniqueId);
        descValue = getDecryptedString(descValue, uniqueId);
        dataFreq = getDecryptedString(dataFreq, uniqueId);
        cumulativeFlag = getDecryptedString(cumulativeFlag, uniqueId);
        mcValue = getDecryptedString(mcValue, uniqueId);
        schemeId = getDecryptedString(schemeId, uniqueId);
        ministryId = getDecryptedString(ministryId, uniqueId);
        sectorId = getDecryptedString(sectorId, uniqueId);
    }
}
