package com.org.prayas.business_logic.pojo.base;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vijay Aug, 05 2021.
 */

public class PojoSpinnerCommon {
    @SerializedName("id")
    private String id;
    @SerializedName("value")
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @NonNull
    @Override
    public String toString() {
        return (!TextUtils.isEmpty(value) ? value : "");
    }
}
