package com.org.prayas.business_logic.rx;

import io.reactivex.Scheduler;

/**
 * Created by Vijay on Aug, 05 2021.
 */

public interface SchedulerProvider {
    Scheduler ui();

    Scheduler computation();

    Scheduler io();
}
