package com.org.prayas.business_logic.pojo;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Naria Sachin on Mar, 02 2021 11:05.
 */

@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoDashboardServiceData {
    private int title;
    private String count;
    private int countColor;
    private int icon;
    private int backBG;
    private int bottomBG;
}
