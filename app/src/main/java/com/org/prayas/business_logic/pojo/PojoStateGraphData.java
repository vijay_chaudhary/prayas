package com.org.prayas.business_logic.pojo;

import com.google.gson.annotations.SerializedName;
import com.org.prayas.business_logic.pojo.base.PojoResponseBase;

import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(AccessLevel.PUBLIC)
public class PojoStateGraphData extends PojoResponseBase {
    @SerializedName("kpI_Value_1")
    private List<PojoKpIValue> kpIValue1 = null;

    @SerializedName("kpI_Value_2")
    private List<PojoKpIValue> kpIValue2 = null;
}
