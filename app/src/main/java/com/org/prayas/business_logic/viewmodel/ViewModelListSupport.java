package com.org.prayas.business_logic.viewmodel;

import static com.org.prayas.utils.ConstantCodes.RESPONSE_DATA_COUNT_PP;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import com.org.prayas.R;
import com.org.prayas.business_logic.interactors.ObservableString;
import com.org.prayas.business_logic.interfaces.ListenerListSupport;
import com.org.prayas.business_logic.interfaces.OnScrollListener;
import com.org.prayas.business_logic.pojo.base.PojoResponseListBase;
import com.org.prayas.utils.ResourceProvider;

import io.reactivex.functions.Consumer;

/**
 * Created by Naria Sachin on Mar, 24 2021 18:43.
 */

public abstract class ViewModelListSupport<D extends PojoResponseListBase, L> extends com.org.prayas.business_logic.viewmodel.ViewModelBase implements ListenerListSupport<D> {

    public final ObservableBoolean isShowEmptySearchView = new ObservableBoolean(false);
    public final ObservableBoolean observableSwipeRefreshLayout = new ObservableBoolean(false);
    public final ObservableBoolean observableIsSwipeEnable = new ObservableBoolean(true);
    public final ObservableArrayList<L> observerContent = new ObservableArrayList<>();
    public ObservableString observableSearchText = new ObservableString("");


    private final MutableLiveData<Integer> mLiveDataNotifyItemInserted = new MutableLiveData<>();

    private boolean mIsLoading = false, mIsClearData = false, mIsFromLastItem = false,
            mShowCentreProgress = true, mIsSearch = false;
    public int mPageIndex = 1, mTotalCount = 0;
    private String searchText = "";

    public ViewModelListSupport() {
    }

    public ViewModelListSupport(ResourceProvider mResourceProvider) {
        this.mResourceProvider = mResourceProvider;
    }

    public MutableLiveData<Integer> getLiveDataNotifyItemInserted() {
        return mLiveDataNotifyItemInserted;
    }

    public boolean isProcessing() {
        return observerProgressBar.get() || observableSwipeRefreshLayout.get();
    }

    public void onMenuItemActionCollapse() {
        if (mIsSearch) {
            onRefreshList(true);
        }
    }

    public void onQueryTextSubmit(String newText) {
        if (isInternetConnected(R.string.message_noconnection)) {
            if (!mIsLoading) {
                searchText = newText;
                mShowCentreProgress = true;
                mIsFromLastItem = false;
                mIsClearData = true;
                mIsSearch = true;

                apiCallList();
            }
        }
    }

    public void onRefreshList(boolean isFromSearch) {
        if (!isFromSearch) {
            observableSwipeRefreshLayout.set(true);
        }
        if (isInternetConnected(R.string.message_noconnection)) {
            if (!mIsLoading) {
                mIsSearch = false;
                mIsClearData = true;
                mIsFromLastItem = false;
                mShowCentreProgress = isFromSearch;

                apiCallList();

            } else {
                observableSwipeRefreshLayout.set(false);
            }
        } else {
            observableSwipeRefreshLayout.set(false);
        }
    }

    public void apiCallList() {
        mIsLoading = true;

        if (mShowCentreProgress) {
            observerProgressBar.set(true);
        } else if (mIsFromLastItem) {
            observerContent.add(null);
            //mLiveDataNotifyItemInserted.setValue((observerContent.size() - 1));
        }

        if (mIsClearData) {
            mPageIndex = 1;
        }
        if (!mIsSearch) {
            observableSearchText.set("");
            searchText = "";
        }

        mCompositeDisposable.add(getApiRequest(mPageIndex)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(apiCallBack, throwable -> {
                    removeFooter();
                    hideProgress();
                    onApiFailure();
                }));
    }

    private void hideProgress() {
        mIsLoading = false;
        mIsClearData = false;
        mIsFromLastItem = false;
        mShowCentreProgress = false;
        observableSwipeRefreshLayout.set(false);
        observerProgressBar.set(false);
        setTextNoRecords();
    }

    private void removeFooter() {
        if (mIsFromLastItem && !observerContent.isEmpty()) {
            observerContent.remove(observerContent.size() - 1);
            mLiveDataNotifyItemInserted.setValue((observerContent.size()));
        }
    }

    public void setTextNoRecords() {
        if (!observerContent.isEmpty()) {
            isShowEmptySearchView.set(false);
        } else {
            isShowEmptySearchView.set(true);
        }
    }

    private final Consumer<D> apiCallBack = pojoResponse -> {
        removeFooter();
        if (isValidApiResponse(pojoResponse)) {
            if (mIsClearData) {
                mTotalCount = 0;
                observerContent.clear();
                mLiveDataNotifyItemInserted.setValue(-1);
            }
            mTotalCount = pojoResponse.getListCount();

            observerIsNoRecords.set(mTotalCount == 0);
            if (pojoResponse.isValidList()) {
                onApiResult(pojoResponse);
                mPageIndex++;
            }
        }

        hideProgress();
    };

    public OnScrollListener onScrollListener = new OnScrollListener() {
        @Override
        public void onScrollStateChanged(Boolean isEnabled) {
            observableIsSwipeEnable.set(isEnabled);
        }

        @Override
        public void onScrolled(int totalItemCount, int pastVisibleItems) {
            if (totalItemCount < mTotalCount && !mIsLoading && ((mPageIndex - 1) * RESPONSE_DATA_COUNT_PP <= (pastVisibleItems + 2))) {
                if (isInternetConnected(R.string.message_noconnection)) {
                    mIsClearData = false;
                    mIsFromLastItem = true;
                    mShowCentreProgress = false;

                    apiCallList();
                }
            }
        }
    };

    protected void setTotalCount(int totalCount) {
        this.mTotalCount = totalCount;
    }

    protected String getSearchText() {
        return searchText;
    }
}
