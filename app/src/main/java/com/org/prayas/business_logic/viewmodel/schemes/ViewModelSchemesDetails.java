package com.org.prayas.business_logic.viewmodel.schemes;

import static com.org.prayas.utils.Utils.getEncryptedString;

import android.text.TextUtils;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.databinding.ObservableList;
import androidx.lifecycle.MutableLiveData;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.org.prayas.R;
import com.org.prayas.business_logic.interactors.ObservableString;
import com.org.prayas.business_logic.pojo.PojoGraph;
import com.org.prayas.business_logic.pojo.PojoKpIValue;
import com.org.prayas.business_logic.pojo.PojoMonthGraphData;
import com.org.prayas.business_logic.pojo.PojoPlace;
import com.org.prayas.business_logic.pojo.PojoSchemes;
import com.org.prayas.business_logic.viewmodel.ViewModelBase;
import com.org.prayas.utils.Logger;
import com.org.prayas.utils.ResourceProvider;
import com.org.prayas.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class ViewModelSchemesDetails extends ViewModelBase {
    //public ObservableString textDepartment = new ObservableString("");

    //public ObservableInt schemeLogo = new ObservableInt(0);
    public ObservableInt itemColor = new ObservableInt(0);
    public ObservableString schemeLogoUrl = new ObservableString(); //
    public ObservableString schemeName = new ObservableString("");
    public ObservableString schemeDate = new ObservableString(""); //

    public ObservableString totalSubscribersValue = new ObservableString(""); //
    public ObservableString totalSubscribersText = new ObservableString("");
    public ObservableString totalDisbursedValue = new ObservableString("");
    public ObservableString totalDisbursedText = new ObservableString(""); //

    public ObservableString subscribersText = new ObservableString(""); //
    public ObservableString subscribersValue = new ObservableString("");
    public ObservableString subscribersRupee = new ObservableString("");

    public ObservableString disbursedText = new ObservableString("");
    public ObservableString disbursedValue = new ObservableString("");
    public ObservableString disbursedRupee = new ObservableString(""); //

    public ObservableList<PojoPlace> subscribersList = new ObservableArrayList<>();
    public ObservableString totalStateSubscribers = new ObservableString("");

    public ObservableList<PojoPlace> disbursedList = new ObservableArrayList<>();
    public ObservableString disbursedSubscribers = new ObservableString("");

    public ArrayList<BarEntry> subscribersChartValues = new ArrayList<>();
    public ArrayList<Entry> disbursedChartValues = new ArrayList<>();

    public ArrayList<PojoGraph> subscribersChartXValues = new ArrayList<>();
    public ArrayList<PojoGraph> disbursedChartXValues = new ArrayList<>();

    public ObservableString subscribersGraphDesc = new ObservableString(""); //
    public ObservableString disbursedGraphDesc = new ObservableString(""); //

    //public ObservableInt schemeLogoNext = new ObservableInt(0);
    //public ObservableInt schemeLogoPrevious = new ObservableInt(0);

    public ObservableString schemeLogoUrlNext = new ObservableString("");
    public ObservableString schemeLogoUrlPrevious = new ObservableString("");

    public ObservableBoolean isNext = new ObservableBoolean(true);
    public ObservableBoolean isPrevious = new ObservableBoolean(false);

    private final SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy", Locale.getDefault());
    public MutableLiveData<Void> dataLoaded = new MutableLiveData<>();

    public ObservableArrayList<PojoPlace> observableState = new ObservableArrayList<>();
    public ObservableArrayList<PojoPlace> observableDistrict = new ObservableArrayList<>();

    public ObservableString stateId = new ObservableString("");
    public ObservableString districtId = new ObservableString("");

    public ObservableInt statePosition = new ObservableInt(0);
    public ObservableInt districtPosition = new ObservableInt(0);

    public MutableLiveData<Void> setUpStateData = new MutableLiveData<>();
    public MutableLiveData<Void> setUpDistrictData = new MutableLiveData<>();

    public String initStateId;
    public String initDistrictId;

    @Inject
    public ViewModelSchemesDetails(ResourceProvider resourceProvider) {
        this.mResourceProvider = resourceProvider;
        //textDepartment.set("Ministry of Labour & Employment");

        observableState.add(new PojoPlace(mResourceProvider.getString(R.string.text_select_state)));
        observableDistrict.add(new PojoPlace(mResourceProvider.getString(R.string.text_select_district)));

        setUpTodayDate();
    }

    private void setUpTodayDate() {
        Calendar cal = Calendar.getInstance();
        schemeDate.set("As on : " + formatter.format(cal.getTime()));
    }

    /*public void setPlaceId(String stateId, String districtId) {
        initStateId = stateId;
        initDistrictId = districtId;
    }*/

    public void setUpPojoGraphData(PojoSchemes pojoSchemes) {
        schemeLogoUrl.set(pojoSchemes.getTitleLogoUrl());
        schemeName.set(pojoSchemes.getTitle());
        itemColor.set(pojoSchemes.getItemColor());

        //totalSubscribersValue.set(pojoSchemes.getTotalSubscribers());
        //totalDisbursedValue.set(pojoSchemes.getAmountDisbursed());

        /*subscribersText.set(pojoSchemes.getTextTitle());
        subscribersValue.set(pojoSchemes.getTitleValue());
        subscribersRupee.set(pojoSchemes.getTitleRupee());

        disbursedText.set(pojoSchemes.getTextDesc());
        disbursedValue.set(pojoSchemes.getDescValue());
        disbursedRupee.set(pojoSchemes.getDescRupee());*/

        /*subscribersList.clear();
        subscribersList.addAll(pojoSchemes.getSubscribeTopState());
        totalStateSubscribers.set(String.valueOf(subscribersList.size()));*/

        /*disbursedList.clear();
        disbursedList.addAll(pojoSchemes.getDisbursedTopState());
        disbursedSubscribers.set(String.valueOf(disbursedList.size()));*/

        clearData(true);
        callStateGraphAPI(pojoSchemes);
        callMonthGraphAPI(pojoSchemes);
    }

    public void setUpPojoGraphData(PojoMonthGraphData upPojoGraphData) {
        totalSubscribersText.set(upPojoGraphData.getKpIName1Level());
        totalSubscribersValue.set(upPojoGraphData.getKpIValue1Level());

        totalDisbursedText.set(upPojoGraphData.getKpIName2Level());
        totalDisbursedValue.set(upPojoGraphData.getKpIValue2Level());

        subscribersGraphDesc.set("Beneficiary in " + upPojoGraphData.getKpIValueUnit1());
        disbursedGraphDesc.set("Beneficiary in " + upPojoGraphData.getKpIValueUnit2());

        subscribersText.set(upPojoGraphData.getKpIName1Level());
        subscribersValue.set(upPojoGraphData.getKpIValue1Level());
        subscribersRupee.set("");

        disbursedText.set(upPojoGraphData.getKpIName2Level());
        disbursedValue.set(upPojoGraphData.getKpIValue2Level());
        disbursedRupee.set("");
    }

    public void clearData(boolean isAll) {
        totalSubscribersText.set("");
        totalSubscribersValue.set("");

        totalDisbursedText.set("");
        totalDisbursedValue.set("");

        subscribersGraphDesc.set("");
        disbursedGraphDesc.set("");

        subscribersText.set("");
        subscribersValue.set("");
        subscribersRupee.set("");

        disbursedText.set("");
        disbursedValue.set("");
        disbursedRupee.set("");

        subscribersChartXValues.clear();
        disbursedChartXValues.clear();

        if (isAll) {
            subscribersList.clear();
            disbursedList.clear();

            totalStateSubscribers.set("");
            disbursedSubscribers.set("");
        }
    }

    public void setUpGraphData(PojoSchemes pojoSchemes) {
        /*subscribersChartXValues.clear();
        subscribersChartXValues.addAll(pojoSchemes.getSubscribeData());

        disbursedChartXValues.clear();
        disbursedChartXValues.addAll(pojoSchemes.getDisbursedData());*/
    }


    public void callDetailsAPIs(PojoSchemes pojoSchemes) {
        if (pojoSchemes != null) {
            //clearData(false);
            callMonthGraphAPI(pojoSchemes);
        }
    }

    public void callStateGraphAPI(PojoSchemes pojoSchemes) {
        if (isInternetConnected()) {
            String uniqueId = Utils.getUniqueIDWhitRandomString();

            compositeDisposable.add(mApiHelper.getStateGraph("", uniqueId,
                    getEncryptedString(pojoSchemes.getMinistryId(), uniqueId),
                   //getEncryptedString(SCHEME_ID, uniqueId))
                    getEncryptedString(pojoSchemes.getSchemeId(), uniqueId))
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(pojo -> {
                        if (isValidApiResponse(pojo)) {

                            if (pojo.getKpIValue1() == null && pojo.getKpIValue2() == null) {
                                handleApiErrorMessage(pojo);
                            }

                            subscribersList.clear();
                            if (pojo.getKpIValue1() != null && !pojo.getKpIValue1().isEmpty()) {
                                for (PojoKpIValue data : pojo.getKpIValue1()) {
                                    if (data != null) {
                                        data.decryptData(uniqueId);
                                    }
                                }

                                PojoKpIValue pojoKpIValue = pojo.getKpIValue1().get(0);

                                addStateData(pojoKpIValue, subscribersList);
                                totalStateSubscribers.set(String.valueOf(subscribersList.size()));
                            }

                            disbursedList.clear();
                            if (pojo.getKpIValue2() != null && !pojo.getKpIValue2().isEmpty()) {
                                for (PojoKpIValue data : pojo.getKpIValue2()) {
                                    if (data != null) {
                                        data.decryptData(uniqueId);
                                    }
                                }
                                PojoKpIValue pojoKpIValue = pojo.getKpIValue2().get(0);

                                addStateData(pojoKpIValue, disbursedList);
                                disbursedSubscribers.set(String.valueOf(disbursedList.size()));
                            }
                        }
                        //observerProgressBar.set(false);
                    }, throwable -> {
                        //observerProgressBar.set(false);
                        handleApiErrorMessage(throwable.getMessage());
                        Logger.e("throwable", throwable.getMessage());
                    }));
        } /*else {
            observerProgressBar.set(false);
        }*/
    }

    public void callMonthGraphAPI(PojoSchemes pojoSchemes) {
        if (isInternetConnected()) {
            String uniqueId = Utils.getUniqueIDWhitRandomString();
            String state_Id = (!TextUtils.isEmpty(initStateId) ? initStateId : null);
            String district_Id = (!TextUtils.isEmpty(initDistrictId) ? initDistrictId : null);

            if (TextUtils.isEmpty(state_Id))
                state_Id = (!TextUtils.isEmpty(stateId.getTrimmed()) ? stateId.getTrimmed() : null);
            if (TextUtils.isEmpty(district_Id))
                district_Id = (!TextUtils.isEmpty(districtId.getTrimmed()) ? districtId.getTrimmed() : null);

            observerProgressBar.set(true);
            compositeDisposable.add(mApiHelper.getMonthGraph("", uniqueId,
                    getEncryptedString(pojoSchemes.getMinistryId(), uniqueId),
                    //getEncryptedString(SCHEME_ID, uniqueId),
                    getEncryptedString(pojoSchemes.getSchemeId(), uniqueId),
                    getEncryptedString(state_Id, uniqueId),
                    getEncryptedString(district_Id, uniqueId))
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(pojo -> {
                        clearData(false);
                        if (isValidApiResponse(pojo)) {

                            if (pojo.getKpIValue1() == null && pojo.getKpIValue2() == null) {
                                handleApiErrorMessage(pojo);
                            }

                            subscribersChartXValues.clear();
                            pojo.decryptData(uniqueId);
                            if (pojo.getKpIValue1() != null && !pojo.getKpIValue1().isEmpty()) {
                                Collections.reverse(pojo.getKpIValue1());
                                for (PojoGraph pojoGraph : pojo.getKpIValue1()) {
                                    if (pojoGraph != null) {
                                        pojoGraph.decryptData(uniqueId);
                                        pojoGraph.setUpXValue();
                                    }
                                }
                                subscribersChartXValues.addAll(pojo.getKpIValue1());
                            }

                            disbursedChartXValues.clear();
                            if (pojo.getKpIValue2() != null && !pojo.getKpIValue2().isEmpty()) {
                                Collections.reverse(pojo.getKpIValue2());
                                for (PojoGraph pojoGraph : pojo.getKpIValue2()) {
                                    if (pojoGraph != null) {
                                        pojoGraph.decryptData(uniqueId);
                                        pojoGraph.setUpXValue();
                                    }
                                }
                                disbursedChartXValues.addAll(pojo.getKpIValue2());
                            }
                            setUpPojoGraphData(pojo);
                        }
                        dataLoaded.setValue(null);
                        observerProgressBar.set(false);
                    }, throwable -> {
                        clearData(false);
                        observerProgressBar.set(false);
                        handleApiErrorMessage(throwable.getMessage());
                        Logger.e("throwable", throwable.getMessage());
                    }));
        } /*else {
            observerProgressBar.set(false);
        }*/
    }

    public void callStateAPI() {
        if (isInternetConnected()) {
            String uniqueId = Utils.getUniqueIDWhitRandomString();
            //observerProgressBar.set(true);
            compositeDisposable.add(mApiHelper.getState("", uniqueId,
                    getToken(uniqueId))
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(pojo -> {
                        if (isValidApiResponse(pojo)) {
                            if (pojo.getPojoPlaces() != null) {
                                observableState.clear();
                                observableState.add(new PojoPlace(mResourceProvider.getString(R.string.text_select_state)));
                                if (pojo.getPojoPlaces().size() > 0) {
                                    for (PojoPlace data : pojo.getPojoPlaces()) {
                                        if (data != null) {
                                            data.decryptData(uniqueId);
                                        }
                                    }
                                    observableState.addAll(pojo.getPojoPlaces());

                                    if (!TextUtils.isEmpty(initStateId)) {
                                        setUpStateData.setValue(null);
                                        //initStateId = null;
                                    }
                                } /*else  {
                                    handleApiErrorMessage(pojo);
                                }*/
                            } else {
                                handleApiErrorMessage(pojo);
                            }
                        }
                        //observerProgressBar.set(false);
                    }, throwable -> {
                        //observerProgressBar.set(false);
                        handleApiErrorMessage(throwable.getMessage());
                        Logger.e("throwable", throwable.getMessage());
                    }));
        }
    }

    public void callDistrictAPI() {
        if (isInternetConnected(R.string.message_noconnection)) {
            String uniqueId = Utils.getUniqueIDWhitRandomString();
            observerProgressBar.set(true);
            compositeDisposable.add(mApiHelper.getDistrict("", uniqueId,
                    getToken(uniqueId),
                    getEncryptedString(stateId.getTrimmed(), uniqueId))
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(pojo -> {
                        if (isValidApiResponse(pojo)) {
                            if (pojo.getPojoPlaces() != null) {
                                observableDistrict.clear();
                                observableDistrict.add(new PojoPlace(mResourceProvider.getString(R.string.text_select_district)));
                                if (pojo.getPojoPlaces().size() > 0) {
                                    for (PojoPlace data : pojo.getPojoPlaces()) {
                                        if (data != null) {
                                            data.decryptData(uniqueId);
                                        }
                                    }
                                    observableDistrict.addAll(pojo.getPojoPlaces());

                                    if (!TextUtils.isEmpty(initDistrictId)) {
                                        setUpDistrictData.setValue(null);
                                        //initDistrictId = null;
                                    }
                                } /*else  {
                                    handleApiErrorMessage(pojo);
                                }*/
                            } else {
                                handleApiErrorMessage(pojo);
                            }
                        }
                        observerProgressBar.set(false);
                    }, throwable -> {
                        observerProgressBar.set(false);
                        handleApiErrorMessage(throwable.getMessage());
                        Logger.e("throwable", throwable.getMessage());
                    }));
        }
    }


    private void addStateData(PojoKpIValue pojoKpIValue, ObservableList<PojoPlace> subscribersList) {
        subscribersList.add(new PojoPlace("1. " + pojoKpIValue.getTop1State()));
        subscribersList.add(new PojoPlace("2. " + pojoKpIValue.getTop2State()));
        subscribersList.add(new PojoPlace("3. " + pojoKpIValue.getTop3State()));
        subscribersList.add(new PojoPlace("4. " + pojoKpIValue.getTop4State()));
        subscribersList.add(new PojoPlace("5. " + pojoKpIValue.getTop5State()));
    }

    public void onClickDistrictSpinner() {
        if (TextUtils.isEmpty(stateId.get())) {
            observerSnackBarInt.set(R.string.text_select_state_first);
        }
    }
}
