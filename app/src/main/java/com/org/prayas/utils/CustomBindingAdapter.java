package com.org.prayas.utils;

import static com.org.prayas.utils.Utils.convertDpToPixel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableInt;
import androidx.databinding.ObservableList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.org.prayas.MyApplication;
import com.org.prayas.R;
import com.org.prayas.business_logic.interactors.ObservableString;
import com.org.prayas.business_logic.interfaces.GeneralItemListener;
import com.org.prayas.business_logic.interfaces.OnScrollListener;
import com.org.prayas.business_logic.interfaces.ViewPageChangeListener;
import com.org.prayas.business_logic.pojo.PojoBannerData;
import com.org.prayas.business_logic.pojo.PojoDashboardServiceData;
import com.org.prayas.business_logic.pojo.PojoMinDeptData;
import com.org.prayas.business_logic.pojo.PojoPlace;
import com.org.prayas.business_logic.pojo.PojoSchemes;
import com.org.prayas.business_logic.pojo.PojoSchemesFilterData;
import com.org.prayas.business_logic.pojo.PojoSectorsData;
import com.org.prayas.view.adapter.AdapterDashboardService;
import com.org.prayas.view.adapter.AdapterGridState;
import com.org.prayas.view.adapter.AdapterMinDept;
import com.org.prayas.view.adapter.AdapterPagerHomeBanner;
import com.org.prayas.view.adapter.AdapterSchemes;
import com.org.prayas.view.adapter.AdapterSchemesFilter;
import com.org.prayas.view.adapter.AdapterSectors;

import java.io.File;
import java.util.List;


/**
 * Created by Naria Sachin on Jan, 15 2020 12:50.
 */

public class CustomBindingAdapter {

    /**
     * Shows snack bar
     *
     * @param viewLayout         Layout relative to which snack bar is to be shown
     * @param snackMessageInt    Message resource to be shown in snack bar
     * @param snackMessageString Message to be shown in snack bar
     */
    @BindingAdapter(value = {"showSnackBarInt", "showSnackBarString"}, requireAll = false)
    public static void showSnackBar(View viewLayout, ObservableInt snackMessageInt,
                                    ObservableString snackMessageString) {
        String message = "";
        if (snackMessageString != null && !TextUtils.isEmpty(snackMessageString.getTrimmed())) {
            message = snackMessageString.getTrimmed();
            snackMessageString.set("");
        } else if (snackMessageInt != null && snackMessageInt.get() != 0) {
            message = viewLayout.getResources().getString(snackMessageInt.get());
            snackMessageInt.set(0);
        }

        if (!TextUtils.isEmpty(message)) {
            Utils.showSnackBar(viewLayout, message);
        }
    }

    @BindingAdapter({"refreshListener"})
    public static void refreshListener(SwipeRefreshLayout swipeRefresh,
                                       SwipeRefreshLayout.OnRefreshListener refreshListener) {
        if (swipeRefresh != null) {
            swipeRefresh.setOnRefreshListener(refreshListener);
        }
    }

    @BindingAdapter({"setEnabledSwipeRefresh"})
    public static void setEnabledSwipeRefresh(SwipeRefreshLayout swipeRefresh, boolean isEnabled) {
        if (swipeRefresh != null) {
            swipeRefresh.setEnabled(isEnabled);
        }
    }

    @BindingAdapter({"setSwipeRefreshing"})
    public static void setSwipeRefreshing(SwipeRefreshLayout swipeRefresh, boolean toRefresh) {
        if (swipeRefresh != null) {
            swipeRefresh.setRefreshing(toRefresh);
        }
    }

    /**
     * Sets enable flag for textinput layout
     *
     * @param textInputLayout Textinputlayout reference
     * @param isEnabled       Whether view is enabled or not
     */
    @BindingAdapter({"setEnabledTIL"})
    public static void setTextInputEnabled(TextInputLayout textInputLayout, boolean isEnabled) {
        textInputLayout.setEnabled(isEnabled);
    }

    /**
     * Sets error for textinput layout
     *
     * @param textInputLayout Textinputlayout reference
     * @param errorMessage    Error message to be shown
     */
    @BindingAdapter({"setError"})
    public static void setError(TextInputLayout textInputLayout, int errorMessage) {
        if (errorMessage != 0) {
            textInputLayout
                    .setError(textInputLayout.getContext().getResources().getString(errorMessage));
        }
    }

    @BindingAdapter({"navigationViewItemClick"})
    public static void loadNavigationHeader(NavigationView navigationView,
                                            NavigationView.OnNavigationItemSelectedListener itemSelectedListener) {
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(itemSelectedListener);
        }
    }

    @BindingAdapter("addHtmlReqTextHint")
    public static void addHtmlReqTextHint(TextInputLayout textView, String text) {
        text += " " + textView.getResources().getString(R.string.text_doc_title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setHint(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
        } else {
            textView.setHint(Html.fromHtml(text));
        }
    }


    @BindingAdapter("addHtmlReqText")
    public static void addHtmlReqText(TextView textView, String text) {
        text += " " + textView.getResources().getString(R.string.text_doc_title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
        } else {
            textView.setText(Html.fromHtml(text));
        }
    }

    @BindingAdapter("addHtmlReqTextCheckBox")
    public static void addHtmlReqTextCheckBox(CheckBox textView, String text) {
        text += " " + textView.getResources().getString(R.string.text_doc_title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
        } else {
            textView.setText(Html.fromHtml(text));
        }
    }

    @BindingAdapter({"bindInputLayout", "bindHelperText"})
    public static void bindHelperText(TextInputEditText textView,
                                      TextInputLayout textInputLayout,
                                      String helperText) {
        textView.setOnFocusChangeListener((view, focused) -> {
            textInputLayout.setHelperTextEnabled(focused);
            textInputLayout.setHelperText(focused ? helperText : "");
        });
    }

    @BindingAdapter("onSpinnerItemSelectListener")
    public static void bindOnItemSelectListener(final Spinner view,
                                                AdapterView.OnItemSelectedListener spinnerSelectedListener) {
        view.setOnItemSelectedListener(spinnerSelectedListener);
    }

    @BindingAdapter("setSelectedPosition")
    public static void bindSetSelectedPosition(final Spinner view, int position) {
        Log.e("setSelectedPosition", "pos=" + position + " view=" + view.getSelectedItemPosition());
        if (view.getSelectedItemPosition() != position) {
            view.setSelection(position);
        }
    }

    /**
     * Validate mobile number with valid code
     */
    @BindingAdapter({"bindTextChangeMobileListener"})
    public static void bindTextChangeMobileListener(TextInputEditText input, ObservableString message) {
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0) {
                    if (charSequence.toString().trim().startsWith("0") ||
                            charSequence.toString().trim().startsWith("1") ||
                            charSequence.toString().trim().startsWith("2") ||
                            charSequence.toString().trim().startsWith("3") ||
                            charSequence.toString().trim().startsWith("4") ||
                            charSequence.toString().trim().startsWith("5")) {
                        input.setText("");
                    } else {
                        input.setError(null);
//                        message.set("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public static void setLayoutManagerAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter, int orientation) {
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(recyclerView.getContext(), orientation, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private static void setGridAdapterSimple(RecyclerView recyclerView, RecyclerView.Adapter adapter, int itemCount) {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(recyclerView.getContext(), itemCount);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Scroll listener
     */
    @BindingAdapter("listenerScroll")
    public static void bindLayoutManager(@NonNull RecyclerView recyclerView, final OnScrollListener
            onScrollListener) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (onScrollListener != null) {
                    LinearLayoutManager linearLayoutManager = getLinearLayoutManager(recyclerView);
                    if (linearLayoutManager != null) {
                        if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() <= 0) {
                            onScrollListener.onScrollStateChanged(true);
                        } else {
                            onScrollListener.onScrollStateChanged(false);
                        }
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (onScrollListener != null) {
                    LinearLayoutManager linearLayoutManager = getLinearLayoutManager(recyclerView);
                    if (linearLayoutManager != null) {
                        boolean callScroll = false;
                        if (linearLayoutManager.getOrientation() == RecyclerView.VERTICAL && dy > 0) {
                            callScroll = true;
                        } else if (linearLayoutManager.getOrientation() == RecyclerView.HORIZONTAL
                                && dx > 0) {
                            callScroll = true;
                        }
                        if (callScroll) {
                            onScrollListener.onScrolled(linearLayoutManager.getItemCount(),
                                    linearLayoutManager.findLastVisibleItemPosition());
                        }
                    }
                }
            }

            private LinearLayoutManager getLinearLayoutManager(RecyclerView recyclerView) {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager instanceof LinearLayoutManager) {
                    return (LinearLayoutManager) layoutManager;
                }
                return null;
            }
        });
    }

    /**
     * Set Formated date TextView
     */
    @BindingAdapter(value = {"bindInitialDate", "bindInitDateFormat", "bindEndDateFormat"})
    public static void bindFormatDate(TextView textView, String initialDate, String initDateFormat, String endDateFormat) {
        String date = UtilsCommon.formatDate(initialDate, initDateFormat, endDateFormat);
        textView.setText(date);
    }

    @BindingAdapter(value = {"bindInitialDate", "bindInitDateFormat", "bindEndDateFormat"})
    public static void bindFormatDate(TextInputEditText textView, String initialDate, String initDateFormat, String endDateFormat) {
        String date = UtilsCommon.formatDate(initialDate, initDateFormat, endDateFormat);
        textView.setText(date);
    }

    /**
     * Set Formated date TextView
     */
    @BindingAdapter(value = {"bindFormatDateSociety"})
    public static void bindFormatDateSociety(TextView textView, String initialDate) {
        textView.setText("-");
        if (!TextUtils.isEmpty(initialDate)) {
            String date = UtilsCommon.formatDate(initialDate, "yyyy-MM-dd'T'hh:mm:ss", "dd/MM/yyyy");
            textView.setText(date);
        }
    }

    /**
     * Set linear background
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    @BindingAdapter(value = {"bindLinearBG"})
    public static void setLinearBG(LinearLayout linearLayout, int resBG) {
        linearLayout.setBackground(linearLayout.getResources().getDrawable(resBG));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @BindingAdapter(value = {"bindLinearBGTint"})
    public static void setLinearBGTint(LinearLayout linearLayout, int color) {
        linearLayout.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @BindingAdapter(value = {"bindTextColor"})
    public static void setTextColor(TextView textView, int color) {
        textView.setTextColor(textView.getResources().getColor(color));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @BindingAdapter(value = {"bindImageBG"})
    public static void bindImageBG(ImageView imageView, int resBG) {
        imageView.setBackground(imageView.getResources().getDrawable(resBG));
    }

    @BindingAdapter(value = {"imageUrl", "imageRes", "imageDrawable", "needCorner", "cornerRadius", "requestOptions"}, requireAll = false)
    public static void bindImageUrl(final ImageView view,
                                    final String imageUrl,
                                    int imageRes,
                                    Drawable imageDrawable,
                                    boolean needCorner,
                                    int cornerRadius,
                                    RequestOptions requestOption) {

        if (!TextUtils.isEmpty(imageUrl) && (imageUrl.contains("http") || imageUrl.contains("https"))) {
            if (requestOption == null) {
                /*if (needCorner) {
                    requestOption = getRoundCornerOptionsSimple(view.getContext().getApplicationContext(),
                            cornerRadius == 0 ? 4 : cornerRadius, R.drawable.pla_hold_middle_sqa);
                } else {
                    requestOption = ((MyApplication) view.getContext().getApplicationContext())
                            .glideBaseOptions(R.drawable.pla_hold_middle_sqa);
                }*/
            }

            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(requestOption)
                    .into(view);
        } else if (imageRes != 0) {
            if (requestOption != null) {
                Glide.with(view.getContext())
                        .load(imageRes)
                        .apply(requestOption)
                        .into(view);
            } else {
                view.setImageResource(imageRes);
            }
        } else if (imageDrawable != null) {
            if (requestOption != null) {
                Glide.with(view.getContext())
                        .load(imageDrawable)
                        .apply(requestOption)
                        .into(view);
            } else {
                view.setImageDrawable(imageDrawable);
            }
        } else {
           /* if (needCorner) {
                requestOption = ((MyApplication) view.getContext().getApplicationContext())
                        .glideBaseOptions(R.drawable.pla_hold_middle_sqa);
            } else if (requestOption == null) {
                requestOption = ((MyApplication) view.getContext().getApplicationContext())
                        .glideBaseOptions(R.drawable.pla_hold_middle_sqa);
            }*/

            /*Glide.with(view.getContext())
                    .load("")
                    .apply(requestOption)
                    .into(view);*/
        }
    }

    public static RequestOptions getRoundCornerOptionsSimple(Context context, int radiosDp,
                                                             int placeHolder) {
        int radiosPx = convertDpToPixel(radiosDp, context);
        return ((MyApplication) context.getApplicationContext())
                .glideOptionRoundCornerSimple(radiosPx, placeHolder);
    }

    public static RequestOptions getRoundCornerOptions(Context context, int radiosDp,
                                                       int placeHolder) {
        int radiosPx = convertDpToPixel(radiosDp, context);
        return ((MyApplication) context.getApplicationContext())
                .glideOptionRoundCorner(radiosPx, placeHolder);
    }

    @SuppressLint("CheckResult")
    @BindingAdapter({"bindImageFile"})
    public static void bindImageFile(final ImageView view, File imageFile) {
        if (imageFile != null) {
            RequestOptions requestOption = ((MyApplication) view.getContext().getApplicationContext())
                    .glideBaseOptions(R.drawable.background_gray);
//            requestOption.override(500);

            Glide.with(view.getContext())
                    .load(imageFile)
                    .apply(requestOption)
                    .into(view);
        }
    }

    @SuppressLint("CheckResult")
    @BindingAdapter({"loadDocURL"})
    public static void bindDocumentURL(final ImageView view, String documentUrl) {
        if (!TextUtils.isEmpty(documentUrl)) {
            RequestOptions requestOption = ((MyApplication) view.getContext().getApplicationContext())
                    .glideBaseOptions(R.drawable.background_gray);
//            requestOption.override(500);

            Glide.with(view.getContext())
                    .load(documentUrl)
                    .apply(requestOption)
                    .into(view);
        }
    }


    @BindingAdapter({"bindDashText"})
    public static void bindDashText(final AppCompatTextView view, String text) {
        view.setText(!TextUtils.isEmpty(text) ? text : "-");
    }

    @BindingAdapter({"bindDashText"})
    public static void bindDashText(final TextView view, String text) {
        view.setText(!TextUtils.isEmpty(text) ? text : "-");
    }

    @SuppressLint("NotifyDataSetChanged")
    @BindingAdapter(value = {"bindAdapterDashboardService", "bindGeneralItemListener"}, requireAll = false)
    public static void bindAdapterDashboard(final RecyclerView recyclerView,
                                            final List<PojoDashboardServiceData> listData,
                                            GeneralItemListener itemListener) {
        if (recyclerView.getAdapter() == null) {
            AdapterDashboardService adapter = new AdapterDashboardService(listData, itemListener);
            setGridAdapterSimple(recyclerView, adapter, 2);
        } else {
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }


    private static void setLinearAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter, int
            orientation) {
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext());
        linearLayoutManager.setOrientation(orientation);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @BindingAdapter(value = {"bindAdapterSchemes", "bindListenerSchemes"}, requireAll = false)
    public static void bindAdapterSchemes(final RecyclerView recyclerView,
                                          final List<PojoSchemes> listData, GeneralItemListener itemListener) {
        if (recyclerView.getAdapter() == null) {
            RequestOptions requestOptions = getRoundCornerOptionsSimple(recyclerView.getContext().getApplicationContext(),
                    40, R.drawable.bg_round);
            AdapterSchemes adapter = new AdapterSchemes(listData, itemListener, requestOptions);
            setLinearAdapter(recyclerView, adapter, LinearLayoutManager.VERTICAL);
        } else {
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    /**
     * Banner
     */
    @BindingAdapter(value = {"adapterViewPagerBanner", "listenerViewPagerBanner",
            "viewPageChangeListener"})
    public static void bindAdapterViewPagerBanner(final ViewPager view, List<PojoBannerData> listData,
                                                  GeneralItemListener listenerBannerPager,
                                                  ViewPageChangeListener viewPageChangeListener) {
        //boolean isFirstTime = true;
        if (view.getAdapter() != null) {
            //isFirstTime = false;
            view.setAdapter(null);
            view.setPageTransformer(false, null);
        }

        RequestOptions requestOption = getRoundCornerOptions(view.getContext().getApplicationContext(),
                180, R.drawable.bg_dashboard_white_logo);

        AdapterPagerHomeBanner adapterImagePager = new AdapterPagerHomeBanner(listData,
                listenerBannerPager, requestOption, convertDpToPixel(2, view.getContext()));
        view.setAdapter(adapterImagePager);

        ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(view, adapterImagePager);
        fragmentCardShadowTransformer.setViewPageChangeListener(viewPageChangeListener);
        fragmentCardShadowTransformer.enableScaling(true);
        view.setPageTransformer(false, fragmentCardShadowTransformer);

      /*  if (extensiblePageIndicator != null) {
            extensiblePageIndicator.setViewPager(view);
        }*/

        if (listData != null) {
            view.setOffscreenPageLimit(listData.size());
        }
    }


    @BindingAdapter("bindSpinnerState")
    public static void bindSpinnerState(final Spinner view,
                                        final ObservableList<PojoPlace> arrayList) {
        ArrayAdapter<PojoPlace> adapter = new ArrayAdapter<>(view.getContext(),
                R.layout.simple_spinner_black_item, R.id.txtSpinnerText, arrayList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_black_item);
        view.setAdapter(adapter);
    }

    @BindingAdapter("bindSpinnerDistrict")
    public static void bindSpinnerDistrict(final Spinner view,
                                           final ObservableList<PojoPlace> arrayList) {
        ArrayAdapter<PojoPlace> adapter = new ArrayAdapter<>(view.getContext(),
                R.layout.simple_spinner_black_item, R.id.txtSpinnerText, arrayList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_black_item);
        view.setAdapter(adapter);
    }

    @BindingAdapter("setDynamicColor")
    public static void setDynamicColor(final TextView view, int color) {
        if (color == 0) {
            color = Color.parseColor("#F0C4DF");
        }
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(view.getContext(), R.drawable.bg_button_small_round);
        if (unwrappedDrawable != null) {
            Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
            DrawableCompat.setTint(wrappedDrawable, color);
            view.setBackground(wrappedDrawable);
        }
    }

    @BindingAdapter("setViewBackColor")
    public static void setViewBackColor(final View view, int color) {
        if (color == 0) {
            color = Color.parseColor("#F0C4DF");
        }
        view.setBackgroundColor(color);
    }

    @BindingAdapter(value = {"bindAdapterGridState"})
    public static void bindAdapterSchemes(final RecyclerView recyclerView, final List<PojoPlace> listData) {
        if (recyclerView.getAdapter() == null) {
            AdapterGridState adapter = new AdapterGridState(listData);
            setGridAdapterSimple(recyclerView, adapter, 2);
        } else {
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    /**
     * Adapter Dashboard
     */
    @SuppressLint("NotifyDataSetChanged")
    @BindingAdapter(value = {"bindAdapterSchemesFilter", "bindGeneralItemListener"}, requireAll = false)
    public static void bindAdapterSchemesFilter(final RecyclerView recyclerView,
                                                final List<PojoSchemesFilterData> listData,
                                                GeneralItemListener itemListener) {
        if (recyclerView.getAdapter() == null) {
            AdapterSchemesFilter adapter = new AdapterSchemesFilter(listData, itemListener);
            setLayoutManagerAdapter(recyclerView, adapter, LinearLayoutManager.VERTICAL);
        } else {
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    /**
     * Adapter Dashboard
     */
    @SuppressLint("NotifyDataSetChanged")
    @BindingAdapter(value = {"bindAdapterSectors", "bindGeneralItemListener"}, requireAll = false)
    public static void bindAdapterSectors(final RecyclerView recyclerView,
                                          final List<PojoSectorsData> listData,
                                          GeneralItemListener itemListener) {
        if (recyclerView.getAdapter() == null) {
            AdapterSectors adapter = new AdapterSectors(listData, itemListener);
            setLayoutManagerAdapter(recyclerView, adapter, LinearLayoutManager.VERTICAL);
        } else {
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    /**
     * Adapter Dashboard
     */
    @SuppressLint("NotifyDataSetChanged")
    @BindingAdapter(value = {"bindAdapterMinDept", "bindGeneralItemListener"}, requireAll = false)
    public static void bindAdapterMinDept(final RecyclerView recyclerView,
                                          final List<PojoMinDeptData> listData,
                                          GeneralItemListener itemListener) {
        if (recyclerView.getAdapter() == null) {
            AdapterMinDept adapter = new AdapterMinDept(listData, itemListener);
            setLayoutManagerAdapter(recyclerView, adapter, LinearLayoutManager.VERTICAL);
        } else {
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    @BindingAdapter("bindBGTint")
    public static void bindBGTint(final View view, final int color) {
        //view.setBackgroundTintList(view.getContext().getResources().getColorStateList(color));
        view.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }

    @BindingAdapter({"setUpMoreInfoText"})
    public static void bindMoreInfoText(TextView view, ClickableSpan spannablePrivacyData) {
        if (view != null) {
            createLink(view, view.getResources().getString(R.string.text_more_details),
                    view.getResources().getString(R.string.text_more_details_click),
                    spannablePrivacyData);
        }
    }


    private static void createLink(TextView targetTextView, String completeString, String
            partToClick, ClickableSpan clickableAction) {
        SpannableString spannableString = new SpannableString(completeString);
        if (completeString.contains(partToClick)) {
            int startPosition = completeString.indexOf(partToClick);
            int endPosition = completeString.lastIndexOf(partToClick) + partToClick.length();
            spannableString.setSpan(clickableAction, startPosition, endPosition, Spanned
                    .SPAN_INCLUSIVE_EXCLUSIVE);
            targetTextView.setText(spannableString);
            targetTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }
}
