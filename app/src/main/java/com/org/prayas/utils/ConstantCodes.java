package com.org.prayas.utils;

import android.os.Environment;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Naria Sachin on Jan, 15 2020 12:49.
 */

public class ConstantCodes {
    public static final int RESPONSE_SUCCESS = 1;
    public static final int RESPONSE_TOKEN_EXPIRE = 2;
    public static final int REQUEST_WRITE_STORAGE_PERMISSION_DOC = 105;
    public static final int REQUEST_CODE_DOC = 20;
    public static final int REQUEST_CAMERA_PERMISSION = 102;
    public static final int REQUEST_CAMERA_IMAGE_RESULT = 30;

    //  URLs
    public static final String URL_SIGN_IN = "https://prayas.nic.in/SingleSignOn.aspx";

    //    PAGEs
    public static final String PAGE_SECTORS = "Sectors";
    public static final String PAGE_MIN_DEPT = "Min & Dept";

    public static final String RESIDENTIAL = "Residential";
    public static final String BUSINESS = "Business";
    public static final String RENT = "Rent";
    public static final String OTHER = "Other";

    public static final String TOKEN = "sfsdfsdsdfsdffds";
    public static final String MINISTRY_ID = "9";
    public static final String SCHEME_ID = "48";

    public static final String IS_FROM_PROFILE = "isFromProfile";

    public static final String PENDING = "Pending";
    public static final String ACCEPTED = "Accepted";
    public static final String REJECTED = "Rejected";

    public static final int RESPONSE_DATA_COUNT_PP = 20;

    public static long VALID_IMAGE_FILE_SIZE_KB = 1024 * 5;//2MB
    public static final String FILE_PATH_COMPRESSED = "Compress/";

    public static final String PATTERN_PAN_CARD = "[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}";
    public static final String PATTERN_PASSPORT = "^[A-PR-WYa-pr-wy][1-9]\\d\\s?\\d{4}[1-9]$"; //Indian Passport
    public static final String PATTERN_DRIVING_LICENCE = "^(([A-Za-z]{2}[0-9]{2})( )|([A-Za-z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7}$"; //Indian Driving Licence
    public static final String PATTERN_RATION_CARD = "^([a-zA-Z0-9]){8,16}\\s*$"; //Indian Ration Card
    public static final String PATTERN_VOTER_ID = "^([a-zA-Z]){3}([0-9]){7}?$"; //Indian Voter Id

    public static final String CARD_IMAGE_PATH = Environment.getExternalStorageDirectory() +
            "/socj/.socj/";

    public static final String APP_DIR = "SOC_J";

    public static final String FILE_SEPERATOR = "/";
    public static final String PATH_MEDIA = ".media/";

    public static final String DOC_PATH = "Document/";
    public static final String DOC_PATH_ORIGIONAL = "Original/";

    public static String UNIQUE_ID = "";
    public static final int INVALIDATION_POSITION = 0;
    public static final String INVALIDATION_POS = "0";
    public static final long OTP_TIME = 2 * 60 * 1000;
    public static final int DATE_HOUR = 4;
    public static final long DATE_HOUR_BEFORE = (DATE_HOUR * 60 * 60 * 1000);

    //    ITEM MOVE COUNTs
    public static final int MOVE_TO_SOCIETY_DETAILS = 0;
    public static final int MOVE_TO_FOR_MONEY_LENDING = 1;
    public static final int MOVE_TO_FOR_HOUSING_SOCIETY = 2;

    public static final int MOVE_TO_HUF_BIN_SANSTHAPIT_DETAILS = 0;
    public static final int MOVE_TO_OTHER_DETAILS = 2;
    public static final int MOVE_TO_BUSINESS_DETAILS = 0;
    public static final int MOVE_TO_DOCUMENTS = 1;
    public static final int MOVE_TO_HISTORY = 2;

    public static final int MOVE_TO_TAB_TWO = 0;
    public static final int MOVE_TO_TAB_THREE = 1;

//    TODO: ML OLD WAY
    /*public static final int MOVE_TO_HUF_BIN_SANSTHAPIT_DETAILS = 0;
    public static final int MOVE_TO_BUSINESS_DETAILS = 1;
    public static final int MOVE_TO_OTHER_DETAILS = 2;
    public static final int MOVE_TO_DOCUMENTS = 3;
    public static final int MOVE_TO_HISTORY = 4;*/

    public static final int MOVE_TO_ADMINISTRATIVE = 0;
    public static final int MOVE_TO_ACCOUNT_DETAILS = 1;
    public static final int MOVE_TO_OTHER_INFORMATION = 2;
    public static final int MOVE_TO_SOCIETY_HISTORY = 3;


    public static final Map<String, String> myColorMap;

    static {
        Map<String, String> aMap = new HashMap<>();
        aMap.put("1","#F0C4DF");
        aMap.put("2", "#E5CCF2");
        aMap.put("3", "#CECFF5");
        aMap.put("4", "#E9C6C6");
        aMap.put("5", "#9FE5CF");
        aMap.put("6", "#EBDCB9");
        aMap.put("7", "#AEECB3");
        myColorMap = Collections.unmodifiableMap(aMap);
    }

   /* public let sectorsList = [
    FilterSectionModel(dict: ["selected" : false, "title" : "Agricultural & Rural", "colorHex" : "#F0C4DF", "id" : "1"]),
    FilterSectionModel(dict: ["selected" : false, "title" : "Social & Welfare", "colorHex" : "#E5CCF2", "id" : "2"]),
    FilterSectionModel(dict: ["selected" : false, "title" : "Finance & Economy", "colorHex" : "#CECFF5", "id" : "3"]),
    FilterSectionModel(dict: ["selected" : false, "title" : "Infrastructure & Resources", "colorHex" : "#E9C6C6", "id" : "4"]),
    FilterSectionModel(dict: ["selected" : false, "title" : "Technology & Governance", "colorHex" : "#9FE5CF", "id" : "5"]),
    FilterSectionModel(dict: ["selected" : false, "title" : "Foreign & Security", "colorHex" : "#EBDCB9", "id" : "6"]),
    FilterSectionModel(dict: ["selected" : false, "title" : "Human Resources", "colorHex" : "#AEECB3", "id" : "7"])]
*/
    public static final String FIELD_SEPERATOR = ",";
}
