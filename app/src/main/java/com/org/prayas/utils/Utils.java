package com.org.prayas.utils;

import static com.org.prayas.utils.ConstantCodes.FIELD_SEPERATOR;
import static com.org.prayas.utils.crypt.CryptUniqueIdGenerator.getCryptUniqueId;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.core.content.FileProvider;

import com.org.prayas.BuildConfig;
import com.org.prayas.utils.crypt.CryptLib;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by Naria Sachin on Jan, 15 2020 13:34.
 */

public class Utils {

    public static final String DD_MM_YYYY = "dd/MM/yyyy";

    /**
     * Shows the snackbar
     *
     * @param toastMessage Message for the snackbar
     * @param viewLayout   View layout along which snackbar will be shown
     */
    public static void showSnackBar(View viewLayout, String toastMessage) {
        try {
            Snackbar.make(viewLayout, toastMessage, Snackbar.LENGTH_LONG).show();
        } catch (Exception exception) {
            com.org.prayas.utils.Logger.log(exception.toString());
        }
    }

    /**
     * Closes the open keyboard if exist in focus
     *
     * @param activity Activity calling class
     */
    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null) {
                IBinder iBinder = activity.getCurrentFocus().getWindowToken();
                if (iBinder != null && inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
                }
            }
        } catch (Exception exception) {
            com.org.prayas.utils.Logger.log(exception.toString());
        }
    }

    public static int convertDpToPixel(float dp, Context context) {
        return Math.round(dp * ((float) context.getResources().getDisplayMetrics().densityDpi /
                DisplayMetrics.DENSITY_DEFAULT));
    }

    public static String getTrimmedData(String data) {
        return (!TextUtils.isEmpty(data) ? data.trim() : "");
    }


    public static String getUniqueIDWhitRandomString() {
        return getCryptUniqueId() + "-" + randomString();
    }

    static char[] symbols = "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm".toCharArray();

    private static String randomString() {
        SecureRandom generator = new SecureRandom();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = 5;
        int tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = generator.nextInt(symbols.length - 1);
            randomStringBuilder.append(symbols[tempChar]);
        }
        return randomStringBuilder.toString();
    }

    public static String getEncryptedString(String value, String uniqueId) {
        String newValue = "";
        try {
            newValue = !TextUtils.isEmpty(value) && !TextUtils.isEmpty(uniqueId) ?
                    new CryptLib().EncryptString(value, uniqueId) : "";
        } catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException |
                BadPaddingException | IllegalBlockSizeException |
                InvalidAlgorithmParameterException | UnsupportedEncodingException e) {
            com.org.prayas.utils.Logger.e("UtilsCommon  setStringEncrypted", e.toString());
        }
        return newValue;
    }

    public static String getEncryptedString(int value, String uniqueId) {
        return getEncryptedString(String.valueOf(value), uniqueId);
    }

    public static String getDecryptedString(String value, String uniqueId) {
        String newValue = "";
        try {
            newValue = !TextUtils.isEmpty(value) && !TextUtils.isEmpty(uniqueId) ?
                    new CryptLib().DecryptString(value, uniqueId) : "";
            Log.e("getDecryptedString", "=" + newValue);
        } catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException |
                BadPaddingException | IllegalBlockSizeException |
                InvalidAlgorithmParameterException | UnsupportedEncodingException e) {
            com.org.prayas.utils.Logger.e("UtilsCommon  setStringEncrypted", e.toString());
        }
        return newValue;
    }

    /**
     * Attach suffix to day
     */
    public static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    /**
     * Date formatter
     */
    public static String formatDate(String dayNumberSuffix, Calendar calendar) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd'" + dayNumberSuffix + "' MMMM yyyy", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    public static String formatDate(Calendar calendar, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        if (calendar.getTime() != null) {
            return dateFormat.format(calendar.getTime());
        } else {
            return "";
        }
    }

/*    public static void showMessageYesNo(Context context, String message, DialogInterface
            .OnClickListener
            okListener) {
            AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(context, R.style.DialogTheme);
            mAlertDialog.setMessage(message)
                    .setPositiveButton(context.getResources().getString(R.string.text_yes), okListener)
                    .setNegativeButton(context.getResources().getString(R.string.text_no), null)
                    .create()
                    .show();
    }*/

    public static String getApplicantName(String firstName, String middleName, String lastName) {
        String applicantName = "";

        if (!TextUtils.isEmpty(firstName)) {
            applicantName += firstName + " ";
        }

        if (!TextUtils.isEmpty(middleName)) {
            applicantName += middleName + " ";
        }

        if (!TextUtils.isEmpty(lastName)) {
            applicantName += lastName + " ";
        }

        return applicantName.trim();
    }


    public static File createTempCameraFile(Context context, boolean isImage) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String cameraFileName;
        File storageDir;
        String extension;
        if (isImage) {
            cameraFileName = "JPEG_" + timeStamp;
            storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            extension = ".jpg";
        } else {
            cameraFileName = "MP4_" + timeStamp;
            storageDir = context.getExternalFilesDir(Environment.DIRECTORY_MOVIES);
            extension = ".mp4";
        }
        return File.createTempFile(
                cameraFileName,  /* prefix */
                extension,         /* suffix */
                storageDir      /* directory */
        );
    }

    public static File getCameraFile(Context context, boolean isImage) {
        File fileTemporary = null;
        try {
            fileTemporary = createTempCameraFile(context, isImage);
        } catch (IOException exception) {
            com.org.prayas.utils.Logger.e(context.getClass().getSimpleName() + " openCamera", exception.toString());
        }
        return fileTemporary;

    }

    public static Uri getTempCameraFileUri(Context context, File file) {
        if (file != null) {
            return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
        } else {
            return null;
        }
    }

    public static String getMonthFromNumber(String monthNum) {
        if (!TextUtils.isEmpty(monthNum)) {
            Calendar cal = Calendar.getInstance();
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat month_date = new SimpleDateFormat("MMMM", Locale.US);
            cal.set(Calendar.MONTH, Integer.parseInt(monthNum) - 1);
            return month_date.format(cal.getTime());
        }
        return "";
    }

    public static List<String> getSplitList(String data) {
        if (!TextUtils.isEmpty(data)) {
            List<String> stringList = Arrays.asList(data.split(FIELD_SEPERATOR, -1));
            if (stringList.isEmpty()) {
                stringList.add(data);
            }
            return stringList;
        } else {
            return null;
        }
    }

    public static int getColor(String hexCode) {
        if (TextUtils.isEmpty(hexCode)) {
            hexCode = "#F0C4DF";
        }
        return Color.parseColor(hexCode);
    }

    public static String getFirstThreeChar(String data) {
        if (!TextUtils.isEmpty(data)) {
            if (data.length() >= 3) {
                return data.substring(0, 3);
            } else {
                return data;
            }
        } else {
            return "";
        }
    }

    public static String getLastTwoChar(String data) {
        if (!TextUtils.isEmpty(data)) {
            if (data.length() >= 2) {
                return data.substring(data.length() - 2);
            } else {
                return data;
            }
        } else {
            return "";
        }
    }

    public static int stringToInt(String data) {
        if (!TextUtils.isEmpty(data) && TextUtils.isDigitsOnly(data)) {
            return Integer.parseInt(data);
        } else {
            return 0;
        }
    }

    public static float stringToFloat(String data) {
        float finalData = 0;
        if (!TextUtils.isEmpty(data)) {
            try {
                finalData = Float.parseFloat(data);
            } catch (Exception e) {
                finalData = 0;
            }
        }
        return finalData;
    }

}
