package com.org.prayas.utils;

import static com.org.prayas.utils.ConstantCodes.CARD_IMAGE_PATH;
import static com.org.prayas.utils.ConstantCodes.DOC_PATH;
import static com.org.prayas.utils.ConstantCodes.DOC_PATH_ORIGIONAL;
import static com.org.prayas.utils.ConstantCodes.FILE_PATH_COMPRESSED;
import static com.org.prayas.utils.ConstantCodes.FILE_SEPERATOR;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableInt;

import com.org.prayas.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;


/**
 * Created by Naria Sachin on Jul, 31 2020 19:41.
 */

public class FileUtils {

    // get path of File for grter than kitkat
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    } else {
                        return "/storage/" + split[0] + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    String fileName = getFilePath(context, uri);
                    if (fileName != null) {
                        return Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName;
                    }

                    String id = DocumentsContract.getDocumentId(uri);

                    if (id.startsWith("raw:")) {
                        id = id.replaceFirst("raw:", "");
                        File file = new File(id);
                        if (file.exists())
                            return id;
                    }

                    final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                    return getDataColumn(context, contentUri, null, null);

                    /*final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.parseLong(id));*/
//                    return  getDataColumn(context, contentUri, null, null);
                } else if (isGoogleDocs(uri)) {
                    return getFileFromDoc(context, uri);
                } else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            } else if ("content".equalsIgnoreCase(uri.getScheme())) {
                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }
        return null;
    }

    public static String getFilePath(Context context, Uri uri) {

        Cursor cursor = null;
        final String[] projection = {
                MediaStore.MediaColumns.DISPLAY_NAME
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, null, null,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private static String getFileFromDoc(Context context, Uri returnUri) {
        String tmppath = "";
        Cursor returnCursor = context.getContentResolver().query(returnUri, null, null, null, null);
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();

        File dir = new File(CARD_IMAGE_PATH + DOC_PATH + DOC_PATH_ORIGIONAL);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            InputStream input = context.getContentResolver().openInputStream(returnUri);
            File tmpfile = new File(dir, returnCursor.getString(nameIndex));
            OutputStream output = new FileOutputStream(tmpfile);
            try {
                try {
                    byte[] buffer = new byte[4 * 1024]; // or other buffer size
                    int read;

                    while ((read = input.read(buffer)) != -1) {
                        output.write(buffer, 0, read);
                    }
                    output.flush();
                } finally {
                    returnCursor.close();
                    output.close();
                }
                input.close();
                tmppath = tmpfile.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace(); // handle exception, define IOException and others
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return tmppath;
    }


    private static boolean isGoogleDocs(Uri uri) {
        return "com.google.android.apps.docs.storage".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }


    public static boolean checkFileExists(String path) {
        File f = new File(path);
        return checkFileExists(f);
    }

    public static boolean checkFileExists(File file) {
        return file != null && file.exists() && file.length() > 0;
    }

    public static boolean setUpSIze(long length, int attach) {
        double sizeKb = (length) / 1024D;
        if (attach == 1 && sizeKb <= 5120) {//image
            return true;
        } else if (attach == 2 && sizeKb <= 10240) {//video
            return true;
        } else if (attach == 3 && sizeKb <= 5120) {//doc
            return true;
        }
        return false;
    }


    public static String[] getDocPath(Uri uri, Context mContext, ObservableInt observerSnackBarInt) {
        String path = "";
        String image = "0";

        if (mContext.getContentResolver() != null) {
            Cursor returnCursor = mContext.getContentResolver().query(uri, null, null, null, null);
            if (returnCursor != null) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                returnCursor.moveToFirst();
                if (nameIndex >= 0 && sizeIndex >= 0) {
                    String fileName = returnCursor.getString(nameIndex);
                    boolean[] fileTypeData = checkOtherFileType(fileName);
                    long fileSize = returnCursor.getLong(sizeIndex);
                    if (fileTypeData[0]) {
                        if (setUpSIze(fileSize, (fileTypeData[1] ? 1 : 3))) {
                            image = fileTypeData[1] ? "1" : "0";
                            path = fileName;
                        } else {
                            observerSnackBarInt.set(R.string.error_document_validation);
                        }
                    } else {
                        observerSnackBarInt.set(R.string.error_valid_pdf_format);
                    }
                }
            } else {
                File file = new File(URI.create(uri.toString()));
                String fType = getMimeType(mContext, uri);
                boolean[] fileTypeData = checkMimeType(fType);
                if (fileTypeData[0]) {
                    if (checkValidFileSize(file.getAbsolutePath(), (fileTypeData[1] ? 1 : 3))) {
                        image = fileTypeData[1] ? "1" : "0";
                        path = file.getAbsolutePath();
                    } else {
                        observerSnackBarInt.set(R.string.error_document_validation);
                    }
                } else {
                    observerSnackBarInt.set(R.string.error_valid_pdf_format);
                }
            }
        }
        return new String[]{path, image};
    }


    public static String[] getDocPath(Uri uri, Context mContext) {
        String path = "";
        String image = "0";

        if (mContext.getContentResolver() != null) {
            Cursor returnCursor = mContext.getContentResolver().query(uri, null, null, null, null);
            if (returnCursor != null) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                returnCursor.moveToFirst();
                if (nameIndex >= 0 && sizeIndex >= 0) {
                    String fileName = returnCursor.getString(nameIndex);
                    boolean[] fileTypeData = checkOtherFileType(fileName);
                    long fileSize = returnCursor.getLong(sizeIndex);
                    if (fileTypeData[0]) {
                        if (setUpSIze(fileSize, (fileTypeData[1] ? 1 : 3))) {
                            image = fileTypeData[1] ? "1" : "0";
                            path = fileName;
                        } else {
                            Toast.makeText(mContext, R.string.error_document_validation, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mContext, R.string.error_valid_pdf_format, Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                File file = new File(URI.create(uri.toString()));
                String fType = getMimeType(mContext, uri);
                boolean[] fileTypeData = checkMimeType(fType);
                if (fileTypeData[0]) {
                    if (checkValidFileSize(file.getAbsolutePath(), (fileTypeData[1] ? 1 : 3))) {
                        image = fileTypeData[1] ? "1" : "0";
                        path = file.getAbsolutePath();
                    } else {
                        Toast.makeText(mContext, R.string.error_document_validation, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, R.string.error_valid_pdf_format, Toast.LENGTH_SHORT).show();
                }
            }
        }
        return new String[]{path, image};
    }

    public static File saveDocFile(Context context, Uri sourceUri, String fileName, boolean isImage) {
        File f;
        String mImagePath;
        //File outputFile = null;
        f = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "");
        mImagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + FILE_SEPERATOR + fileName;

        if (!f.exists()) {
            f.mkdirs();
        }

        if (sourceUri != null) { //&& outputFile != null
            FileOutputStream fos = null;
            InputStream fis = null;
            try {
                fis = context.getContentResolver().openInputStream(sourceUri);
                fos = new FileOutputStream(mImagePath, false);
                //fos = new FileOutputStream(outputFile, false);
                byte[] buf = new byte[1024];
                int len;
                while ((len = fis.read(buf)) != -1) {
                    fos.write(buf, 0, len);
                }
                fos.close();
                fis.close();
                return new File(mImagePath);
                //return outputFile;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fos != null)
                        fos.close();
                    if (fis != null)
                        fis.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static boolean[] checkOtherFileType(String filePath) {
        boolean isValid = false;
        boolean isImage = false;
        if (!TextUtils.isEmpty(filePath)) {
            String filePathInLowerCase = filePath.toLowerCase();
            if (filePathInLowerCase.endsWith(".pdf")) {
                isValid = true;
            } else if (filePathInLowerCase.endsWith(".jpg")
                    || filePathInLowerCase.endsWith(".png")
                    || filePathInLowerCase.endsWith(".jpeg")) {
                isValid = true;
                isImage = true;
            }
        }
        return new boolean[]{isValid, isImage};
    }

    public static boolean[] checkMimeType(String mimeType) {
        boolean isValid = false;
        boolean isImage = false;
        if (!TextUtils.isEmpty(mimeType)) {
            String mimeTypeInLowerCase = mimeType.toLowerCase();
            if (TextUtils.equals("pdf", mimeTypeInLowerCase) || mimeTypeInLowerCase.toLowerCase().contains("pdf")) {
                isValid = true;
            } else if (TextUtils.equals("jpg", mimeTypeInLowerCase) || mimeTypeInLowerCase.toLowerCase().contains("jpg")) {
                isValid = true;
                isImage = true;
            } else if (TextUtils.equals("jpeg", mimeTypeInLowerCase) || mimeTypeInLowerCase.toLowerCase().contains("jpeg")) {
                isValid = true;
                isImage = true;
            } else if (TextUtils.equals("png", mimeTypeInLowerCase) || mimeTypeInLowerCase.toLowerCase().contains("png")) {
                isValid = true;
                isImage = true;
            }
        }
        return new boolean[]{isValid, isImage};
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension = "";
        if (uri != null) {
            if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                final MimeTypeMap mime = MimeTypeMap.getSingleton();
                extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
            } else {
                extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
            }
        }
        return extension;
    }

    public static boolean checkValidFileSize(String path, int isDocument) {
        File f = new File(path);
        return checkFileExists(f) && setUpSIze(f.length(), isDocument);
    }

    public static void DownloadFile(String fileURL, File directory) {
        try {

            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static String getBaseFileName(String str) {
        if (str == null) return "";
        int pos = str.lastIndexOf(".");
        if (pos == -1) return str;
        return str.substring(0, pos);
    }

    private static File getCacheDir(Context context) {
        File externalCacheDir = context.getExternalCacheDir();
        if (externalCacheDir != null) {
            return externalCacheDir;
        } else {
            return context.getCacheDir();
        }
    }

    public static File checkForValidImage(Context context, String tempFilePath) {
        if (!TextUtils.isEmpty(tempFilePath)) {
            String filename = tempFilePath.substring(tempFilePath.lastIndexOf("/") + 1);
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + FILE_SEPERATOR + filename);
            if (checkFileExists(file)) {
                return file;
            }
        }
        return null;
    }

    public static boolean validFileType(String filePath, String[] validExtension) {
        boolean isMatch = false;
        String extension = filePath.substring(filePath.lastIndexOf('.'));
        if (!TextUtils.isEmpty(extension)) {
            for (String type : validExtension) {
                if (extension.equalsIgnoreCase(type)) {
                    isMatch = true;
                    break;
                }
            }
        }
        return isMatch;
    }

    public static boolean validFileSize(long length, long validSizeKb) {
        long sizeKb = (length) / 1024;
        return (sizeKb <= validSizeKb);
    }

    public static File compressFullImage(Context context, File photoFile, long maxSize) {
        File file;
        if (checkFileExists(photoFile)) {
            double sizeKb = (photoFile.length()) / 1024D;
            if (sizeKb > maxSize) {
                return compressImageSize(context, photoFile, (maxSize / sizeKb), false);
            } else {
                return compressImageSize(context, photoFile, 0.0, true);
            }
        } else {
            file = photoFile;
        }
        return file;
    }

    private static File compressImageSize(Context context, File photoFile, double ratio, boolean isActual) {
        File file;
        Bitmap bmp = null;
        Bitmap scaledBitmap = null;
        InputStream input = null;
        InputStream input1 = null;

        ContentResolver cr = context.getContentResolver();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        try {
            input = cr.openInputStream(Uri.fromFile(photoFile));
            BitmapFactory.decodeStream(input, null, options);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        int actualWidth = options.outWidth;
        int actualHeight = options.outHeight;

        if (actualHeight <= 0) {
            actualHeight = 1;
        }
        if (actualWidth <= 0) {
            actualWidth = 1;
        }

        float maxHeight;
        float maxWidth;

        if (isActual) {
            maxHeight = actualHeight;
            maxWidth = actualWidth;
        } else {
            maxHeight = Math.round(actualHeight * ratio);
            maxWidth = Math.round(actualWidth * ratio);
        }

        float imgRatio = (float) actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            input1 = cr.openInputStream(Uri.fromFile(photoFile));
            bmp = BitmapFactory.decodeStream(input1, null, options);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input1 != null) {
                try {
                    input1.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2f, middleY - bmp.getHeight() / 2f,
                new Paint(Paint.FILTER_BITMAP_FLAG));
        ExifInterface exif;
        try {
            exif = new ExifInterface(photoFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                /*if (exif != null) {
                    timeLatLong = FetchGPSDataFromImages(exif);
                }*/
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(),
                    scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        OutputStream outStream = null;

        file = getFile(getFinalImagePath(context, FILE_PATH_COMPRESSED), ".jpg");
        try {
            outStream = new FileOutputStream(file);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 84, outStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outStream != null) {
                try {
                    outStream.flush();
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (scaledBitmap != null) {
                scaledBitmap.recycle();
            }
            if (bmp != null) {
                bmp.recycle();
            }
        }
        return file;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).
            final float totalPixels = width * height;
            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    public static String getFinalImagePath(Context context, String dir) {
        return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + FILE_SEPERATOR + dir;
    }


    public static File getCompressVideoFile(Context context) {
        return getFile(context.getExternalFilesDir(Environment.DIRECTORY_MOVIES) + FILE_SEPERATOR + FILE_PATH_COMPRESSED, ".mp4");
    }


    public static File getFile(String filePath, String extension) {
        File path = new File(filePath);
        if (!path.exists()) {
            path.mkdirs();
        }
        return (new File(path, System.currentTimeMillis() + extension));
    }

    public static void deleteFile(File file) {
        if (checkFileExists(file)) {
            file.delete();
        }
    }

    @NonNull
    public static String getMimeType(@NonNull File file) {
        String type = null;
        final String url = file.toString();
        final String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
        }
        if (type == null) {
            type = "image/*"; // fallback type. You might set it to */*
        }
        return type;
    }

}
