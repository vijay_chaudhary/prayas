package com.org.prayas.utils;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ResourceProvider {

    private final Context mContext;

    @Inject
    public ResourceProvider(Context mContext) {
        this.mContext = mContext;
    }

    public String getString(int resId) {
        return mContext.getResources().getString(resId);
    }

    public String getString(int resId, String value) {
        return mContext.getResources().getString(resId, value);
    }
}
