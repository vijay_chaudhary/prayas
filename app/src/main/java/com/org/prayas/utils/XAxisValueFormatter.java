package com.org.prayas.utils;

import android.util.Log;

import com.github.mikephil.charting.formatter.ValueFormatter;
import com.org.prayas.business_logic.pojo.PojoGraph;

import java.util.List;

public class XAxisValueFormatter extends ValueFormatter {
    private List<PojoGraph> mValues;
    private int mValueCount = 0;

    public XAxisValueFormatter(List<PojoGraph> values) {
        this.mValues = values;
        this.mValueCount = values.size();
    }

    @Override
    public String getFormattedValue(float value) {
        int index = Math.round(value);

        if (index < 0 || index >= mValueCount || index != (int) value) {
            Log.e("getFormattedValue", "=" + index);
            return "";
        } else {
            Log.e("getFormattedValue", "=" + index);
        }

        return (mValues != null && !mValues.isEmpty() ? mValues.get(index).getXAxisData() : "");
    }
}
