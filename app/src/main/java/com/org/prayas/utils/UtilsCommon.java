package com.org.prayas.utils;

import static com.org.prayas.utils.Utils.convertDpToPixel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.org.prayas.MyApplication;
import com.org.prayas.R;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Rupal Goswami on Nov, 12 2019 19:28.
 */
public class UtilsCommon {

    public static final int GR_SUBMITTED_STATUS = 0;
    public static final int GR_TRANSFER_STATUS = 1;
    public static final int GR_REASSIGNED_STATUS = 2;
    public static final int GR_ASSIGNED_STATUS = 3;
    public static final int GR_FORWARDED_INPUT_STATUS = 5;
    public static final int GR_RECEIVED_INPUT_STATUS = 6;
    public static final int GR_RESOLVED_STATUS = 7;
    public static final int GR_ESCALATE_STATUS = 8;
    public static final int GR_ClOSED_STATUS = 9;
    public static final int GR_AUTOClOSED_STATUS = 10;
    public static final int REQ_USER_CONSENT = 210;

    /**
     * Shows the snackbar
     *
     * @param toastMessage Message for the snackbar
     * @param viewLayout   View layout along which snackbar will be shown
     */
    public static void showSnackBar(View viewLayout, String toastMessage) {
        try {
            Snackbar.make(viewLayout, toastMessage, Snackbar.LENGTH_LONG).show();
        } catch (Exception exception) {
            com.org.prayas.utils.Logger.e("showSnackBar", exception.toString());
        }
    }

    /**
     * Provides error message depending on the message received from api being it non empty of
     * containing data
     *
     * @param context Context of calling class
     * @param message Message to be checked for
     * @return Error message if present else something went wong message
     */
    public static String getErrorMessage(Context context, String message) {
        if (!TextUtils.isEmpty(message)) {
            return message;
        } else {
            return context.getResources().getString(R.string.message_something_wrong);
        }
    }


    /**
     * Set the Spinner padding into Text
     *
     * @param isDropDown
     * @param context
     * @param textView
     */
    public static void setSpinnerTextPadding(boolean isDropDown, Context context,
                                             TextView textView) {
        if (isDropDown) {
            textView.setPadding(context.getResources().getDimensionPixelSize(R.dimen.margin_eight),
                    context.getResources().getDimensionPixelSize(R.dimen.margin_eight),
                    context.getResources().getDimensionPixelSize(R.dimen.margin_eight),
                    context.getResources().getDimensionPixelSize(R.dimen.margin_eight));
        } else {
            textView.setPadding(0, 0, 0, 0);
        }
    }

    /**
     * Used to open web url in browser
     *
     * @param url Web url to be opened in browser
     */
    public static void openWebPage(String url, Context context) {
        Uri webPage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webPage);
        if (!TextUtils.isEmpty(url) && isPDF(url)) {
            intent.setDataAndType(webPage, "application/pdf");
        }
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    public static boolean isPDF(String path) {
        if (path == null) {
            return false;
        } else
            return path.trim().endsWith("pdf") || path.trim().endsWith("Pdf") ||
                    path.trim().endsWith("PDF");
    }

    /**
     * Hides keyboard for the activity
     *
     * @param activity Activity reference for keyboard to be hidden
     */
    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null) {
                IBinder iBinder = activity.getCurrentFocus().getWindowToken();
                if (iBinder != null && inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
                }
            }
        } catch (Exception exception) {
            com.org.prayas.utils.Logger.e(activity.getClass().getSimpleName() + " hideKeyBoard", exception.toString());
        }
    }

    /**
     * Show date picker
     */
    public static void showDatePickerDialog(Activity activity, Calendar calendar,
                                            DatePickerDialog.OnDateSetListener onDateSetListener) {
        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(
                activity,
                onDateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

   /* public static void showDatePickerDialog(Activity activity, Calendar calendar, boolean endDate, PojoRegistrationSociety pojoRegistrationSociety
            , DatePickerDialog.OnDateSetListener onDateSetListener) {
        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(
                activity,
                onDateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        if (endDate && !TextUtils.isEmpty(pojoRegistrationSociety.getAccntDtlAuditStartDate())) {
            datePickerDialog.getDatePicker().setMinDate(Objects.requireNonNull(getDateFromView(pojoRegistrationSociety.getAccntDtlAuditStartDate())).getTime());
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        } else if (!TextUtils.isEmpty(pojoRegistrationSociety.getAccntDtlAuditEndDate())) {
            datePickerDialog.getDatePicker().setMaxDate(Objects.requireNonNull(getDateFromView(pojoRegistrationSociety.getAccntDtlAuditEndDate())).getTime());
        }

        datePickerDialog.show();
    }*/

    public static void showDatePickerDialog(Activity activity, Calendar calendar, boolean isEndDate,
                                            String startDate,
                                            DatePickerDialog.OnDateSetListener onDateSetListener) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                activity,
                onDateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        if (!isEndDate) {
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        }

        if (isEndDate && !TextUtils.isEmpty(startDate)) {
            datePickerDialog.getDatePicker().setMinDate(getDateFromView(startDate).getTime());
        }

        datePickerDialog.show();
    }

    public static String getDateToView(String date) {
        if (TextUtils.isEmpty(date)) {
            return "";
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ", Locale.getDefault());
        Date result;
        try {
            result = df.parse(date);
            System.out.println("date:" + result); //prints date in current locale
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy",
                    Locale.getDefault());
            return sdf.format(result);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Date getDateFromView(String date) {
        if (TextUtils.isEmpty(date)) {
            return null;
        }
        try {
            DateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            return simpleDateFormat.parse(date);
        } catch (ParseException ignored) {

        }
        return null;
    }

    public static String getDateToView(Long date) {
        Date result = new Date(date * 1000L);
        System.out.println("date:" + result); //prints date in current locale
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy",
                Locale.getDefault());
        return sdf.format(result);
    }

    public static void setCalenderFromView(Calendar cal, String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            cal.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public static String getDateTimeToView(String date) {
        if (TextUtils.isEmpty(date)) {
            return "";
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ", Locale.getDefault());
        Date result;
        try {
            result = df.parse(date);
            System.out.println("date:" + result); //prints date in current locale
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, hh:mm a",
                    Locale.getDefault());
            return sdf.format(result);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getDateTimeToView(Long date) {
        Date result = new Date(date * 1000L);
        System.out.println("date:" + result); //prints date in current locale
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, hh:mm a",
                Locale.getDefault());
        return sdf.format(result);
    }

    public static boolean isValidEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidMobile(String mobile) {
        return mobile.length() == 10;
    }

    public static boolean isValidYear(String year) {
        return year.length() == 4;
    }

    public static String mergeStrings(String str1, String str2) {
        return String.format(str1, str2);
    }

    /**
     * Format date
     */
    @SuppressLint("SimpleDateFormat")
    public static String formatDate(String date, String initDateFormat, String endDateFormat) {
        if (!TextUtils.isEmpty(date)) {
            try {
                Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
                SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
                assert initDate != null;
                return formatter.format(initDate);
            } catch (ParseException e) {
                return "";
            }
        } else {
            return "";
        }
    }

    public static String getFileName(String text) {
        try {
            int lastDot = text.lastIndexOf('/');
            return text.substring(lastDot + 1);
        } catch (Exception e) {
            return "";
        }
    }

    public static RequestOptions getRoundCornerOptionsSimple(Context context, int radiosDp,
                                                             int placeHolder) {
        int radiosPx = convertDpToPixel(radiosDp, context);
        return ((MyApplication) context.getApplicationContext())
                .glideOptionRoundCornerSimple(radiosPx, placeHolder);
    }

}
