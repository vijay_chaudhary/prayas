package com.org.prayas.view.activity.main;

import static com.org.prayas.business_logic.preferences.PrefKeys.prefIsUserLogIn;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefLoginUserId;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefUserEmail;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefUserImage;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefUserMobile;
import static com.org.prayas.utils.Utils.getDecryptedString;
import static com.org.prayas.utils.Utils.getEncryptedString;
import static com.org.prayas.utils.Utils.hideKeyboard;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.GeneralListener;
import com.org.prayas.business_logic.pojo.base.PojoMenu;
import com.org.prayas.business_logic.viewmodel.main.ViewModelMain;
import com.org.prayas.databinding.ActivityMainBinding;
import com.org.prayas.view.activity.BaseActivity;
import com.org.prayas.view.adapter.AdapterExpandable;
import com.org.prayas.view.fragment.dashboard.FragmentDashboard;
import com.org.prayas.view.fragment.schemes.FragmentSchemes;
import com.org.prayas.view.webview.FragmentWebView;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ActivityMain extends BaseActivity {

    private ViewModelMain mViewModel;
    private ActivityMainBinding mBinding;

    private ActionBarDrawerToggle mDrawerToggle;
    private AdapterExpandable expandableListAdapter;

    private boolean mDoubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mViewModel = new ViewModelProvider(this).get(ViewModelMain.class);
        mBinding.setViewmodel(mViewModel);
        mBinding.setGeneralListener(generalListener);
        mBinding.setOnClickNavHeader(onClickListener);

        initComponents();
        navigateToDashboard();
        setUpUserDetails();

        String data = getDecryptedString("mnZvG82kdo6t189+5Ro7ng==", "00000000-5e21-5356-ffff-ffffef05ac4a-QhZso");
        Log.e("new val ", "schemeId =" + data);
        data = getDecryptedString("pYij6a7GLsX5ceLWqKfxAA==", "00000000-5e21-5356-ffff-ffffef05ac4a-QhZso");
        //String data = getEncryptedString("7", "123456789");
        Log.e("new val ", "=" + data);

    }

    /**
     * Sets toolbar for the application
     */
    private void setToolbar() {
        setSupportActionBar(mBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        mViewModel.isShowToolbar.set(false);
    }

    /**
     * Initializes components
     */
    private void initComponents() {
        setToolbar();
        observeEvents();
        setDrawer();
        populateExpandableList();
        mViewModel.setNavBanner();
        mViewModel.observableLogout.set(R.string.text_logout);
    }

    /**
     * Observes events from live data
     */
    private void observeEvents() {
       /* mViewModel.getLiveDataIsValidProfileData().observe(this, isValid -> {
            if (isValid != null) {
                if (mPreferences.getBoolean(R.string.prefValidProfileData)) {
                    setUpBannerData();
                    navigateToRegistration(new PojoRegistrationData());
                } else {
                    Toast.makeText(mContext, getString(R.string.message_complete_profile),
                            Toast.LENGTH_LONG).show();
                    navigateToProfile(true);
                }
            }
        });*/
    }

    public int getToolbarHeight() {
        return mBinding.toolbar.getHeight();
    }

    /**
     * Updates drawer toggle for home button as well its type
     */
    private void updateDrawerToggle() {
        if (mDrawerToggle == null) {
            return;
        }

        boolean isRoot = getSupportFragmentManager().getBackStackEntryCount() == 0;
        mDrawerToggle.setDrawerIndicatorEnabled(isRoot);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(!isRoot);
            getSupportActionBar().setDisplayHomeAsUpEnabled(!isRoot);
            getSupportActionBar().setHomeButtonEnabled(!isRoot);
        }
        if (isRoot) {
            mDrawerToggle.syncState();
//            mBinding.toolbar.setVisibility(View.VISIBLE);
        } else {
            mBinding.toolbar.setVisibility(View.GONE);
        }
    }

    private void setDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mBinding.drawerLayoutMain, mBinding.toolbar,
                R.string.navigationDrawerOpen, R.string.navigationDrawerClose) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard(ActivityMain.this);
            }
        };

        updateDrawerToggle();
        mDrawerToggle.setToolbarNavigationClickListener(v -> {
            hideKeyboard(ActivityMain.this);
            onBackPressed();
        });
        mBinding.drawerLayoutMain.addDrawerListener(mDrawerToggle);
    }

    private void populateExpandableList() {
        mViewModel.prepareMenuData();
        expandableListAdapter = new AdapterExpandable(this, mViewModel.headerList,
                mViewModel.childList);
        mBinding.expandablelistDrawer.setAdapter(expandableListAdapter);
        mBinding.expandablelistDrawer.deferNotifyDataSetChanged();

        mBinding.expandablelistDrawer.setOnGroupClickListener((expandableListView, view,
                                                               groupPosition, l) -> {
            PojoMenu pojoMenu = mViewModel.headerList.get(groupPosition);
            if (!pojoMenu.isHasChildren()) {
                closeDrawer();
                removeBackStack();
                if (isSame(pojoMenu.getName(), R.string.drawer_dashboard)) {
                    navigateToDashboard();
                } else if (isSame(pojoMenu.getName(), R.string.text_schemes)) {
                    FragmentSchemes fragmentSchemes = new FragmentSchemes();
                    addFragment(fragmentSchemes, getString(R.string.app_name), getString(R.string.tagFragmentSchemes));
                }
            }
            return false;
        });

        mBinding.expandablelistDrawer.setOnChildClickListener((expandableListView, view,
                                                               groupPosition, childPosition, l) -> {
            PojoMenu pojoMenu = mViewModel.headerList.get(groupPosition);
            if (pojoMenu.isHasChildren()) {
                closeDrawer();
                removeBackStack();
                List<PojoMenu> pojoMenuList = mViewModel.childList.get(pojoMenu);
                if (pojoMenuList != null && !pojoMenuList.isEmpty()) {
                    PojoMenu pojoChildMenu = pojoMenuList.get(childPosition);
                    /*if (isSame(pojoMenu.getName(), R.string.drawer_demo)) {
                        if (isSame(pojoChildMenu.getName(), R.string.drawer_demo_data1)) {

                        } else if (isSame(pojoChildMenu.getName(), R.string.drawer_demo_data2)) {

                        }
                    }*/
                }
            }
            return false;
        });

        mBinding.expandablelistDrawer.setOnGroupExpandListener(groupPosition -> {
            for (int i = 0; i < expandableListAdapter.getGroupCount(); i++) {
                if (i != groupPosition) {
                    mBinding.expandablelistDrawer.collapseGroup(i);
                }
            }
        });
    }

    /**
     * Adds fragment for the application with integer values
     *
     * @param fragment Fragment to be added
     * @param title    Title to be shown
     * @param tag      Tag for the fragment
     */
    public void addFragment(Fragment fragment, int title, int tag) {
        addFragment(fragment, getString(title), getString(tag));
    }

    /**
     * Adds fragment for the application with integer values
     *
     * @param fragment Fragment to be added
     * @param title    Title to be shown
     * @param tag      Tag for the fragment
     */
    public void addFragment(Fragment fragment, String title, int tag) {
        addFragment(fragment, title, getString(tag));
    }

    /**
     * Adds fragment for the application with string values
     *
     * @param fragment Fragment to be added
     * @param title    Title to be shown
     * @param tag      Tag for the fragment
     */
    public void addFragment(Fragment fragment, String title, String tag) {
        String currentFragmentTag = getCurrentFragment().getTag();
        if (!currentFragmentTag.equals(tag)) {
            getSupportFragmentManager().beginTransaction()
                    /*.setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    )*/
                    .add(R.id.frame_container, fragment, tag)
                    .addToBackStack(tag).commit();
            if (!TextUtils.isEmpty(title)) {
                setAppTitle(title);
            } else {
                setAppTitle("");
            }
        }
        setDrawerEnabled(false);
    }

    private void placeFragment(Fragment fragment, String tag) {
        setDrawerEnabled(true);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_container, fragment, tag).commit();
    }

    /**
     * Sets app title with string as input
     *
     * @param title String content for title
     */
    public void setAppTitle(String title) {
        mBinding.toolbar.setTitle(title);
    }

    /**
     * Sets app title with int as input
     *
     * @param title Int content for title
     */
    public void setAppTitle(@StringRes int title) {
//        mBinding.toolbar.setTitle(getString(title));
        mBinding.toolbar.setTitle(title);
    }


    /**
     * Get Fragment By tag
     *
     * @param tag fragment tag
     * @return fragment
     */
    private Fragment getFragmentByTag(@StringRes int tag) {
        return getSupportFragmentManager().findFragmentByTag(getString(tag));
    }

    private Fragment getFragmentByTag(String className) {
        return getSupportFragmentManager().findFragmentByTag(className);
    }

    /**
     * Provides current fragment in use in application
     *
     * @return Fragment in use
     */
    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.frame_container);
    }

    public void setDrawerProfileDetail() {
        mViewModel.observableImageURL.set(mPreferences.getString(prefUserImage));
        mViewModel.observableEmail.set(mPreferences.getString(prefUserEmail));
        mViewModel.observableMobile.set(mPreferences.getString(prefUserMobile));
    }

    /**
     * Navigate to item
     */

    public void navigateToDashboard() {
        removeBackStack();
        setAppTitle(R.string.app_name_dashboard);
        FragmentDashboard fragmentMLDashboard = new FragmentDashboard();
        placeFragment(fragmentMLDashboard, FragmentDashboard.class.getCanonicalName());
    }

    public void navigateToSchemes() {
        FragmentSchemes fragmentSchemes = new FragmentSchemes();
        addFragment(fragmentSchemes, getString(R.string.text_schemes), FragmentSchemes.class.getCanonicalName());
    }

    public void navigateToWebView(String title, String url) {
        FragmentWebView fragmentWebView = new FragmentWebView();
        fragmentWebView.setWebContent(url);
        addFragment(fragmentWebView, title, FragmentWebView.class.getCanonicalName());
    }

    /**
     * Listener for toolbar's back button
     */
    private final FragmentManager.OnBackStackChangedListener mBackStackChangedListener =
            this::updateDrawerToggle;

    /**
     * Removes back stacks if any from the application
     */
    public void removeBackStack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
            fragmentManager.popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        if (mBinding.drawerLayoutMain.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                fragmentBackPressed();
            } else {
                if (mDoubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                this.mDoubleBackToExitPressedOnce = true;
                Toast.makeText(this, getString(R.string.message_exit_app), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(() -> mDoubleBackToExitPressedOnce = false, 2000);
            }
        }
    }

    /**
     * Fragment back stack on device bck pressed button
     */
    private void fragmentBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment.getActivity() != null) {
            View view = fragment.getActivity().getCurrentFocus();
            if (view != null) {
                hideKeyboard(this);
            }
        }
        boolean isGoBack = true;
        /*if (fragment instanceof FragmentMoneyLenderReg) {
            isGoBack = ((FragmentMoneyLenderReg) fragment).isGoPrevious();
        } else if (fragment instanceof FragmentSocietyReg) {
            isGoBack = ((FragmentSocietyReg) fragment).isGoPrevious();
        } else if (fragment instanceof FragmentOldMLReg) {
            isGoBack = ((FragmentOldMLReg) fragment).isGoPrevious();
        }*/
        goToPrevious(isGoBack);
    }

    public void goToPrevious(boolean isGoBack) {
        if (isGoBack) {
            super.onBackPressed();
            setPostBackStackHeader();
        }
    }

    /**
     * Sets back stack header after removal of the added fragment
     */
    private void setPostBackStackHeader() {
        mViewModel.isShowToolbar.set(false);
        if (getSupportFragmentManager().getBackStackEntryCount() < 1) {
            setDrawerEnabled(true);
            setAppTitle(R.string.app_name_dashboard);
            mBinding.expandablelistDrawer.setSelection(0);
            Fragment fragment = getCurrentFragment();
            if (fragment instanceof FragmentDashboard) {
                setAppTitle(R.string.app_name_dashboard);
//                ((FragmentDashboard) fragment).reload();
                ((FragmentDashboard) fragment).checkLoginSuccess();
            }
        } else {
            setBackStackAppTitle();
        }
    }

    private void setBackStackAppTitle() {
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof FragmentDashboard) {
            setAppTitle(R.string.app_name_dashboard);
        }
    }

    private void showLogoutAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.message_logout));
        alertDialogBuilder.setPositiveButton(getString(R.string.text_yes), (arg0, arg1) -> {
            Toast.makeText(ActivityMain.this, R.string.message_logout_success, Toast.LENGTH_LONG).show();
            logoutFromApp();
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.text_no),
                (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void setUpUserDetails() {
        mViewModel.observableEmail.set(mPreferences.getString(prefLoginUserId));
    }


    private final GeneralListener generalListener = (view) -> {
        closeDrawer();
        if (view.getId() == R.id.layout_drawer_header) {
//            navigateToProfile();
        }
    };

    private final View.OnClickListener onClickListener = view -> {
        if (view.getId() == R.id.textview_drawer_logout) {
            if (mPreferences.getBoolean(prefIsUserLogIn)) {
                showLogoutAlert();
            } else {
                /*startActivity(new Intent(ActivityMain.this, ActivityLogin.class));
                ActivityMain.this.finish();*/
            }
        }
    };

    /**
     * Close drawer
     */
    public void closeDrawer() {
        mBinding.drawerLayoutMain.closeDrawers();
    }

    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        mBinding.drawerLayoutMain.setDrawerLockMode(lockMode);
        mDrawerToggle.setDrawerIndicatorEnabled(enabled);
    }

    private boolean isSame(int strOne, int strTwo) {
        return TextUtils.equals(getString(strOne), getString(strTwo));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        /*else if (item.getItemId() == R.id.action_notification) {
            FragmentNotifications fragment = new FragmentNotifications();
            addFragment(fragment, R.string.text_notification, R.string.tagFragmentNotifications);
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notification, menu);
        MenuItem item_notification = menu.findItem(R.id.action_notification);
        if (item_notification != null) {
            item_notification.setVisible(false);
        }*/
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportFragmentManager().addOnBackStackChangedListener(mBackStackChangedListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getSupportFragmentManager().removeOnBackStackChangedListener(mBackStackChangedListener);
    }
}