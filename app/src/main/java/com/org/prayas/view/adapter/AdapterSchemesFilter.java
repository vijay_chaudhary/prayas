package com.org.prayas.view.adapter;

import static com.org.prayas.utils.ConstantCodes.PAGE_MIN_DEPT;
import static com.org.prayas.utils.ConstantCodes.PAGE_SECTORS;
import static com.org.prayas.utils.Utils.hideKeyboard;

import android.app.Activity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableArrayList;
import androidx.recyclerview.widget.RecyclerView;

import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.GeneralItemListener;
import com.org.prayas.business_logic.interfaces.GeneralListener;
import com.org.prayas.business_logic.pojo.PojoMinDeptData;
import com.org.prayas.business_logic.pojo.PojoSchemesFilterData;
import com.org.prayas.business_logic.pojo.PojoSectorsData;
import com.org.prayas.databinding.LayoutFilterMinDeptBinding;
import com.org.prayas.databinding.LayoutFilterSectorsBinding;

import java.util.ArrayList;
import java.util.List;

public class AdapterSchemesFilter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<PojoSchemesFilterData> pojoSchemesFilterData;
    private final GeneralItemListener generalItemListener;
    private final int VIEW_TYPE_SECTORS = 0, VIEW_TYPE_MIN_DEPT = 1;

    public AdapterSchemesFilter(List<PojoSchemesFilterData> pojoSchemesFilterData, GeneralItemListener generalItemListener) {
        this.pojoSchemesFilterData = pojoSchemesFilterData;
        this.generalItemListener = generalItemListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        /*RequestOptions requestOptionBanner = getRoundCornerOptionsSimple(parent.getContext().getApplicationContext(),
                12, R.drawable.place_hold_banner_circle);*/

        if (viewType == VIEW_TYPE_SECTORS) {
            LayoutFilterSectorsBinding binding = DataBindingUtil.inflate(LayoutInflater
                            .from(parent.getContext()), R.layout.layout_filter_sectors,
                    parent, false);
            binding.setGeneralItemListener(generalItemListener);
            return new ViewHolderSectors(binding);
        } else if (viewType == VIEW_TYPE_MIN_DEPT) {
            LayoutFilterMinDeptBinding binding = DataBindingUtil.inflate(LayoutInflater
                            .from(parent.getContext()), R.layout.layout_filter_min_dept,
                    parent, false);
            binding.setGeneralItemListener(generalItemListener);
            return new ViewHolderMinDept(binding);
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_progress_bottom, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderSectors) {
            ((ViewHolderSectors) holder).bind(pojoSchemesFilterData.get(position), position);
        } else if (holder instanceof ViewHolderMinDept) {
            ((ViewHolderMinDept) holder).bind(pojoSchemesFilterData.get(position), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        int itemViewType = 0;
        if (pojoSchemesFilterData != null && pojoSchemesFilterData.size() > 0) {
            itemViewType = getItemType(pojoSchemesFilterData.get(position).getPage_key());
        }
        return itemViewType;
    }

    private int getItemType(String page) {
        if (!TextUtils.isEmpty(page)) {
            if (page.equalsIgnoreCase(PAGE_SECTORS)) {
                return VIEW_TYPE_SECTORS;
            } else if (page.equalsIgnoreCase(PAGE_MIN_DEPT)) {
                return VIEW_TYPE_MIN_DEPT;
            }
        }
        return 0;
    }

    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressViewHolder(View v) {
            super(v);
        }
    }


    @Override
    public int getItemCount() {
        return pojoSchemesFilterData.size();
    }

    private final GeneralListener generalListener = view -> {
        if (view.getId() == R.id.txt_hide_sectors) {

        }
    };

    /**
     * View Holders
     */

    public static class ViewHolderSectors extends RecyclerView.ViewHolder {
        private final LayoutFilterSectorsBinding binding;

        ViewHolderSectors(LayoutFilterSectorsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(PojoSchemesFilterData pojo, int position) {
            binding.setPojo(pojo);
            binding.setCurrentPosition(position);
            binding.executePendingBindings();

            binding.txtHideSectors.setOnClickListener(v -> pojo.setSectorsVisible(!pojo.isSectorsVisible()));
        }
    }

    public static class ViewHolderMinDept extends RecyclerView.ViewHolder {
        private final LayoutFilterMinDeptBinding binding;
        List<PojoMinDeptData> pojoMinDeptDataList = new ArrayList<>();
        List<PojoMinDeptData> pojoMinDeptDataSearchList;

        ViewHolderMinDept(LayoutFilterMinDeptBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(PojoSchemesFilterData pojo, int position) {
            pojoMinDeptDataList.addAll(pojo.getPojoMinDeptDataList());
            pojoMinDeptDataSearchList = pojo.getPojoMinDeptDataList();
            binding.setPojo(pojo);
            binding.setCurrentPosition(position);
            binding.executePendingBindings();

            binding.txtHideMinDept.setOnClickListener(v -> pojo.setMinDeptVisible(!pojo.isMinDeptVisible()));

            binding.edtMinDeptSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    searchMember(s.toString(), binding.recyclerMinDept);
                }
            });
        }

        private void searchMember(String searchText, RecyclerView recyclerMinDept) {
            pojoMinDeptDataSearchList.clear();
            if (!TextUtils.isEmpty(searchText)) {
                for (PojoMinDeptData pojo : pojoMinDeptDataList) {
                    addSearchMember(pojo, searchText);
                }
            } else {
//                hideKeyboard((Activity) recyclerMinDept.getContext());
                pojoMinDeptDataSearchList.addAll(pojoMinDeptDataList);
            }

            if (recyclerMinDept.getAdapter() != null) {
                recyclerMinDept.getAdapter().notifyDataSetChanged();
            }
        }

        private void addSearchMember(PojoMinDeptData pojo, String searchItem) {
            String search = searchItem.toLowerCase();
            if (!TextUtils.isEmpty(pojo.getTitle()) && pojo.getTitle().toLowerCase().contains(search)) {
                pojoMinDeptDataSearchList.add(pojo);
            }
        }
    }
}
