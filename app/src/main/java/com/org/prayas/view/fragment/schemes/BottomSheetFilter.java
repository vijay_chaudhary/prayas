package com.org.prayas.view.fragment.schemes;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Insets;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.view.WindowMetrics;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableArrayList;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.GeneralItemListener;
import com.org.prayas.business_logic.interfaces.GeneralListener;
import com.org.prayas.business_logic.pojo.PojoSchemesFilterData;
import com.org.prayas.business_logic.viewmodel.schemes.ViewModelSchemes;
import com.org.prayas.business_logic.viewmodel.schemes.ViewModelSchemesFilter;
import com.org.prayas.databinding.BottomSheetSchemesFilterBinding;
import com.org.prayas.view.activity.main.ActivityMain;
import com.org.prayas.view.dialog.BottomSheetBase;

import java.util.ArrayList;
import java.util.List;

public class BottomSheetFilter extends BottomSheetBase {

    private BottomSheetSchemesFilterBinding mBinding;
    private ViewModelSchemesFilter viewModel;
    private ViewModelSchemes mViewModelSchemes;
    private PojoSchemesFilterData pojoSchemesFilterData = null;
    private BottomSheetDialog bottomSheetDialog;


    private List<PojoSchemesFilterData> pojoSchemesFilterDataList = new ArrayList<>();

    public static BottomSheetFilter newInstance(ViewModelSchemesFilter mViewModel,
                                                ViewModelSchemes mViewModelSchemes,
                                                ObservableArrayList<PojoSchemesFilterData> observableListFilter) {
        BottomSheetFilter bottomSheetBusArea = new BottomSheetFilter();
        bottomSheetBusArea.setViewModel(mViewModel);
        bottomSheetBusArea.setViewModelSchemes(mViewModelSchemes);
        bottomSheetBusArea.setPojoEmpFilterData(observableListFilter);
        return bottomSheetBusArea;
    }

    public void setPojoEmpFilterData(List<PojoSchemesFilterData> pojoSchemesFilterData) {
        this.pojoSchemesFilterDataList.clear();
        this.pojoSchemesFilterDataList = pojoSchemesFilterData;
    }

    public void setViewModelSchemes(ViewModelSchemes mViewModelSchemes) {
        this.mViewModelSchemes = mViewModelSchemes;
    }

    public void setViewModel(ViewModelSchemesFilter viewModel) {
        this.viewModel = viewModel;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        bottomSheetDialog.setOnShowListener(dialog -> {
            BottomSheetDialog d = (BottomSheetDialog) dialog;
            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            if (bottomSheet != null) {
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        return bottomSheetDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        int height = 0;
        int statusBarHeight = 0;
        if (getActivity() != null) {
            height = getScreenWidth(getActivity());
            statusBarHeight = getStatusBarHeight(getActivity());
        }
        mBinding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_schemes_filter, container,
                false);
        mBinding.setViewModel(viewModel);
        mBinding.setViewModelSchemes(mViewModelSchemes);
        mBinding.setGeneralItemListener(generalItemListener);
        mBinding.setGeneralListener(generalListener);

        if (height > 0) {
            mBinding.constraintFilter.setMinHeight((statusBarHeight > 0 ?
                    height - statusBarHeight : height));
        }
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents();
        observeEvents();
    }

    private void initComponents() {
        viewModel.observableListFilter.clear();
        viewModel.observableListFilter.addAll(pojoSchemesFilterDataList);
    }

    private void observeEvents() {

    }

    public void dismissDialog() {
        dismiss();
    }

    private final GeneralListener generalListener = view -> {
        if (view.getId() == R.id.txt_clear_all) {
            mViewModelSchemes.clearAll();
        } else if (view.getId() == R.id.img_filter_close) {
            viewModel.callBusAreaBottomDialog.setValue(null);
        } else if (view.getId() == R.id.btn_apply_filter) {
            mViewModelSchemes.setSelectFilter();
            viewModel.callBusAreaBottomDialog.setValue("filter");
        }
    };

    private final GeneralItemListener generalItemListener = (view, position, item) -> {
        if (item instanceof PojoSchemesFilterData) {
            if (view.getId() == R.id.txt_hide_sectors) {
                viewModel.observableBooleanIsSectorShow.set(!viewModel.observableBooleanIsSectorShow.get());
            }
        }
    };

    public static int getScreenWidth(@NonNull Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            WindowMetrics windowMetrics = activity.getWindowManager().getCurrentWindowMetrics();
            Insets insets = windowMetrics.getWindowInsets()
                    .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars());
            return windowMetrics.getBounds().height() - insets.top - insets.bottom;
        } else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            return displayMetrics.heightPixels;
        }
    }

    public int getStatusBarHeight(@NonNull Activity activity) {
        return ((ActivityMain) activity).getToolbarHeight();
    }
}
