package com.org.prayas.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.IntegerRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;

import com.org.prayas.R;
import com.org.prayas.business_logic.preferences.UtilsSharedPreferences;
import com.org.prayas.business_logic.viewmodel.ViewModelBase;
import com.org.prayas.view.activity.BaseActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;

public abstract class BaseFragment extends Fragment {

    @Inject
    protected Context mContext;

    @Inject
    protected UtilsSharedPreferences mPreferences;

    protected BaseActivity mActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BaseActivity) getActivity();
        setHasOptionsMenu(false);
    }

    protected Lifecycle getViewLifeCycle() {
        return getViewLifecycleOwner().getLifecycle();
    }

    protected int dimen(@DimenRes int resId) {
        return (int) getResources().getDimension(resId);
    }

    protected int color(@ColorRes int resId) {
        return getResources().getColor(resId);
    }

    protected int integer(@IntegerRes int resId) {
        return getResources().getInteger(resId);
    }

    protected String string(@StringRes int resId) {
        return getResources().getString(resId);
    }

    protected void hideFABOnScroll(RecyclerView mRecyclerView, FloatingActionButton mFloatingActionButton) {
        if (mRecyclerView != null && mFloatingActionButton != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0 && mFloatingActionButton.getVisibility() == View.VISIBLE) {
                        mFloatingActionButton.hide();
                    } else if (dy < 0 && mFloatingActionButton.getVisibility() != View.VISIBLE) {
                        mFloatingActionButton.show();
                    }
                }
            });
        }
    }

    /**
     * Use to Grant permission
     */
    public void requestWriteExternalStoragePermission(String permission, int requestCode,
                                                      String message) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (shouldShowRequestPermissionRationale(permission)) {
                showConfirmationDialog(permission, requestCode, message);
            } else {
                requestPermissions(new String[]{permission}, requestCode);
            }
        }
    }

    /**
     * Show message Dialog for grant permission
     */
    public void showConfirmationDialog(final String permission, final int requestCode,
                                       String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.DialogTheme);
        builder.setTitle(mContext.getResources().getString(R.string.text_alert));
        builder.setMessage(message);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setCancelable(false);
        builder.setPositiveButton(getResources().getString(R.string.text_ok),
                (dialog, which) -> {
                    dialog.dismiss();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{permission}, requestCode);
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.text_cancel),
                (dialog, which) -> dialog.dismiss());
        AlertDialog networkDialog = builder.create();
        networkDialog.show();
    }

    /**
     * Show message Dialog for save as draft
     */
    public void showSaveAsDraftDialog(String message, DialogInterface
            .OnClickListener
            okListener) {
        AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(mActivity, R.style.DialogTheme);
        mAlertDialog.setMessage(message)
                .setPositiveButton(getResources().getString(R.string.text_yes), okListener)
                .setNegativeButton(getResources().getString(R.string.text_no), null)
                .create()
                .show();
    }

    public void showSaveAsDraftDialog(String message, boolean isPositiveOnly, DialogInterface
            .OnClickListener
            okListener) {
        AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(mActivity, R.style.DialogTheme);
        mAlertDialog.setMessage(message);
        if (isPositiveOnly) {
            mAlertDialog.setPositiveButton(getResources().getString(R.string.text_ok), okListener);
        } else {
            mAlertDialog.setPositiveButton(getResources().getString(R.string.text_yes), okListener);
            mAlertDialog.setNegativeButton(getResources().getString(R.string.text_no), null);
        }
        mAlertDialog.create();
        mAlertDialog.show();
    }

    public void setEventListener(ViewModelBase viewModelBase) {
        /*viewModelBase.getLiveDataTokenExpire().observe(mActivity, msg -> {
            if (isAdded()) {
                ((ActivityMain) mActivity).showSessionExpiredDialog(msg, (dialog, which) ->
                        ((ActivityMain) mActivity).logoutFromApp());
            }
        });*/
    }
}