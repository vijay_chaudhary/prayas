package com.org.prayas.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.request.RequestOptions;
import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.CardAdapter;
import com.org.prayas.business_logic.interfaces.GeneralItemListener;
import com.org.prayas.business_logic.pojo.PojoBannerData;
import com.org.prayas.databinding.LayoutItemBannerBinding;

import java.util.List;

/**
 * Created by Naria Sachin on Mar, 01 2021 19:25.
 */

public class AdapterPagerHomeBanner extends PagerAdapter implements CardAdapter {

    private final List<PojoBannerData> mBannerList;
    private final GeneralItemListener mListenerBannerPager;
    private float mBaseElevation = 0;
    private final RequestOptions optionsImage;

    public AdapterPagerHomeBanner(List<PojoBannerData> bannerList,
                                  GeneralItemListener listenerBannerPager,
                                  RequestOptions optionsImage,
                                  float baseElevation) {
        mBannerList = bannerList;
        mListenerBannerPager = listenerBannerPager;
        this.optionsImage = optionsImage;
        this.mBaseElevation = baseElevation;
    }

    @Override
    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return (mBannerList != null && !mBannerList.isEmpty()) ? mBannerList.get(position).getCardView() : null;
    }

    @Override
    public void destroyItem(@NonNull final ViewGroup container, final int position, @NonNull final Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull final View view, @NonNull final Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return (mBannerList != null ? mBannerList.size() : 0);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull final ViewGroup container, final int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(container.getContext());
        LayoutItemBannerBinding mBinding = DataBindingUtil.inflate(layoutInflater,
                R.layout.layout_item_banner, container,
                false);

        mBinding.setPojo(mBannerList.get(position));
        mBinding.setCurrentPosition(position);
        mBinding.setGeneralItemListener(mListenerBannerPager);
        mBinding.setRequestOptions(optionsImage);
        container.addView(mBinding.getRoot());
        mBinding.executePendingBindings();
        return mBinding.getRoot();
    }
}
