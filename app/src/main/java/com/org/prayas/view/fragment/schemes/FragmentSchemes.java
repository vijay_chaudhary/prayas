package com.org.prayas.view.fragment.schemes;

import static com.org.prayas.utils.Utils.getSplitList;
import static com.org.prayas.utils.Utils.getTrimmedData;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.GeneralItemListener;
import com.org.prayas.business_logic.interfaces.GeneralListener;
import com.org.prayas.business_logic.pojo.PojoPlace;
import com.org.prayas.business_logic.pojo.PojoSchemes;
import com.org.prayas.business_logic.viewmodel.schemes.ViewModelSchemes;
import com.org.prayas.business_logic.viewmodel.schemes.ViewModelSchemesFilter;
import com.org.prayas.databinding.FragmentSchemesBinding;
import com.org.prayas.view.activity.main.ActivityMain;
import com.org.prayas.view.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class FragmentSchemes extends BaseFragment {
    private FragmentSchemesBinding mBinding;
    private ViewModelSchemes mViewModel;
    private ViewModelSchemesFilter mViewModelSchemesFilter;

    private BottomSheetFilter bottomSheetFilter;
    private boolean isFilterOpen;

    private final SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::onRefresh;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_schemes, container, false);
        mViewModel = new ViewModelProvider(this).get(ViewModelSchemes.class);
        mViewModelSchemesFilter = new ViewModelProvider(this).get(ViewModelSchemesFilter.class);
        mBinding.setViewModel(mViewModel);
        mBinding.setGeneralListener(generalListener);
        mBinding.setItemListener(generalItemListener);
        mBinding.setOnRefreshListener(onRefreshListener);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initComponents();
        observerEvents();

        mViewModel.callDashboardAPI();
        mViewModel.callStateAPI();
        mViewModel.callFilterAPI();
    }

    private void initComponents() {
        //mViewModel.apiFilter();
    }

    private void observerEvents() {
        mViewModelSchemesFilter.getCallBusAreaBottomDialog().observe(getViewLifecycleOwner(), msg -> {
            isFilterOpen = false;
            bottomSheetFilter.dismissDialog();
            if (!TextUtils.isEmpty(msg) && msg.equalsIgnoreCase("filter")) {
                mViewModel.sectorIdList.clear();
                if (!mViewModel.sectorsIDs.isEmpty()) {
                    mViewModel.sectorIdList.addAll(mViewModel.sectorsIDs);
                }

                mViewModel.miniIdList.clear();
                if (!mViewModel.minDeptIDs.isEmpty()) {
                    mViewModel.miniIdList.addAll(mViewModel.minDeptIDs);
                }
                //manageFilterData();
                mViewModel.callDashboardAPI();
            }
        });

        mBinding.spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*if (position != 0) {
                    mBinding.spinnerDistrict.setSelection(0);
                    if (mViewModel.isInternetConnected(R.string.message_noconnection)) {
                        //Api Call  viewModel.observableState.get(mBinding.spinnerState.getSelectedItemPosition()).getId()
                    }
                }*/
                mViewModel.districtId.set("");
                mBinding.spinnerDistrict.setSelection(0);
                if (position == 0) {
                    mViewModel.stateId.set("");
                } else {
                    mViewModel.stateId.set(mViewModel.observableState.get(position).getId());
                    mViewModel.callDistrictAPI();
                }
                if (mViewModel.statePosition.get() != position) {
                    mViewModel.statePosition.set(position);
                    //manageFilterData();
                    mViewModel.callDashboardAPI();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mBinding.spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*if (position != 0) {
                    mBinding.spinnerDistrict.setSelection(0);
                    if (mViewModel.isInternetConnected(R.string.message_noconnection)) {
                        //Api Call  viewModel.observableState.get(mBinding.spinnerState.getSelectedItemPosition()).getId()
                    }
                }*/

                if (position == 0) {
                    mViewModel.districtId.set("");
                } else {
                    mViewModel.districtId.set(mViewModel.observableDistrict.get(position).getId());
                }
                if (mViewModel.districtPosition.get() != position) {
                    mViewModel.districtPosition.set(position);
                    //manageFilterData();
                    mViewModel.callDashboardAPI();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    public void onRefresh() {
        mViewModel.onRefreshList(false);
    }

    private void districtData(ArrayList<PojoPlace> district) {
        mViewModel.observableDistrict.clear();
        mViewModel.observableDistrict.add(new PojoPlace(getString(R.string.text_select_district)));
        mViewModel.observableDistrict.addAll(district);
        notifyDistrictData();
        String districtId = "";
        /*if (!TextUtils.isEmpty(pojoLoginDetails.getTaluka())) {
            districtId = pojoLoginDetails.getTaluka();
        }*/
        if (!TextUtils.isEmpty(districtId) && mViewModel.observableDistrict.size() > 0) {
            for (int i = 0; i < mViewModel.observableDistrict.size(); i++) {
                if (TextUtils.equals(mViewModel.observableDistrict.get(i).getId(), districtId)) {
                    mBinding.spinnerDistrict.setSelection(i);
                    break;
                }
            }
        }
    }

    private void stateData(ArrayList<PojoPlace> state) {
        mViewModel.observableState.clear();
        mViewModel.observableState.add(new PojoPlace(getString(R.string.text_select_state)));
        mViewModel.observableState.addAll(state);
        notifyStateData();
        String stateId = "";

        /*if (!TextUtils.isEmpty(pojoLoginDetails.getDistrict())) {
            stateId = pojoLoginDetails.getDistrict();
        }*/
        if (!TextUtils.isEmpty(stateId) && mViewModel.observableState.size() > 0) {
            for (int i = 0; i < mViewModel.observableState.size(); i++) {
                if (TextUtils.equals(mViewModel.observableState.get(i).getId(), stateId)) {
                    mBinding.spinnerState.setSelection(i);
                    break;
                }
            }
        }
    }

    private void manageFilterData() {
        mViewModel.schemesListFiltered.clear();
        boolean isShowAll = true;

        if (!TextUtils.isEmpty(mViewModel.stateId.getTrimmed()) || !TextUtils.isEmpty(mViewModel.districtId.getTrimmed()) ||
                !mViewModel.sectorIdList.isEmpty() || !mViewModel.miniIdList.isEmpty() ||
                !TextUtils.isEmpty(mViewModel.searchText.getTrimmed())) {
            isShowAll = false;
        }

        if (isShowAll) {
            mViewModel.schemesListFiltered.addAll(mViewModel.schemesList);
        } else {
            for (int i = 0; i < mViewModel.schemesList.size(); i++) {
                PojoSchemes pojoSchemes = mViewModel.schemesList.get(i);
                boolean isNeedToAdd = false;
                boolean isDataFail = false;
                if (!TextUtils.isEmpty(mViewModel.stateId.getTrimmed()) && !TextUtils.isEmpty(mViewModel.districtId.getTrimmed())) {
                    List<String> stateList = getSplitList(pojoSchemes.getState());
                    List<String> districtList = getSplitList(pojoSchemes.getDistrict());
                    if (isSameID(stateList, mViewModel.stateId.getTrimmed())
                            && isSameID(districtList, mViewModel.districtId.getTrimmed())) {
                        isNeedToAdd = true;
                    } else {
                        isDataFail = true;
                    }
                } else if (!TextUtils.isEmpty(mViewModel.stateId.getTrimmed())) {
                    List<String> list = getSplitList(pojoSchemes.getState());
                    if (isSameID(list, mViewModel.stateId.getTrimmed())) {
                        isNeedToAdd = true;
                    } else {
                        isDataFail = true;
                    }
                }

                if (!isDataFail && !mViewModel.sectorIdList.isEmpty()) {
                    if (isSameID(mViewModel.sectorIdList, pojoSchemes.getSectorId())) {
                        isNeedToAdd = true;
                    } else {
                        isNeedToAdd = false;
                        isDataFail = true;
                    }
                }

                if (!isDataFail && !mViewModel.miniIdList.isEmpty()) {
                    if (isSameID(mViewModel.miniIdList, pojoSchemes.getMinistryId())) {
                        isNeedToAdd = true;
                    } else {
                        isNeedToAdd = false;
                        isDataFail = true;
                    }
                }

                if (!isDataFail && !TextUtils.isEmpty(mViewModel.searchText.getTrimmed())) {
                    if (TextUtils.equals(mViewModel.searchText.getTrimmed(), pojoSchemes.getTitle())) {
                        isNeedToAdd = true;
                    } else {
                        isNeedToAdd = false;
                        isDataFail = true;
                    }
                }

                if (isNeedToAdd) {
                    mViewModel.schemesListFiltered.add(pojoSchemes);
                }
            }
        }
        //notifySchemeData();
    }

    private boolean isSameID(List<String> list, String dataId) {
        boolean isSame = false;
        if (list != null && !list.isEmpty()) {
            for (int j = 0; j < list.size(); j++) {
                if (isBothSame(list.get(j), dataId)) {
                    isSame = true;
                    break;
                }
            }
        }
        return isSame;
    }

    private boolean isBothSame(String value1, String value2) {
        return (!TextUtils.isEmpty(getTrimmedData(value1)) && !TextUtils.isEmpty(getTrimmedData(value2))
                && value1.trim().equalsIgnoreCase(value2.trim()));
    }

    @SuppressLint("NotifyDataSetChanged")
    private void notifySchemeData() {
        if (mBinding.recyclerviewEmp.getAdapter() != null) {
            mBinding.recyclerviewEmp.getAdapter().notifyDataSetChanged();
        }
    }

    private void notifyDistrictData() {
        if (mBinding.spinnerDistrict.getAdapter() != null && mBinding.spinnerDistrict.getAdapter() instanceof ArrayAdapter) {
            ((ArrayAdapter<?>) mBinding.spinnerDistrict.getAdapter()).notifyDataSetChanged();
        }
    }

    private void notifyStateData() {
        if (mBinding.spinnerState.getAdapter() != null && mBinding.spinnerState.getAdapter() instanceof ArrayAdapter) {
            ((ArrayAdapter<?>) mBinding.spinnerState.getAdapter()).notifyDataSetChanged();
        }
    }

    private final GeneralListener generalListener = view -> {
        if (view.getId() == R.id.imageview_emp_filter) {
            if (!isFilterOpen) {
                isFilterOpen = true;
                mViewModel.resetBottomDialog();
                bottomSheetFilter = BottomSheetFilter.newInstance(mViewModelSchemesFilter, mViewModel,
                        mViewModel.observableListFilter);
                bottomSheetFilter.show(getChildFragmentManager(), BottomSheetFilter.class.getSimpleName());
            }
        } /*else if (view.getId() == R.id.imageview_dep_logo) {
            mBinding.drawerLayoutUser.openDrawer(GravityCompat.END);
        } else if (view.getId() == R.id.img_close_drawer) {
            mBinding.drawerLayoutUser.closeDrawer(GravityCompat.END);
        }*/
    };

    private final GeneralItemListener generalItemListener = new GeneralItemListener() {
        @Override
        public void onItemClick(View view, int position, Object item) {
            if (mViewModel.isInternetConnected(R.string.message_noconnection)) {
                if (item instanceof PojoSchemes) {
                    PojoSchemes pojoSchemes = ((PojoSchemes) item);
                    FragmentSchemesDetails fragmentSchemesDetails = new FragmentSchemesDetails();
                    fragmentSchemesDetails.setSchemesList(mViewModel.schemesListFiltered);
                    fragmentSchemesDetails.setPojoSchemes(pojoSchemes, position);
                    fragmentSchemesDetails.setPlaceData(mViewModel.observableState, mViewModel.observableDistrict,
                            mViewModel.stateId.getTrimmed(), mViewModel.districtId.getTrimmed());
                    ((ActivityMain) mActivity).addFragment(fragmentSchemesDetails, getString(R.string.app_name),
                            getString(R.string.tagFragmentSchemesDetails));
                }
            }
        }
    };


}
