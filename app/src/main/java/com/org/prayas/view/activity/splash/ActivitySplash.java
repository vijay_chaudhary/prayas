package com.org.prayas.view.activity.splash;

import static com.org.prayas.business_logic.preferences.PrefKeys.prefIsUserFirstTime;
import static com.org.prayas.business_logic.preferences.PrefKeys.prefIsUserLogIn;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.org.prayas.R;
import com.org.prayas.utils.Logger;
import com.org.prayas.view.activity.BaseActivity;
import com.org.prayas.view.activity.main.ActivityMain;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ActivitySplash extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_splash);
        beginNavigationThread();
    }

    /**
     * Initiate splash navigation
     */
    private void beginNavigationThread() {
        new Thread(() -> {
            try {
                Thread.sleep(3000);
            } catch (Exception e) {
                Logger.log(e.toString());
            }
            navigateFromSplash();
        }).start();
    }

    /**
     * Navigate from splash
     */
    private void navigateFromSplash() {
        if (mPreferences.getBoolean(prefIsUserFirstTime)) {
//            navigateToChangePWD(false);
        } else {
            startActivity(new Intent(this, ActivityMain.class));
            /*if (mPreferences.getBoolean(prefIsUserLogIn)) {
                startActivity(new Intent(this, ActivityMain.class));
            } else {
                startActivity(new Intent(this, ActivityLogin.class));
            }*/
        }

        overridePendingTransition(0, 0);
        finish();
    }
}