package com.org.prayas.view.fragment.schemes;

import static com.org.prayas.utils.CustomBindingAdapter.getRoundCornerOptionsSimple;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableList;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.request.RequestOptions;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.GeneralListener;
import com.org.prayas.business_logic.pojo.PojoGraph;
import com.org.prayas.business_logic.pojo.PojoPlace;
import com.org.prayas.business_logic.pojo.PojoSchemes;
import com.org.prayas.business_logic.viewmodel.schemes.ViewModelSchemesDetails;
import com.org.prayas.databinding.FragmentSchemesDetailsBinding;
import com.org.prayas.utils.XAxisValueFormatter;
import com.org.prayas.view.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class FragmentSchemesDetails extends BaseFragment {
    private FragmentSchemesDetailsBinding mBinding;
    private ViewModelSchemesDetails mViewModel;
    private int currentItemPosition;
    private PojoSchemes pojoSchemes;
    public ObservableList<PojoSchemes> schemesList;

    private List<PojoPlace> stateList;
    private List<PojoPlace> districtList;
    private String stateId;
    private String districtId;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_schemes_details, container, false);
        mViewModel = new ViewModelProvider(this).get(ViewModelSchemesDetails.class);
        mBinding.setViewModel(mViewModel);
        mBinding.setClickableSpan(spannablePrivacyData);
        mBinding.setGeneralListener(generalListener);
        RequestOptions requestOptions = getRoundCornerOptionsSimple(mContext,
                40, R.drawable.bg_round);
        mBinding.setRequestOptions(requestOptions);
        mViewModel.initStateId = stateId;
        mViewModel.initDistrictId = districtId;
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initComponents();
        observerEvents();
        setUpNextPreviousData(currentItemPosition);
        mViewModel.callStateAPI();
    }

    private void initComponents() {
        /*if (stateList != null && !stateList.isEmpty()) {
            stateData(stateList);
        }*/
    }

    private void observerEvents() {
        mViewModel.dataLoaded.observe(getViewLifecycleOwner(), new Observer<Void>() {
            @Override
            public void onChanged(Void unused) {
                if (!mViewModel.subscribersChartXValues.isEmpty()) {
                    setUpChartSubscribers();
                }
                if (!mViewModel.disbursedChartXValues.isEmpty()) {
                    setUpChartDisbursed();
                }
            }
        });

        mViewModel.setUpStateData.observe(getViewLifecycleOwner(), new Observer<Void>() {
            @Override
            public void onChanged(Void unused) {
                setUpStateData();
            }
        });

        mViewModel.setUpDistrictData.observe(getViewLifecycleOwner(), new Observer<Void>() {
            @Override
            public void onChanged(Void unused) {
                setUpDistrictData();
            }
        });

        mBinding.spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*if (position != 0) {
                    mBinding.spinnerDistrict.setSelection(0);
                    if (mViewModel.isInternetConnected(R.string.message_noconnection)) {
                        //Api Call  viewModel.observableState.get(mBinding.spinnerState.getSelectedItemPosition()).getId()
                    }
                }*/

                mViewModel.districtId.set("");
                mBinding.spinnerDistrict.setSelection(0);

                if (position == 0) {
                    mViewModel.stateId.set("");
                } else {
                    mViewModel.stateId.set(mViewModel.observableState.get(position).getId());
                    mViewModel.callDistrictAPI();
                }
                if (mViewModel.statePosition.get() != position) {
                    mViewModel.statePosition.set(position);
                    //manageFilterData();
                    if (TextUtils.isEmpty(mViewModel.initStateId)) {
                        mViewModel.callDetailsAPIs(pojoSchemes);
                    } else {
                        mViewModel.initStateId = null;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mBinding.spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*if (position != 0) {
                    mBinding.spinnerDistrict.setSelection(0);
                    if (mViewModel.isInternetConnected(R.string.message_noconnection)) {
                        //Api Call  viewModel.observableState.get(mBinding.spinnerState.getSelectedItemPosition()).getId()
                    }
                }*/

                if (position == 0) {
                    mViewModel.districtId.set("");
                } else {
                    mViewModel.districtId.set(mViewModel.observableDistrict.get(position).getId());
                }

                if (mViewModel.districtPosition.get() != position) {
                    mViewModel.districtPosition.set(position);
                    //manageFilterData();

                    if (TextUtils.isEmpty(mViewModel.initDistrictId)) {
                        mViewModel.callDetailsAPIs(pojoSchemes);
                    } else {
                        mViewModel.initDistrictId = null;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setUpStateData() {
        if (!TextUtils.isEmpty(stateId) && mViewModel.observableState.size() > 0) {
            for (int i = 0; i < mViewModel.observableState.size(); i++) {
                if (TextUtils.equals(mViewModel.observableState.get(i).getId(), stateId)) {
                    int finalI = i;
                    mBinding.spinnerState.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mBinding.spinnerState.setSelection(finalI);
                        }
                    }, 500);
                    break;
                }
            }
        }
    }

    private void setUpDistrictData() {
        if (!TextUtils.isEmpty(districtId) && mViewModel.observableDistrict.size() > 0) {
            for (int i = 0; i < mViewModel.observableDistrict.size(); i++) {
                if (TextUtils.equals(mViewModel.observableDistrict.get(i).getId(), districtId)) {
                    Log.e("setUpDistrictData", "= done");
                    int finalI = i;
                    mBinding.spinnerDistrict.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mBinding.spinnerDistrict.setSelection(finalI);
                        }
                    }, 500);
                    break;
                }
            }
        }
    }

    private void stateData(List<PojoPlace> state) {
        mViewModel.observableState.clear();
        mViewModel.observableState.add(new PojoPlace(getString(R.string.text_select_state)));
        mViewModel.observableState.addAll(state);
        /*notifyStateData();
        String stateId = "";

        if (!TextUtils.isEmpty(pojoLoginDetails.getDistrict())) {
            stateId = pojoLoginDetails.getDistrict();
        }*/
        if (!TextUtils.isEmpty(stateId) && mViewModel.observableState.size() > 0) {
            for (int i = 0; i < mViewModel.observableState.size(); i++) {
                if (TextUtils.equals(mViewModel.observableState.get(i).getId(), stateId)) {
                    mBinding.spinnerState.setSelection(i);
                    break;
                }
            }
        }
    }

    private void districtData(List<PojoPlace> district) {
        mViewModel.observableDistrict.clear();
        mViewModel.observableDistrict.add(new PojoPlace(getString(R.string.text_select_district)));
        mViewModel.observableDistrict.addAll(district);
        /*notifyDistrictData();
        String districtId = "";
        if (!TextUtils.isEmpty(pojoLoginDetails.getTaluka())) {
            districtId = pojoLoginDetails.getTaluka();
        }*/
        if (!TextUtils.isEmpty(districtId) && mViewModel.observableDistrict.size() > 0) {
            for (int i = 0; i < mViewModel.observableDistrict.size(); i++) {
                if (TextUtils.equals(mViewModel.observableDistrict.get(i).getId(), districtId)) {
                    mBinding.spinnerDistrict.setSelection(i);
                    break;
                }
            }
        }
    }

    private void setUpPojoData() {
        mBinding.chartSchemeSubscribersGraph.invalidate();
        mBinding.chartSchemeSubscribersGraph.clear();
        mBinding.chartSchemeDisbursedGraph.invalidate();
        mBinding.chartSchemeDisbursedGraph.clear();
        if (pojoSchemes != null) {
            mViewModel.setUpPojoGraphData(pojoSchemes);
            /*mViewModel.setUpGraphData(pojoSchemes);
            if (pojoSchemes.getSubscribeData() != null && !pojoSchemes.getSubscribeData().isEmpty()) {
                setUpChartSubscribers();
            }
            if (pojoSchemes.getSubscribeData() != null && !pojoSchemes.getSubscribeData().isEmpty()) {
                setUpChartDisbursed();
            }*/
        }
    }

    private void setUpChartSubscribers() {
        mBinding.chartSchemeSubscribersGraph.getDescription().setEnabled(false);
        mBinding.chartSchemeSubscribersGraph.setDrawBarShadow(false);
        mBinding.chartSchemeSubscribersGraph.setDrawValueAboveBar(true);
        mBinding.chartSchemeSubscribersGraph.setPinchZoom(false);
        mBinding.chartSchemeSubscribersGraph.setScaleEnabled(false);

        mViewModel.subscribersChartValues.clear();
        for (int i = 0; i < mViewModel.subscribersChartXValues.size(); i++) {
            PojoGraph pojoGraph = mViewModel.subscribersChartXValues.get(i);
            mViewModel.subscribersChartValues.add(new BarEntry(i, pojoGraph.getYAxisData()));
        }

        ValueFormatter xAxisFormatter = new XAxisValueFormatter(mViewModel.subscribersChartXValues);
        XAxis xAxis = mBinding.chartSchemeSubscribersGraph.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(mViewModel.subscribersChartXValues.size());
        //xAxis.setAvoidFirstLastClipping(true);
        xAxis.setValueFormatter(xAxisFormatter);

        mBinding.chartSchemeSubscribersGraph.getAxisLeft().setDrawLabels(false);
        mBinding.chartSchemeSubscribersGraph.getAxisRight().setDrawLabels(false);

        //mBinding.chartSchemeSubscribersGraph.animateY(1500);
        mBinding.chartSchemeSubscribersGraph.getLegend().setEnabled(false);

        BarDataSet set1 = new BarDataSet(mViewModel.subscribersChartValues, "Data Set");
        set1.setValueTextSize(12f);
        set1.setValueTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        set1.setDrawValues(true);
        set1.setGradientColor(Color.parseColor("#2950AF"), Color.parseColor("#ECC2DE"));

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setHighlightEnabled(false);
        data.setBarWidth(0.5f);
        mBinding.chartSchemeSubscribersGraph.setData(data);
        mBinding.chartSchemeSubscribersGraph.setFitBars(true);
        mBinding.chartSchemeSubscribersGraph.invalidate();
    }

    private void setUpChartDisbursed() {
        mBinding.chartSchemeDisbursedGraph.getDescription().setEnabled(false);
        mBinding.chartSchemeDisbursedGraph.setTouchEnabled(false);
        mBinding.chartSchemeDisbursedGraph.setDragEnabled(false);
        mBinding.chartSchemeDisbursedGraph.setScaleEnabled(false);
        mBinding.chartSchemeDisbursedGraph.setPinchZoom(false);

        mViewModel.disbursedChartValues.clear();
        for (int i = 0; i < mViewModel.disbursedChartXValues.size(); i++) {
            PojoGraph pojoGraph = mViewModel.disbursedChartXValues.get(i);
            mViewModel.disbursedChartValues.add(new BarEntry(i, pojoGraph.getYAxisData()));
        }

        ValueFormatter xAxisFormatter = new XAxisValueFormatter(mViewModel.disbursedChartXValues);
        XAxis xAxis = mBinding.chartSchemeDisbursedGraph.getXAxis();
        xAxis.setDrawLimitLinesBehindData(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(mViewModel.disbursedChartXValues.size(), true);
        xAxis.setAvoidFirstLastClipping(true);
        //xAxis.setAxisMinimum(0);
        //xAxis.setAxisMaximum(mViewModel.disbursedChartXValues.size());
        xAxis.setValueFormatter(xAxisFormatter);

        mBinding.chartSchemeDisbursedGraph.getAxisLeft().setDrawLabels(false);
        mBinding.chartSchemeDisbursedGraph.getAxisRight().setDrawLabels(false);

        //mBinding.chartSchemeDisbursedGraph.animateY(1500);
        mBinding.chartSchemeDisbursedGraph.getLegend().setEnabled(false);

        LineDataSet set1 = new LineDataSet(mViewModel.disbursedChartValues, "Data Set");
        set1.setDrawIcons(false);
        set1.setColors(R.color.purple_200);
        set1.setCircleColor(R.color.purple_200);
        set1.setCircleHoleColor(Color.WHITE);
        set1.setLineWidth(2f);
        set1.setCircleRadius(5f);
        set1.setCircleHoleRadius(3f);
        set1.setDrawCircleHole(true);
        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set1.setValueTextSize(12f);
        set1.setValueTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

        set1.setDrawFilled(true);
        set1.setFillFormatter(new IFillFormatter() {
            @Override
            public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                return mBinding.chartSchemeDisbursedGraph.getAxisLeft().getAxisMinimum();
            }
        });

        Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.bg_gradient_chart);
        set1.setFillDrawable(drawable);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        LineData data = new LineData(dataSets);
        data.setHighlightEnabled(false);
        mBinding.chartSchemeDisbursedGraph.setData(data);
        mBinding.chartSchemeDisbursedGraph.invalidate();
    }

    public void setPojoSchemes(PojoSchemes pojoSchemes, int position) {
        this.pojoSchemes = pojoSchemes;
        this.currentItemPosition = position;
    }

    public void setSchemesList(ObservableList<PojoSchemes> schemesList) {
        this.schemesList = schemesList;
    }

    public void setPlaceData(List<PojoPlace> stateList, List<PojoPlace> districtList,
                             String stateId, String districtId) {
        this.stateList = stateList;
        this.districtList = districtList;
        this.stateId = stateId;
        this.districtId = districtId;
    }

    private void setUpNextPreviousData(int nextPosition) {
        if (nextPosition + 1 == schemesList.size()) {
            mViewModel.isNext.set(false);
        } else {
            mViewModel.isNext.set(true);
            PojoSchemes pojoSchemes = schemesList.get(nextPosition + 1);
            mViewModel.schemeLogoUrlNext.set(pojoSchemes.getTitleLogoUrl());
        }

        if (nextPosition - 1 == -1) {
            mViewModel.isPrevious.set(false);
        } else {
            mViewModel.isPrevious.set(true);
            PojoSchemes pojoSchemes = schemesList.get(nextPosition - 1);
            mViewModel.schemeLogoUrlPrevious.set(pojoSchemes.getTitleLogoUrl());
        }

        PojoSchemes pojoSchemes = schemesList.get(nextPosition);
        setPojoSchemes(pojoSchemes, nextPosition);
        setUpPojoData();

        mBinding.scrollviewDetails.postDelayed(() ->
                mBinding.scrollviewDetails.smoothScrollTo(0, 0), 200);

    }

    private final GeneralListener generalListener = view -> {
        if (view.getId() == R.id.textview_scheme_back) {
            mActivity.onBackPressed();
        } else if (view.getId() == R.id.imageview_scheme_bottom_previous_logo ||
                view.getId() == R.id.imageview_scheme_bottom_previous) {
            if (mViewModel.isInternetConnected(R.string.message_noconnection) && !mViewModel.observerProgressBar.get()) {
                int nextPosition = currentItemPosition - 1;
                setUpNextPreviousData(nextPosition);
            }
        } else if (view.getId() == R.id.imageview_scheme_bottom_next_logo ||
                view.getId() == R.id.imageview_scheme_bottom_next) {
            if (mViewModel.isInternetConnected(R.string.message_noconnection) && !mViewModel.observerProgressBar.get()) {
                int nextPosition = currentItemPosition + 1;
                setUpNextPreviousData(nextPosition);
            }
        }
    };

    public ClickableSpan spannablePrivacyData = new ClickableSpan() {
        @Override
        public void onClick(@NonNull View widget) {
            if (mViewModel.isInternetConnected(R.string.message_noconnection)) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://prayas.nic.in/"));
                startActivity(browserIntent);
            }
        }

        @Override
        public void updateDrawState(final TextPaint textPaint) {
            textPaint.setColor(ContextCompat.getColor(mContext, R.color.colorAccent));
            textPaint.setUnderlineText(true);
        }
    };
}
