package com.org.prayas.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.GeneralItemListener;
import com.org.prayas.business_logic.pojo.PojoSchemes;
import com.org.prayas.databinding.LayoutSchemesRowBinding;

import java.util.ArrayList;
import java.util.List;

public class AdapterSchemes extends RecyclerView.Adapter {
    private final List<PojoSchemes> itemList;
    private final int VIEW_ITEM = 1;
    private final GeneralItemListener itemListener;
    private final RequestOptions requestOptions;

    public AdapterSchemes(List<PojoSchemes> itemList, GeneralItemListener itemListener,
                          RequestOptions requestOptions) {
        this.itemList = itemList;
        this.itemListener = itemListener;
        this.requestOptions = requestOptions;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == VIEW_ITEM) {
            LayoutSchemesRowBinding binding = DataBindingUtil.inflate(LayoutInflater
                            .from(parent.getContext()), R.layout.layout_schemes_row,
                    parent, false);
            binding.setItemListener(itemListener);
            binding.setRequestOptions(requestOptions);
            return new ViewHolderItemView(binding);
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_progress_bottom, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderItemView) {
            ((ViewHolderItemView) holder).bind(itemList.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) != null ? VIEW_ITEM : 0;
    }

    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressViewHolder(View v) {
            super(v);
        }
    }

    public static class ViewHolderItemView extends RecyclerView.ViewHolder {
        private final LayoutSchemesRowBinding binding;

        ViewHolderItemView(LayoutSchemesRowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(PojoSchemes pojo, int position) {
            binding.setPojo(pojo);
            binding.setCurrentPosition(position);
            binding.executePendingBindings();
        }
    }
}

/*

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.MyViewHolder>  implements Filterable {
    private ArrayList<ItemDataModel> dataSet;
    private ArrayList<ItemDataModel> FullList;

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tvName;
        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageview);
            tvName = itemView.findViewById(R.id.tvName);
        }
    }
    public RecycleAdapter(ArrayList<ItemDataModel> itemList) {
        this.dataSet = itemList;
        FullList = new ArrayList<>(itemList);
    }
    @NonNull
    @Override
    public RecycleAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_layout_for_recyclerview,
                parent, false);
        return new MyViewHolder(v);
    }
    @Override
    public void onBindViewHolder(@NonNull RecycleAdapter.MyViewHolder holder, int position) {
        ItemDataModel currentItem = dataSet.get(position);
        holder.imageView.setImageResource(currentItem.getImage());
        holder.tvName.setText(currentItem.getTxtname());
    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }
    @Override
    public Filter getFilter() {
        return Searched_Filter;
    }
    private Filter Searched_Filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<ItemDataModel> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(FullList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (ItemDataModel item : FullList) {
                    if (item.getTxtname().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataSet.clear();
            dataSet.addAll((ArrayList) results.values);
            notifyDataSetChanged();
        }
    };
}*/
