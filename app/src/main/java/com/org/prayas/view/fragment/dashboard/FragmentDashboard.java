package com.org.prayas.view.fragment.dashboard;

import static com.org.prayas.business_logic.preferences.PrefKeys.prefIsUserLogIn;
import static com.org.prayas.utils.ConstantCodes.URL_SIGN_IN;
import static com.org.prayas.utils.CustomBindingAdapter.getRoundCornerOptions;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.request.RequestOptions;
import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.GeneralItemListener;
import com.org.prayas.business_logic.interfaces.GeneralListener;
import com.org.prayas.business_logic.interfaces.ViewPageChangeListener;
import com.org.prayas.business_logic.viewmodel.dasboard.ViewModelDashboard;
import com.org.prayas.databinding.FragmentDashboardBinding;
import com.org.prayas.view.activity.main.ActivityMain;
import com.org.prayas.view.fragment.BaseFragment;

import java.util.Timer;
import java.util.TimerTask;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class FragmentDashboard extends BaseFragment {

    private ViewModelDashboard mViewModel;
    private FragmentDashboardBinding mBinding;

    private int currentPage = 0;
    private Timer mBannerTimer;
    private final Handler mBannerHandler = new Handler();

    private final SwipeRefreshLayout.OnRefreshListener onRefreshListener = this::onRefresh;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        mViewModel = new ViewModelProvider(this).get(ViewModelDashboard.class);
        mBinding.setViewmodel(mViewModel);
        mBinding.setRequestOptions(roundRequest());
        mBinding.setGeneralListener(generalListener);
        mBinding.setGeneralItemListener(generalItemListener);
        mBinding.setViewPageChangeListener(viewPageChangeListener);
        mBinding.setOnRefreshListener(onRefreshListener);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initComponents();
        observeEvents();
        mViewModel.callHomeAPI();
    }

    private void initComponents() {
//        setBanner();
        // mBinding.imgBanner.postDelayed(runnableBanner, 3000);
    }

    private void observeEvents() {
    }

    public void onRefresh() {
        mViewModel.onRefreshList(false);
    }

    private final GeneralListener generalListener = view -> {
        if (view.getId() == R.id.txt_sign_in) {
//            ((ActivityMain) mActivity).navigateToSchemes();
            ((ActivityMain) mActivity).navigateToWebView(string(R.string.text_sign_in), URL_SIGN_IN);
        }
    };

    private final GeneralItemListener generalItemListener = (view, position, item) -> {
        /*if (item instanceof PojoDashboardData) {

        }*/
    };

    public void reload() {

    }

    private void setBanner() {
        /*if (mBannerTimer != null) {
            mBannerTimer.cancel();
            mBannerTimer = null;
            if (currentPage != 0) {
                mBinding.viewpagerBanner.setCurrentItem(0, true);
            }
        }
        mBannerTimer = new Timer();
        mBinding.viewpagerBanner.postDelayed(() -> mBannerTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mBannerHandler.post(Update);
            }
        }, 4000, 4000), 2000L);*/
    }

    private void startBanner() {
        stopBanner();
        mBannerTimer = new Timer();
        mBannerTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mBannerHandler.post(Update);
            }
        }, 10, 3000);
    }

    private void stopBanner() {
        if (mBannerTimer != null) {
            mBannerTimer.cancel();
            mBannerTimer.purge();
            mBannerTimer = null;
            mBannerHandler.removeCallbacks(Update);
        }
    }

    private final Runnable Update = new Runnable() {
        public void run() {
            if (mViewModel.observableListBanner.size() > 0) {
                mViewModel.observableUrl.set(mViewModel.observableListBanner.get(currentPage).getSchemeLogoName());
                currentPage++;
                if (currentPage >= mViewModel.observableListBanner.size()) {
                    currentPage = 0;
                }
                Log.e("runnableBanner", "=" + currentPage);
                //mBinding.imgBanner.postDelayed(runnableBanner, 3000);
            }
        }
    };

    private final Runnable runnableBanner = new Runnable() {
        public void run() {
            if (mViewModel.observableListBanner.size() > 0) {
                mViewModel.observableUrl.set(mViewModel.observableListBanner.get(currentPage).getSchemeLogoName());
                currentPage++;
                if (currentPage >= mViewModel.observableListBanner.size()) {
                    currentPage = 0;
                }
                //Log.e("runnableBanner", "=" + currentPage);
                mBinding.imgBanner.postDelayed(runnableBanner, 3000);
            }
        }
    };

    private final ViewPageChangeListener viewPageChangeListener = position -> currentPage = position;

    private RequestOptions roundRequest() {
        return getRoundCornerOptions(mContext, 180, R.drawable.bg_round);
    }

    @Override
    public void onStart() {
        super.onStart();
        startBanner();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopBanner();
    }

    public void checkLoginSuccess() {
        mViewModel.observableIsLogin.set(mPreferences.getBoolean(prefIsUserLogIn));
    }
}