package com.org.prayas.view.dialog;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.org.prayas.view.activity.BaseActivity;

public class BottomSheetBase extends BottomSheetDialogFragment {

    protected Context mContext;
    protected BaseActivity mActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BaseActivity) getActivity();
        mContext = getContext();
        setHasOptionsMenu(false);
    }

}
