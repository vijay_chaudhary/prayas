package com.org.prayas.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.GeneralItemListener;
import com.org.prayas.business_logic.pojo.PojoMinDeptData;
import com.org.prayas.business_logic.pojo.PojoMinDeptData;
import com.org.prayas.databinding.LayoutItemFilterMinDeptBinding;
import com.org.prayas.databinding.LayoutItemFilterSectorsBinding;

import java.util.List;

public class AdapterMinDept extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<PojoMinDeptData> pojoMinDeptData;
    private final GeneralItemListener generalItemListener;
    private final int VIEW_ITEM = 1;

    public AdapterMinDept(List<PojoMinDeptData> pojoMinDeptData, GeneralItemListener generalItemListener) {
        this.pojoMinDeptData = pojoMinDeptData;
        this.generalItemListener = generalItemListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            LayoutItemFilterMinDeptBinding binding = DataBindingUtil.inflate(LayoutInflater
                            .from(parent.getContext()), R.layout.layout_item_filter_min_dept,
                    parent, false);
            binding.setGeneralItemListener(generalItemListener);
            return new ViewHolder(binding);
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_progress_bottom, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bind(pojoMinDeptData.get(position), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return pojoMinDeptData.get(position) != null ? VIEW_ITEM : 0;
    }

    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressViewHolder(View v) {
            super(v);
        }
    }


    @Override
    public int getItemCount() {
        return pojoMinDeptData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LayoutItemFilterMinDeptBinding binding;

        public ViewHolder(@NonNull LayoutItemFilterMinDeptBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(PojoMinDeptData pojo, int position) {
            binding.setPojo(pojo);
            binding.setCurrentPosition(position);
            binding.chkFilterMinDept.setOnCheckedChangeListener((buttonView, isChecked) -> pojo.setIs_selected(isChecked));
            binding.executePendingBindings();
        }
    }
}
