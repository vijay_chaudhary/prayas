package com.org.prayas.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.org.prayas.R;
import com.org.prayas.business_logic.interfaces.GeneralItemListener;
import com.org.prayas.business_logic.pojo.PojoDashboardServiceData;
import com.org.prayas.databinding.LayoutDashboardBinding;

import java.util.List;

/**
 * Created by Naria Sachin on Mar, 02 2021 11:34.
 */

public class AdapterDashboardService extends RecyclerView.Adapter {
    private final List<PojoDashboardServiceData> itemList;
    private final int VIEW_ITEM = 1;
    private final GeneralItemListener itemListener;

    public AdapterDashboardService(List<PojoDashboardServiceData> itemList, GeneralItemListener itemListener) {
        this.itemList = itemList;
        this.itemListener = itemListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            LayoutDashboardBinding binding = DataBindingUtil.inflate(LayoutInflater
                            .from(parent.getContext()), R.layout.layout_dashboard,
                    parent, false);
            binding.setGeneralItemListener(itemListener);
            return new ViewHolderItemView(binding);
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_progress_bottom, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderItemView) {
            ((ViewHolderItemView) holder).bind(itemList.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) != null ? VIEW_ITEM : 0;
    }

    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressViewHolder(View v) {
            super(v);
        }
    }

    private static class ViewHolderItemView extends RecyclerView.ViewHolder {
        private final LayoutDashboardBinding binding;

        ViewHolderItemView(LayoutDashboardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(PojoDashboardServiceData pojo, int position) {
            binding.setPojo(pojo);
            binding.setCurrentPosition(position);
            binding.executePendingBindings();
        }
    }
}
