package com.org.prayas.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.org.prayas.R;
import com.org.prayas.business_logic.pojo.PojoPlace;
import com.org.prayas.databinding.LayoutItemGridStateBinding;

import java.util.List;

public class AdapterGridState extends RecyclerView.Adapter {
    private final List<PojoPlace> itemList;
    private final int VIEW_ITEM = 1;

    public AdapterGridState(List<PojoPlace> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            LayoutItemGridStateBinding binding = DataBindingUtil.inflate(LayoutInflater
                            .from(parent.getContext()), R.layout.layout_item_grid_state,
                    parent, false);
            return new ViewHolderItemView(binding);
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_progress_bottom, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderItemView) {
            ((ViewHolderItemView) holder).bind(itemList.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) != null ? VIEW_ITEM : 0;
    }

    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressViewHolder(View v) {
            super(v);
        }
    }

    private static class ViewHolderItemView extends RecyclerView.ViewHolder {
        private final LayoutItemGridStateBinding binding;

        ViewHolderItemView(LayoutItemGridStateBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(PojoPlace pojo, int position) {
            binding.setTitle(pojo.getTitle());
            //binding.setCurrentPosition(position);
            binding.executePendingBindings();
        }
    }
}
