package com.org.prayas.view.activity;

import static com.org.prayas.business_logic.preferences.PrefKeys.prefIsUserLogIn;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.IntegerRes;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.org.prayas.R;
import com.org.prayas.business_logic.preferences.UtilsSharedPreferences;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    protected Context mContext;

    @Inject
    protected UtilsSharedPreferences mPreferences;

    private AlertDialog mAlertDialogPermission;

    protected int dimen(@DimenRes int resId) {
        return (int) getResources().getDimension(resId);
    }

    protected int color(@ColorRes int resId) {
        return getResources().getColor(resId);
    }

    protected int integer(@IntegerRes int resId) {
        return getResources().getInteger(resId);
    }

    protected String string(@StringRes int resId) {
        return getResources().getString(resId);
    }

    public void storeLoginPref(boolean isLogin) {
        mPreferences.setBoolean(prefIsUserLogIn, true);
    }

    /**
     * Navigate
     */
   /* public void navigateToLogin() {
        startActivity(new Intent(this, ActivityLogin.class));
        overridePendingTransition(0, 0);
        finish();
    }

    public void navigateToMain() {
        startActivity(new Intent(this, ActivityMain.class));
        overridePendingTransition(0, 0);
        finish();
    }*/

    public int checkPermissionResult(String[] permissions, int[] grantResults, @StringRes int rationalMessage) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean anyPermissionDenied = false;
            boolean neverAskAgainSelected = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    anyPermissionDenied = true;
                    neverAskAgainSelected = neverAskAgain(i, permissions);
                }
            }

            if (anyPermissionDenied) {
                if (neverAskAgainSelected) {
                    showAlertPermissions(rationalMessage);
                    return -1;
                } else {
                    return 0;
                }
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    public boolean isPermissionWithGranted(String[] permission, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> needPermission = checkPermissionList(permission, mContext);
            boolean isGranted = !(needPermission != null && !needPermission.isEmpty());
            if (isGranted) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this,
                        needPermission.toArray(new String[needPermission.size()]),
                        requestCode);
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Check for  permission is not granted
     *
     * @param permission list of  permission
     * @return non granted  permission list
     */
    public List<String> checkPermissionList(String[] permission, Context context) {
        List<String> needPermission = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permission != null &&
                permission.length > 0) {
            needPermission = new ArrayList<>();
            for (String aPermission : permission) {
                if (ActivityCompat.checkSelfPermission(context, aPermission) != PackageManager.PERMISSION_GRANTED) {
                    needPermission.add(aPermission);
                }
            }
        }
        return needPermission;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean neverAskAgain(int i, String[] permissions) {
        boolean neverAskAgainSelected = false;
        if (!shouldShowRequestPermissionRationale(permissions[i])) {
            neverAskAgainSelected = true;
        }
        return neverAskAgainSelected;
    }

    private void showAlertPermissions(int rationalMessage) {
        boolean isShow = true;
        if (mAlertDialogPermission != null && mAlertDialogPermission.isShowing()) {
            TextView tv_message = mAlertDialogPermission.findViewById(android.R.id.message);
            if (tv_message != null && !TextUtils.equals(tv_message.getText(), getString(rationalMessage))) {
                tv_message.setText(getString(rationalMessage));
            }
            isShow = false;
        }

        if (isShow) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.DialogTheme));
            alertDialog.setTitle(R.string.message_permission_not_granted);
            alertDialog.setMessage(rationalMessage);
            alertDialog.setCancelable(false);
            alertDialog.setNeutralButton(R.string.text_ok, (dialog, which) -> {
                Intent i = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + this.getPackageName()));
                startActivity(i);
            });
            mAlertDialogPermission = alertDialog.create();
            mAlertDialogPermission.show();
        }
    }

    public void logoutFromApp() {
        /*clearPreferenceData();
        Intent intent = new Intent(getBaseContext(), ActivityLogin.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();*/
    }

    private void clearPreferenceData() {
        mPreferences.clearAllPreferences();
    }

   /* public void setEventListener(ViewModelBase viewModelBase) {
        viewModelBase.getLiveDataTokenExpire().observe(this, msg -> {
            if (!isDestroyed() && mPreferences != null) {
                showSessionExpiredDialog(getString(R.string.text_token_expired), (dialog, which) -> {
                    ((ActivityMain) this).logoutFromApp();
                });
            }
        });
    }*/

   /* public void showSessionExpiredDialog(String message, DialogInterface
            .OnClickListener
            okListener) {
        AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this, R.style.DialogTheme);
        mAlertDialog.setCancelable(false);
        mAlertDialog.setMessage(message)
                .setPositiveButton(getResources().getString(R.string.tag_signin), okListener)
                .create()
                .show();
    }*/
}
