package com.org.prayas.view.webview;

import static com.org.prayas.business_logic.preferences.PrefKeys.prefIsUserLogIn;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.org.prayas.R;
import com.org.prayas.business_logic.viewmodel.web_view.ViewModelWebView;
import com.org.prayas.databinding.FragmentWebviewBinding;
import com.org.prayas.view.activity.main.ActivityMain;
import com.org.prayas.view.fragment.BaseFragment;

import dagger.hilt.android.AndroidEntryPoint;

/**
 * Created by Vijay Chaudhary Dec, 22 2021
 */
@AndroidEntryPoint
public class FragmentWebView extends BaseFragment {

    private ViewModelWebView mViewModel;
    private FragmentWebviewBinding mBinding;

    private boolean isLoginSuccess;

    /**
     * Web chrome client for webview used in screen basically used for progress bar
     */
    public WebChromeClient webChromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (newProgress < 100) {
                mBinding.progressbar.setVisibility(View.VISIBLE);
            } else {
                mBinding.progressbar.setVisibility(View.GONE);
            }
        }
    };
    private String webContent = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_webview, container, false);
        mViewModel = new ViewModelProvider(this).get(ViewModelWebView.class);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initComponents();
        observeEvents();
    }

    private void initComponents() {
        loadWebData();
    }

    private void observeEvents() {
    }

    private void loadWebData() {
        if (!TextUtils.isEmpty(webContent)) {
            setUpWebView(mBinding.webview);

            mBinding.webview.loadUrl(webContent);
            mBinding.webview.setWebChromeClient(webChromeClient);
            mBinding.webview.setWebViewClient(new WebViewClient() {
                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    mBinding.progressbar.setVisibility(View.GONE);
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                   /* if (handleSubPage) {
                        view.loadUrl(url);
                    } else {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    }*/
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    mBinding.progressbar.setVisibility(View.GONE);
                    if (url.contains("success-mobile-response") || url.contains("failure-mobile-response")) {
                        isLoginSuccess = url.contains("success-mobile-response");
                        mBinding.webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                    }
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    mBinding.progressbar.setVisibility(View.VISIBLE);
                    super.onPageStarted(view, url, favicon);
                }

                @SuppressLint("WebViewClientOnReceivedSslError")
                @Override
                public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                    handler.proceed(); // Ignore SSL certificate errors
                }
            });
        }
    }

    /*public void setHandleSubPage(boolean handleSubPage) {
        this.handleSubPage = handleSubPage;
    }*/

    /*public boolean canGoBack() {
//        if (handleSubPage && mBinding.webview.canGoBack()) {
        if (mBinding.webview.canGoBack()) {
            mBinding.webview.goBack();
            return true;
        } else {
            return false;
        }
    }*/

    public void setWebContent(String webContent) {
        this.webContent = webContent;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setUpWebView(WebView mWebview) {
        mWebview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        mWebview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        mWebview.getSettings().setDefaultTextEncodingName("utf-8");
        mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setLoadWithOverviewMode(false);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setAllowFileAccess(true);
        mWebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
//        mWebview.clearCache(true);
        mWebview.getSettings().setAppCacheEnabled(true);
        mWebview.getSettings().setAppCachePath(mActivity.getCacheDir().getPath());
        mWebview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        mWebview.getSettings().setDomStorageEnabled(true);
        mWebview.getSettings().setDisplayZoomControls(false);
        mWebview.getSettings().setBuiltInZoomControls(false);
        mWebview.getSettings().setLoadsImagesAutomatically(true);
        mWebview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        mWebview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private class MyJavaScriptInterface {
        @JavascriptInterface
        public void processHTML(String html) {
            /*String status = null;
            if (html.contains("Failure")) {
                status = "Transaction Declined!";
            } else if (html.contains("Success")) {
                status = "Transaction Successful!";
            } else if (html.contains("Aborted")) {
                status = "Transaction Cancelled!";
            } else {
                status = "Status Not Known!";
            }*/

            mPreferences.setBoolean(prefIsUserLogIn, isLoginSuccess);
            ((ActivityMain) mActivity).onBackPressed();
        }
    }
}
