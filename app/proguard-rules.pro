# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-keep class androidx.** { *; }
-keep interface androidx.** { *; }
-dontwarn androidx.**

-keep class android.** { *; }
-keep interface android.** { *; }
-dontwarn android.**

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

#Google
-keep class com.google.** { *; }
-keep interface com.google.** { *; }
-dontwarn com.google.**

-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}

# GSON
-keepattributes Signature
-keepattributes Annotation
-keep class sun.misc.Unsafe { *; }

#Dagger
-keep class dagger.** { *; }
-keep interface dagger.** { *; }
-dontwarn dagger.android.**

# okhttp
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**

# Okio
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-keep class okio.* { *; }
-keep interface okio.* { *; }
-dontwarn okio.**

# retrofit2
-dontwarn retrofit2.**
-keep class retrofit2.* { *; }
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

#DotIndicator
-keep class com.tbuonomo.** { *; }
-keep interface com.tbuonomo.** { *; }
-dontwarn com.tbuonomo.**

#ReactiveX
-keep class io.reactivex.** { *; }
-keep interface io.reactivex.** { *; }
-dontwarn io.reactivex.**

#Inject
-keep class javax.inject.** { *; }
-keep interface javax.inject.** { *; }
-dontwarn javax.inject.**

#ReactiveStream
-keep class org.reactivestreams.** { *; }
-keep interface org.reactivestreams.** { *; }
-dontwarn org.reactivestreams.**

#Lombok
-keep class org.projectlombok.** { *; }
-keep interface org.projectlombok.** { *; }
-dontwarn org.projectlombok.**